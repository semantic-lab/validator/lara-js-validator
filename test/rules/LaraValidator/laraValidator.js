import assert from 'assert';
import LaraValidator from '../../../src/laraValidator';

describe('LaraValidator', () => {
    describe('string rule config', () => {
        const config = {
            name: 'required|string|min:4',
            email: 'required|email',
            password: 'required|string|min:6|regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i|confirmed',
            phone: 'required|regex:/^(09)[0-9]{8}$/gi|bail',
            gender: 'nullable|numeric|in:0,1,2',
            address: 'present|string',
        };
        describe('expect [{}]', () => {
            const expect = JSON.stringify({});
            it('complete data', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                testParserResultWithExpect({config, data}, expect);
            });
            it('nullable field', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: null,
                    address: 'Taipei, Taiwan',
                };
                testParserResultWithExpect({config, data}, expect);
            });
            it('present field', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: undefined,
                };
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [name field invalid message]', () => {
            it('All (required)', () => {
                const data = {
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({name: [
                    'required fail',
                    'string fail',
                    'min fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('string', () => {
                const data = {
                    name: 123456,
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({name: [
                    'string fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('min:4', () => {
                const data = {
                    name: 'Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({name: [
                    'min fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [password field invalid message]', () => {
            it('string', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: 1597534,
                    password_confirmation: 1597534,
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'string fail',
                    'regex fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('min:6', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: 'A@s9',
                    password_confirmation: 'A@s9',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'min fail',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('regex', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!Ab@#$',
                    password_confirmation: '!Ab@#$',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'regex fail',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('confirmed', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA0Bb?',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'confirmed fail',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [phone field invalid message]', () => {
            it('required', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({phone: [
                    'required fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('regex', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '091100000A',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({phone: [
                    'regex fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
    });
    describe('object rule config', () => {
        const config = {
            name: {
                rules: {
                    'required': 'The field "name" is required',
                    'string': 'The field "name" should be string type',
                    'min:4': 'The field "name" can not less then :min characters'
                },
            },
            email: {
                rules: {
                    'required': null,
                    'email': 'Error email format',
                },
            },
            password: {
                rules: {
                    'required': null,
                    'string': null,
                    'min:6': null,
                    'regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i': 'The password should match regular expression: :pattern',
                    'confirmed': 'The field "password_confirmation" is not exist or the value is not equals to value of "password_confirmation"',
                },
            },
            phone: {
                rules: {
                    'required': null,
                    'regex:/^(09)[0-9]{8}$/gi': null,
                    'bail': null,
                },
            },
            gender: {
                rules: {
                    'nullable': null,
                    'numeric': null,
                    'in:0,1,2': 'The gender should only be 0, 1 or 2'
                },
            },
            address: {
                rules: {
                    'present': 'The field "address" is not exist',
                    'string': null,
                },
            },
        };
        describe('expect [{}}]', () => {
            const expect = JSON.stringify({});
            it('complete data', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                testParserResultWithExpect({config, data}, expect);
            });
            it('nullable field', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: null,
                    address: 'Taipei, Taiwan',
                };
                testParserResultWithExpect({config, data}, expect);
            });
            it('present field', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: undefined,
                };
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [name field invalid message]', () => {
            it('All (required)', () => {
                const data = {
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({name: [
                    'The field "name" is required',
                    'The field "name" should be string type',
                    'The field "name" can not less then 4 characters'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('string', () => {
                const data = {
                    name: 123456,
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({name: [
                    'The field "name" should be string type'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('min:4', () => {
                const data = {
                    name: 'Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({name: [
                    'The field "name" can not less then 4 characters'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [password field invalid message]', () => {
            it('string', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: 1597534,
                    password_confirmation: 1597534,
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'string fail',
                    'The password should match regular expression: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('min:6', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: 'A@s9',
                    password_confirmation: 'A@s9',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'min fail',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('regex', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!Ab@#$',
                    password_confirmation: '!Ab@#$',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'The password should match regular expression: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('confirmed', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA0Bb?',
                    phone: '0911000001',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({password: [
                    'The field "password_confirmation" is not exist or the value is not equals to value of "password_confirmation"',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [phone field invalid message]', () => {
            it('required', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({phone: [
                    'required fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('regex', () => {
                const data = {
                    name: 'Semantic Lab',
                    email: 'familywithloveg@gmail.com',
                    password: '!aA00Bb%',
                    password_confirmation: '!aA00Bb%',
                    phone: '091100000A',
                    gender: 1,
                    address: 'Taipei, Taiwan',
                };
                const expect = JSON.stringify({phone: [
                    'regex fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
    });
    describe('array rule config', () => {
        const config = {
            users: [
                {
                    name: {
                        rules: {
                            'required': 'The field "name" is required',
                            'string': 'The field "name" should be string type',
                            'min:4': 'The field "name" can not less then :min characters'
                        },
                    },
                    email: {
                        rules: {
                            'required': null,
                            'email': 'Error email format',
                        },
                    },
                    password: {
                        rules: {
                            'required': null,
                            'string': null,
                            'min:6': null,
                            'regex:/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i': 'The password should match regular expression: :pattern',
                            'confirmed': 'The field "password_confirmation" is not exist or the value is not equals to value of "password_confirmation"',
                        },
                    },
                    phone: {
                        rules: {
                            'required': null,
                            'regex:/^(09)[0-9]{8}$/gi': null,
                            'bail': null,
                        },
                    },
                    gender: {
                        rules: {
                            'nullable': null,
                            'numeric': null,
                            'in:0,1,2': 'The gender should only be 0, 1 or 2'
                        },
                    },
                    address: {
                        rules: {
                            'present': 'The field "address" is not exist',
                            'string': null,
                        },
                    },
                }
            ],
        };
        describe('expect [{}]', () => {
            const expect = JSON.stringify({});
            it('complete data', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                testParserResultWithExpect({config, data}, expect);
            });
            it('nullable field', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            phone: '0911000001',
                            gender: null,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                testParserResultWithExpect({config, data}, expect);
            });
            it('present field', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            phone: '0911000001',
                            gender: 1,
                            address: undefined,
                        }
                    ]
                };
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [name field invalid message]', () => {
            it('All (required)', () => {
                const data = {
                    users: [
                        {
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.name': [
                    'The field "name" is required',
                    'The field "name" should be string type',
                    'The field "name" can not less then 4 characters'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('string', () => {
                const data = {
                    users: [
                        {
                            name: 123456,
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.name': [
                    'The field "name" should be string type'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('min:4', () => {
                const data = {
                    users: [
                        {
                            name: 'Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.name': [
                    'The field "name" can not less then 4 characters'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [password field invalid message]', () => {
            it('string', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: 1597534,
                            password_confirmation: 1597534,
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.password': [
                    'string fail',
                    'The password should match regular expression: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('min:6', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: 'A@s9',
                            password_confirmation: 'A@s9',
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.password': [
                    'min fail',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('regex', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!Ab@#$',
                            password_confirmation: '!Ab@#$',
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.password': [
                    'The password should match regular expression: /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*?]).*$/i',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('confirmed', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!aA0Bb?',
                            phone: '0911000001',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.password': [
                    'The field "password_confirmation" is not exist or the value is not equals to value of "password_confirmation"',
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
        describe('expect [phone field invalid message]', () => {
            it('required', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.phone': [
                    'required fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
            it('regex', () => {
                const data = {
                    users: [
                        {
                            name: 'Semantic Lab',
                            email: 'familywithloveg@gmail.com',
                            password: '!aA00Bb%',
                            password_confirmation: '!aA00Bb%',
                            phone: '091100000A',
                            gender: 1,
                            address: 'Taipei, Taiwan',
                        }
                    ]
                };
                const expect = JSON.stringify({'users.0.phone': [
                    'regex fail'
                ]});
                testParserResultWithExpect({config, data}, expect);
            });
        });
    });
    describe('complex config', () => {
        const createFriendGroupsConfig = {
            id: 'required|string',
            token: {
                rules: {
                    'required': 'auth token is missing',
                    'string': null,
                }
            },
            groups: [
                {
                    isPrivate: 'boolean',
                    groupName: 'required|string',
                    members: [
                        {
                            id: 'required|string',
                            title: {
                                rules: {
                                    'nullable': null,
                                    'string': null,
                                }
                            },
                        }
                    ],
                    updateInfo: {
                        startDate: 'present|date',
                        frequency: 'present|in:1,30,90',
                        endDate: 'present|date',
                    }
                }
            ],
        };
        describe('expect [{}] (no error message)', () => {
            const expect = JSON.stringify({});
            it('complete data', () => {
                const data = {
                    id: '41',
                    token: 'A56WETrf54SDjHtrAf75',
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: 'BF0' },
                                { id: '1', title: 'BF1' },
                                { id: '2', title: 'BF2' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: '2019-04-23',
                                frequency: 1,
                                endDate: 1555977600000,
                            }
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: 'PM' },
                                { id: '106', title: 'SA' },
                                { id: '82', title: 'R&D' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 90,
                                endDate: 1555977600000,
                            }
                        },
                    ],
                };
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
            });
            it('nullable field', () => {
                const data = {
                    id: '41',
                    token: 'A56WETrf54SDjHtrAf75',
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: null },
                                { id: '1', title: null },
                                { id: '2', title: null },
                            ],
                            updateInfo: {
                                startDate: null,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: null,
                                frequency: null,
                                endDate: null,
                            }
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: null },
                                { id: '106', title: null },
                                { id: '82', title: null },
                            ],
                            updateInfo: {
                                startDate: null,
                                frequency: null,
                                endDate: null,
                            }
                        },
                    ],
                };
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
            });
            it('present field', () => {
                const data = {
                    id: '41',
                    token: 'A56WETrf54SDjHtrAf75',
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: null },
                                { id: '1', title: null },
                                { id: '2', title: null },
                            ],
                            updateInfo: {
                                startDate: null,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: '2019-04-23',
                                frequency: undefined,
                                endDate: undefined,
                            }
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: null },
                                { id: '106', title: null },
                                { id: '82', title: null },
                            ],
                            updateInfo: {
                                startDate: undefined,
                                frequency: undefined,
                                endDate: undefined,
                            }
                        },
                    ],
                };
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
            });
        });
        describe('expect [token field invalid message]', () => {
            it('All (required)', () => {
                const data = {
                    id: '41',
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: 'BF0' },
                                { id: '1', title: 'BF1' },
                                { id: '2', title: 'BF2' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: '2019-04-23',
                                frequency: 1,
                                endDate: 1555977600000,
                            }
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: 'PM' },
                                { id: '106', title: 'SA' },
                                { id: '82', title: 'R&D' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 90,
                                endDate: 1555977600000,
                            }
                        },
                    ],
                };
                const expect = JSON.stringify({token: [
                    'auth token is missing',
                    'string fail'
                ]});
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
            });
            it('string', () => {
                const data = {
                    id: '41',
                    token: 456123,
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: 'BF0' },
                                { id: '1', title: 'BF1' },
                                { id: '2', title: 'BF2' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: '2019-04-23',
                                frequency: 1,
                                endDate: 1555977600000,
                            }
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: 'PM' },
                                { id: '106', title: 'SA' },
                                { id: '82', title: 'R&D' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 90,
                                endDate: 1555977600000,
                            }
                        },
                    ],
                };
                const expect = JSON.stringify({token: [
                    'string fail'
                ]});
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
            });
        });
        describe('expect [groups.*.isPrivate field invalid message]', () => {
            it('boolean', () => {
                let data;
                let expect = JSON.stringify({'groups.0.isPrivate': [
                    'boolean fail'
                ]});
                data = {
                    id: '41',
                    token: 'A56WETrf54SDjHtrAf75',
                    groups: [
                        {
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: 'BF0' },
                                { id: '1', title: 'BF1' },
                                { id: '2', title: 'BF2' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: '2019-04-23',
                                frequency: 1,
                                endDate: 1555977600000,
                            }
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: 'PM' },
                                { id: '106', title: 'SA' },
                                { id: '82', title: 'R&D' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 90,
                                endDate: 1555977600000,
                            }
                        },
                    ],
                };
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
                expect = JSON.stringify({'groups.1.isPrivate': [
                    'boolean fail'
                ]});
                data = {
                    id: '41',
                    token: 'A56WETrf54SDjHtrAf75',
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: 'BF0' },
                                { id: '1', title: 'BF1' },
                                { id: '2', title: 'BF2' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: 456,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: '2019-04-23',
                                frequency: 1,
                                endDate: 1555977600000,
                            }
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: 'PM' },
                                { id: '106', title: 'SA' },
                                { id: '82', title: 'R&D' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 90,
                                endDate: 1555977600000,
                            }
                        },
                    ],
                };
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
                expect = JSON.stringify({'groups.2.isPrivate': [
                    'boolean fail'
                ]});
                data = {
                    id: '41',
                    token: 'A56WETrf54SDjHtrAf75',
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: 'BF0' },
                                { id: '1', title: 'BF1' },
                                { id: '2', title: 'BF2' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                            updateInfo: {
                                startDate: '2019-04-23',
                                frequency: 1,
                                endDate: 1555977600000,
                            }
                        },
                        {
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: 'PM' },
                                { id: '106', title: 'SA' },
                                { id: '82', title: 'R&D' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 90,
                                endDate: 1555977600000,
                            }
                        },
                    ],
                };
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
            });
        });
        describe('expect [groups.*.updateInfo field invalid message]', () => {
            it('All', () => {
                const data = {
                    id: '41',
                    token: 'A56WETrf54SDjHtrAf75',
                    groups: [
                        {
                            isPrivate: true,
                            groupName: 'Best Friends',
                            members: [
                                { id: '0', title: 'BF0' },
                                { id: '1', title: 'BF1' },
                                { id: '2', title: 'BF2' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 30,
                                endDate: '2019-11-21',
                            }
                        },
                        {
                            isPrivate: true,
                            groupName: 'Private Repo',
                            members: [
                                { id: '0', title: 'R&D 0' },
                                { id: '80', title: 'R&D 1' },
                                { id: '27', title: 'R&D 2' },
                            ],
                        },
                        {
                            isPrivate: false,
                            groupName: 'colleague',
                            members: [
                                { id: '73', title: 'PM' },
                                { id: '106', title: 'SA' },
                                { id: '82', title: 'R&D' },
                            ],
                            updateInfo: {
                                startDate: 1552867200000,
                                frequency: 90,
                                endDate: 1555977600000,
                            }
                        },
                    ],
                };
                const expect = JSON.stringify({
                    'groups.1.updateInfo.startDate': [
                        'present fail',
                        'date fail'
                    ],
                    'groups.1.updateInfo.frequency': [
                        'present fail',
                        'in fail'
                    ],
                    'groups.1.updateInfo.endDate': [
                        'present fail',
                        'date fail'
                    ],
                });
                testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
            });
            describe('groups.*.updateInfo.startDate', () => {
                it('date', () => {
                    const data = {
                        id: '41',
                        token: 'A56WETrf54SDjHtrAf75',
                        groups: [
                            {
                                isPrivate: true,
                                groupName: 'Best Friends',
                                members: [
                                    { id: '0', title: 'BF0' },
                                    { id: '1', title: 'BF1' },
                                    { id: '2', title: 'BF2' },
                                ],
                                updateInfo: {
                                    startDate: 1552867200000,
                                    frequency: 30,
                                    endDate: '2019-11-21',
                                }
                            },
                            {
                                isPrivate: true,
                                groupName: 'Private Repo',
                                members: [
                                    { id: '0', title: 'R&D 0' },
                                    { id: '80', title: 'R&D 1' },
                                    { id: '27', title: 'R&D 2' },
                                ],
                                updateInfo: {
                                    startDate: '2019-04-23',
                                    frequency: 1,
                                    endDate: 1555977600000,
                                }
                            },
                            {
                                isPrivate: false,
                                groupName: 'colleague',
                                members: [
                                    { id: '73', title: 'PM' },
                                    { id: '106', title: 'SA' },
                                    { id: '82', title: 'R&D' },
                                ],
                                updateInfo: {
                                    startDate: 'now',
                                    frequency: 90,
                                    endDate: 1555977600000,
                                }
                            },
                        ],
                    };
                    const expect = JSON.stringify({'groups.2.updateInfo.startDate': [
                        'date fail',
                    ]});
                    testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
                });
            });
            describe('groups.*.updateInfo.frequency', () => {
                it('in', () => {
                    const data = {
                        id: '41',
                        token: 'A56WETrf54SDjHtrAf75',
                        groups: [
                            {
                                isPrivate: true,
                                groupName: 'Best Friends',
                                members: [
                                    { id: '0', title: 'BF0' },
                                    { id: '1', title: 'BF1' },
                                    { id: '2', title: 'BF2' },
                                ],
                                updateInfo: {
                                    startDate: 1552867200000,
                                    frequency: 30,
                                    endDate: '2019-11-21',
                                }
                            },
                            {
                                isPrivate: true,
                                groupName: 'Private Repo',
                                members: [
                                    { id: '0', title: 'R&D 0' },
                                    { id: '80', title: 'R&D 1' },
                                    { id: '27', title: 'R&D 2' },
                                ],
                                updateInfo: {
                                    startDate: '2019-04-23',
                                    frequency: 1,
                                    endDate: 1555977600000,
                                }
                            },
                            {
                                isPrivate: false,
                                groupName: 'colleague',
                                members: [
                                    { id: '73', title: 'PM' },
                                    { id: '106', title: 'SA' },
                                    { id: '82', title: 'R&D' },
                                ],
                                updateInfo: {
                                    startDate: 1552867200000,
                                    frequency: 2,
                                    endDate: 1555977600000,
                                }
                            },
                        ],
                    };
                    const expect = JSON.stringify({'groups.2.updateInfo.frequency': [
                        'in fail',
                    ]});
                    testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
                });
            });
            describe('groups.*.updateInfo.endDate', () => {
                it('date', () => {
                    const data = {
                        id: '41',
                        token: 'A56WETrf54SDjHtrAf75',
                        groups: [
                            {
                                isPrivate: true,
                                groupName: 'Best Friends',
                                members: [
                                    { id: '0', title: 'BF0' },
                                    { id: '1', title: 'BF1' },
                                    { id: '2', title: 'BF2' },
                                ],
                                updateInfo: {
                                    startDate: 1552867200000,
                                    frequency: 30,
                                    endDate: '2019-11-21',
                                }
                            },
                            {
                                isPrivate: true,
                                groupName: 'Private Repo',
                                members: [
                                    { id: '0', title: 'R&D 0' },
                                    { id: '80', title: 'R&D 1' },
                                    { id: '27', title: 'R&D 2' },
                                ],
                                updateInfo: {
                                    startDate: '2019-04-23',
                                    frequency: 1,
                                    endDate: '?!@#$',
                                }
                            },
                            {
                                isPrivate: false,
                                groupName: 'colleague',
                                members: [
                                    { id: '73', title: 'PM' },
                                    { id: '106', title: 'SA' },
                                    { id: '82', title: 'R&D' },
                                ],
                                updateInfo: {
                                    startDate: 1552867200000,
                                    frequency: 90,
                                    endDate: 1555977600000,
                                }
                            },
                        ],
                    };
                    const expect = JSON.stringify({'groups.1.updateInfo.endDate': [
                        'date fail',
                    ]});
                    testParserResultWithExpect({config: createFriendGroupsConfig, data}, expect);
                });
            });
        });
    });
});

function testParserResultWithExpect(dataToParse, expect) {
    const parser = new LaraValidator(
        dataToParse.config,
        dataToParse.data,
        dataToParse.customRulesMessage
    );
    let validResult = JSON.stringify({});
    const isValid = parser.valid();
    if (!isValid) {
        validResult = JSON.stringify(parser.errorMessage);
    }
    assert.strictEqual(validResult, expect);
}
