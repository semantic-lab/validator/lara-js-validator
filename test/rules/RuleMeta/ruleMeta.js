import assert from 'assert';
import RuleMeta from '../../../src/rules/RuleMeta';


describe('getValuesByPaths', () => {
    it('true', () => {
        const path = ['a', '*', 'b', '*', 'c'];
        const data = {
            a: [
                {b: [ {c: '0'}, {c: '1'}, {c: '2'} ]},
                {b: [ {c: '3'}, {c: '4'}, {c: '5'} ]},
                {b: [ {c: '6'}, {c: '7'}, {c: '8'} ]},
            ],
        };
        const expected = [
            { path: [ 'a', '0', 'b', '0', 'c' ], value: '0' },
            { path: [ 'a', '0', 'b', '1', 'c' ], value: '1' },
            { path: [ 'a', '0', 'b', '2', 'c' ], value: '2' },
            { path: [ 'a', '1', 'b', '0', 'c' ], value: '3' },
            { path: [ 'a', '1', 'b', '1', 'c' ], value: '4' },
            { path: [ 'a', '1', 'b', '2', 'c' ], value: '5' },
            { path: [ 'a', '2', 'b', '0', 'c' ], value: '6' },
            { path: [ 'a', '2', 'b', '1', 'c' ], value: '7' },
            { path: [ 'a', '2', 'b', '2', 'c' ], value: '8' }
        ];
        const result = RuleMeta.getValuesByPaths(path, data);
        assert.strictEqual(JSON.stringify(result), JSON.stringify(expected));
    });
});
