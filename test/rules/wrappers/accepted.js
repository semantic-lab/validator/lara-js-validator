import testHelper from './testHelper';

describe('wrappers.accepted', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {withPets: true},
                {withPets: 'true'},
                {withPets: 'yes'},
                {withPets: 'on'},
                {withPets: '1'},
                {withPets: 1},
            ];
            const nullTestCases = [
                {withPets: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['withPets']] };
            const testCases = [
                {withPets: false},
                {withPets: 'false'},
                {withPets: 'no'},
                {withPets: 'off'},
                {withPets: '0'},
                {withPets: 0},
                {withPets: 'string'},
                {withPets: 41},
                {withPets: {}},
                {withPets: []},
                {withPets: undefined},
                {withPets: null},
                {withPets: new Date('2019-03-01')},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {room: {environment: {withPets: 'on'}}},
                {room: {environment: {withPets: 'yes'}}},
                {room: {environment: {withPets: 1}}},
            ];
            const nullTestCases = [
                {room: {environment: {withPets: null}}}
            ];

            testHelper({
                parentPath: ['room', 'environment'],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['room', 'environment'],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['room', 'environment', 'withPets']] };
            const testCases = [
                {room: {environment: {withPets: 'off'}}},
                {room: {environment: {withPets: {}}}},
                {room: {environment: {withPets: []}}},
                {room: {environment: {withPets: undefined}}},
                {room: {environment: {withPets: null}}},
            ];

            testHelper({
                parentPath: ['room', 'environment'],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {room: [{withPets: true}, {withPets: 'yes'}, {withPets: 1}]},
            ];
            const nullTestCases = [
                {room: [{withPets: true}, {withPets: 'yes'}, {withPets: null}]},
                {room: [{withPets: null}, {withPets: null}, {withPets: null}]},
            ];

            testHelper({
                parentPath: ['room', '*'],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['room', '*'],
                fieldName: 'withPets',
                ruleWithOptions: 'accepted',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['room', '1', 'withPets']] },
                { result: false, fail: [['room', '2', 'withPets']] },
                { result: false, fail: [['room', '0', 'withPets']] },
                { result: false, fail: [['room', '0', 'withPets']] },
            ];
            const testCases = [
                {room: [{withPets: true}, {withPets: 'off'}, {withPets: 1}]},
                {room: [{withPets: true}, {withPets: 'yes'}, {withPets: {}}]},
                {room: [{withPets: []}, {withPets: 'yes'}, {withPets: 1}]},
                {room: [{withPets: undefined}, {withPets: null}, {withPets: new Date('2019-03-03')}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['room', '*'],
                    fieldName: 'withPets',
                    ruleWithOptions: 'accepted',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});