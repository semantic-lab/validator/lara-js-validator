import testHelper from './testHelper';

describe('wrappers.in_array', () => {
    const menu = [
        {id: 0, title: 'Big Mac'},
        'McDouble',
        2,
        ['Coke-Cola', 'Fries'],
        /^Mc/i,
        new Date('2019-03-02'),
    ];
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {menu, orders: 'Big Mac'},
                {menu, orders: 'McDouble'},
                {menu, orders: 2},
                {menu, orders: 'Coke-Cola'},
                {menu, orders: /^Mc/i},
                {menu, orders: new Date('2019-03-02')},
            ];
            const nullTestCases = [
                {menu, orders: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'in_array:menu.*',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'in_array:menu.*',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['orders']] };
            const testCases = [
                {menu, orders: {id: 1, title: 'Big Mac'}},
                {menu, orders: 'Double Quarter Pounder'},
                {menu, orders: 3},
                {menu, orders: ['Fries']},
                {menu, orders: /^Double Quarter/i},
                {menu, orders: new Date('2019-03-03')},
                {menu, orders: true},
                {menu, orders: {}},
                {menu, orders: []},
                {menu, orders: undefined},
                {menu, orders: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'in_array:menu.*',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data:{menu}, user: {order: {meals: 'Big Mac'}}},
                {data:{menu}, user: {order: {meals: 'McDouble'}}},
                {data:{menu}, user: {order: {meals: 2}}},
                {data:{menu}, user: {order: {meals: 'Coke-Cola'}}},
                {data:{menu}, user: {order: {meals: /^Mc/i}}},
                {data:{menu}, user: {order: {meals: new Date('2019-03-02')}}},
            ];
            const nullTestCases = [
                {data:{menu}, user: {order: {meals: null}}}
            ];

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'in_array:data.menu.*',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'in_array:data.menu.*',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', 'order', 'meals']] };
            const testCases = [
                {data:{menu}, user: {order: {meals: {id: 1, title: 'Big Mac'}}}},
                {data:{menu}, user: {order: {meals: 'Double Quarter Pounder'}}},
                {data:{menu}, user: {order: {meals: 3}}},
                {data:{menu}, user: {order: {meals: ['Fries']}}},
                {data:{menu}, user: {order: {meals: /^Double Quarter/i}}},
                {data:{menu}, user: {order: {meals: new Date('2019-03-03')}}},
                {data:{menu}, user: {order: {meals: true}}},
                {data:{menu}, user: {order: {meals: {}}}},
                {data:{menu}, user: {order: {meals: []}}},
                {data:{menu}, user: {order: {meals: undefined}}},
                {data:{menu}, user: {order: {meals: null}}},
            ];

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'in_array:data.menu.*',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 2}, {orders: /^Mc/i}]},
            ];
            const nullTestCases = [
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 2}, {orders: null}]},
                {data:[{menu}, {menu}, {menu}], user: [{orders: null}, {orders: null}, {orders: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'orders',
                ruleWithOptions: 'in_array:data.*.menu.*',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'orders',
                ruleWithOptions: 'in_array:data.*.menu.*',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['user', '0', 'orders']] },
                { result: false, fail: [['user', '1', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
            ];
            const testCases = [
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'Large Coke'}, {orders: 2}, {orders: /^Mc/i}]},
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 4}, {orders: /^Mc/i}]},
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 2}, {orders: /^mC/i}]},
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 2}, {orders: []}]},
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 2}, {orders: {}}]},
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 2}, {orders: undefined}]},
                {data:[{menu}, {menu}, {menu}], user: [{orders: 'McDouble'}, {orders: 2}, {orders: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['user', '*'],
                    fieldName: 'orders',
                    ruleWithOptions: 'in_array:data.*.menu.*',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});
