import testHelper from "./testHelper";

class User {
    constructor(name) {
        this.name = name;
    };
    getName() {
        return name;
    };
}

class Helper {
    constructor() {}
    sayHi() { console.log('hi'); };
}

describe('wrappers.different', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'A12bc', validData: 'a1-2bc'},
                {data: 0.41, validData: 4.1},
                {data: false, validData: true},
                {data: {index: 1}, validData: {index: 2}},
                {data: /^[0-9a-zA-z]*$/i, validData: /^[0-9]*$/i},
                {data: new User('Albert'), validData: new User('Lin')},
                {data: [], validData: [1, 2, 3]},
                {data: {}, validData: {index: 2}},
            ];
            const nullTestCases = [
                {data: null, validData: undefined},
            ];
            const undefinedTestCases = [
                {data: undefined, validData: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'different:validData',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'different:validData',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'different:validData',
                isNullable: false,
                presentOnly: true
            }, undefinedTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {data: 'A12bc', validData: 'A12bc'},
                {data: 0.41, validData: 0.41},
                {data: false, validData: false},
                {data: {index: 1}, validData: {index: 1}},
                {data: /^[0-9a-zA-z]*$/i, validData: /^[0-9a-zA-z]*$/i},
                {data: new User('Albert'), validData: new User('Albert')},
                {data: new Helper(), validData: new Helper()},
                {data: [], validData: []},
                {data: {}, validData: {}},
                {data: undefined, validData: undefined},
                {data: null, validData: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'different:validData',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {
                    data: {user: {info: 'A12bc'}},
                    validData: {editor: {info: 'a1-2bc'}},
                },
                {
                    data: {user: {info: 0.41}},
                    validData: {editor: {info: 4.1}},
                },
                {
                    data: {user: {info: false}},
                    validData: {editor: {info: true}},
                },
                {
                    data: {user: {info: {index: 1}}},
                    validData: {editor: {info: {index: 2}}},
                },
                {
                    data: {user: {info: /^[0-9a-zA-z]*$/i}},
                    validData: {editor: {info: /^[0-9a-zA-z]*$/gi}},
                },
                {
                    data: {user: {info: new User('Albert')}},
                    validData: {editor: {info: new User('Lin')}},
                },
                {
                    data: {user: {info: []}},
                    validData: {editor: {info: [1, 2, 3]}},
                },
                {
                    data: {user: {info: {}}},
                    validData: {editor: {info: {index: 2}}},
                },
            ];
            const nullTestCases = [
                {
                    data: {user: {info: null}},
                    validData: {editor: {info: undefined}},
                },
            ];
            const undefinedTestCases = [
                {
                    data: {user: {info: undefined}},
                    validData: {editor: {info: null}},
                },
            ];

            testHelper({
                parentPath: ['data', 'user'],
                fieldName: 'info',
                ruleWithOptions: 'different:validData.editor.info',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['data', 'user'],
                fieldName: 'info',
                ruleWithOptions: 'different:validData.editor.info',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);

            testHelper({
                parentPath: ['data', 'user'],
                fieldName: 'info',
                ruleWithOptions: 'different:validData.editor.info',
                isNullable: false,
                presentOnly: true
            }, undefinedTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data', 'user', 'info']] };
            const testCases = [
                {
                    data: {user: {info: 'A12bc'}},
                    validData: {editor: {info: 'A12bc'}},
                },
                {
                    data: {user: {info:  0.41}},
                    validData: {editor: {info:  0.41}},
                },
                {
                    data: {user: {info: false}},
                    validData: {editor: {info: false}},
                },
                {
                    data: {user: {info: {index: 1}}},
                    validData: {editor: {info: {index: 1}}},
                },
                {
                    data: {user: {info: /^[0-9a-zA-z]*$/i}},
                    validData: {editor: {info: /^[0-9a-zA-z]*$/i}},
                },
                {
                    data: {user: {info: new User('Albert')}},
                    validData: {editor: {info: new User('Albert')}},
                },
                {
                    data: {user: {info: new Helper()}},
                    validData: {editor: {info: new Helper()}},
                },
                {
                    data: {user: {info: []}},
                    validData: {editor: {info: []}},
                },
                {
                    data: {user: {info: {}}},
                    validData: {editor: {info: {}}},
                },
                {
                    data: {user: {info: undefined}},
                    validData: {editor: {info: undefined}},
                },
                {
                    data: {user: {info: null}},
                    validData: {editor: {info: null}},
                },
            ];

            testHelper({
                parentPath: ['data', 'user'],
                fieldName: 'info',
                ruleWithOptions: 'different:validData.editor.info',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('both array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {
                        posts: [{data: 'a'}, {data: 41}, {data: true}],
                        valid: [{data: 'b'}, {data: 1}, {data: false}],
                    },
                    {
                        posts: [],
                        valid: [],
                    }
                ];

                testHelper({
                    parentPath: ['posts', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'different:valid.*.data',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expects = [
                    { result: false, fail: [['posts', '2', 'data']] },
                    { result: false, fail: [['posts', '1', 'data']] },
                ];
                const testCases = [
                    {
                        posts: [{data: 'a'}, {data: 41}, {data: true}],
                        valid: [{data: 'b'}, {data: 1}, {data: true}],
                    },
                    {
                        posts: [{data: 'a'}, {data: 41}, {data: true}],
                        valid: [{data: 'b'}, {}, {data: false}],
                    },
                ];

                testCases.forEach((testCase, index) => {
                    testHelper({
                        parentPath: ['posts', '*'],
                        fieldName: 'data',
                        ruleWithOptions: 'different:valid.*.data',
                        isNullable: false,
                        presentOnly: false
                    }, [testCase], expects[index]);
                });
            });
        });

        describe('fields in rule is not array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {editor: 'A12bc', data: [{user: 'A1-2bc'}, {user: 'a1-2bc'}]},
                    {editor: 0.41, data: [{user: 4}, {user: 41}]},
                    {editor: false, data: [{user: 'null'}, {user: true}]},
                    {editor: {index: 1}, data: [{user: {index: 10}}, {user: {index: 2}}]},
                    {editor: /^[0-9a-zA-z]*$/i, data: [{user: /^[0-9a-z]*$/i}, {user: /^[0-9a-zA-z]*$/gi}]},
                    {editor: new User('Albert'), data: [{user: new User('Dirk')}, {user: new User('Nowitzki')}]},
                    {editor: [], data: [{user: [1, 2, 3]}, {user: [4]}]},
                    {editor: {}, data: [{user: {index: 3}}, {user: []}]},
                ];
                const nullTestCases = [
                    {editor: null, data: [{user: null}, {user: null}]},
                ];
                const undefinedTestCases = [
                    {editor: undefined, data: [{user: undefined}, {user: undefined}]},
                ];

                testHelper({
                    parentPath: ['data', '*'],
                    fieldName: 'user',
                    ruleWithOptions: 'different:editor',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);

                testHelper({
                    parentPath: ['data', '*'],
                    fieldName: 'user',
                    ruleWithOptions: 'different:editor',
                    isNullable: true,
                    presentOnly: false
                }, nullTestCases, expect);

                testHelper({
                    parentPath: ['data', '*'],
                    fieldName: 'user',
                    ruleWithOptions: 'different:editor',
                    isNullable: false,
                    presentOnly: true
                }, undefinedTestCases, expect);
            });
            describe('expect [false]', () => {
                const expects = [
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '1', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                    { result: false, fail: [['data', '0', 'user']] },
                ];
                const testCases = [
                    {editor: 'A12bc', data: [{user: 'A12bb'}, {user: 'A12bc'}]},
                    {editor: 0.41, data: [{user: 0.42}, {user: 0.41}]},
                    {editor: false, data: [{user: true}, {user: false}]},
                    {editor: {index: 1}, data: [{user: {index: 1}}, {user: {index: 2}}]},
                    {editor: /^[0-9a-zA-z]*$/i, data: [{user: /^[0-9a-zA-z]*$/i}, {user: /^[0-9a-zA-z]*$/g}]},
                    {editor: new User('Albert'), data: [{user: new User('Albert')}, {user: new User('Lin')}]},
                    {editor: new Helper(), data: [{user: new Helper()}, {user: 'Helper'}]},
                    {editor: [], data: [{user: []}, {user: [1]}]},
                    {editor: {}, data: [{user: {}}, {user: {1: 'x'}}]},
                    {editor: 'undefined', data: [{user: 'null'}, {user: 'undefined'}]},
                    {editor: 'null', data: [{user: 'undefined'}, {user: 'null'}]},
                    {editor: 'A12bc', data: [{user: 'A12bc'}, {user: 'a1-2bc'}]},
                    {editor: 0.41, data: [{user: 4.1}, {user: 0.41}]},
                    {editor: false, data: [{user: true}, {user: false}]},
                    {editor: {index: 1}, data: [{user: {index: 1}}, {user: {index: 2}}]},
                    {editor: /^[0-9a-zA-z]*$/i, data: [{user: /^[0-9a-zA-z]*$/i}, {user: /^[0-9a-zA-z]*$/gi}]},
                    {editor: new User('Albert'), data: [{user: new User('Lin')}, {user: new User('Albert')}]},
                    {editor: [], data: [{user: [1, 2, 3]}, {user: []}]},
                    {editor: {}, data: [{user: {index: 3}}, {user: {}}]},
                    {editor: undefined, data: [{user: undefined}, {user: 'null'}]},
                    {editor: null, data: [{user: null}, {user: 'undefined'}]},
                ];

                testCases.forEach((testCase, index) => {
                    testHelper({
                        parentPath: ['data', '*'],
                        fieldName: 'user',
                        ruleWithOptions: 'different:editor',
                        isNullable: false,
                        presentOnly: false
                    }, [testCase], expects[index]);
                });
            });
        });

        describe('validate field is not array, fields in rule is array', () => {
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['data']] };
                const testCases = [
                    {
                        data: 41,
                        valid: [],
                    },
                    {
                        data: 41,
                        valid: [{data: 41}, {data: 42}, {data: 41}],
                    },
                    {
                        data: 41,
                        valid: [{}, {}, {}],
                    },
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'data',
                    ruleWithOptions: 'different:valid.*.data',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
    });
});
