import testHelper from "./testHelper";

describe('wrappers.uuid', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {id: 'cf641482-399d-11e9-b210-d663bd873d93'},
                {id: 'cf64170c-399d-11e9-b210-d663bd873d93'},
                {id: '8470b099-6d95-4dd5-b74b-adfc4598437b'},
                {id: 'a02e3b0b-c3b8-4f36-8962-5f3d27122a99'},
            ];
            const nullTestCases = [
                {id: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['id']] };
            const testCases = [
                {id: 'a02e3b0b-c3b8-xf36-8962-5f3d27122a99'},
                {id: -123.456},
                {id: 'id'},
                {id: true},
                {id: new Date('2019-03-01')},
                {id: /^[0-9][a-z]*$/i},
                {id: {}},
                {id: []},
                {id: undefined},
                {id: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: {user: {id: 'cf641482-399d-11e9-b210-d663bd873d93'}}},
                {update: {user: {id: 'cf64170c-399d-11e9-b210-d663bd873d93'}}},
                {update: {user: {id: '8470b099-6d95-4dd5-b74b-adfc4598437b'}}},
                {update: {user: {id: 'a02e3b0b-c3b8-4f36-8962-5f3d27122a99'}}},
            ];
            const nullTestCases = [
                {update: {user: {id: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', 'user', 'id']] };
            const testCases = [
                {update: {user: {id: '8470b099-6d95-4dd5-adfc4598437b'}}},
                {update: {user: {id: 'id'}}},
                {update: {user: {id: 123.456}}},
                {update: {user: {id: false}}},
                {update: {user: {id: new Date('2019-03-01')}}},
                {update: {user: {id: /^[0-9][a-z]*$/i}}},
                {update: {user: {id: {}}}},
                {update: {user: {id: []}}},
                {update: {user: {id: undefined}}},
                {update: {user: {id: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: 'a02e3b0b-c3b8-4f36-8962-5f3d27122a99'}]},
                {update: [{id: '8470b099-6d95-4dd5-b74b-adfc4598437b'}, {id: 'cf64170c-399d-11e9-b210-d663bd873d93'}]},
            ];
            const nullTestCases = [
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: null}]},
                {update: [{id: null}, {id: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', '1', 'id']] };
            const testCases = [
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: 'cf64170c-399d-11e9-b210-d663bd3d93'}]},
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: 'id'}]},
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: -0.4568}]},
                {update: [{id: '8470b099-6d95-4dd5-b74b-adfc4598437b'}, {id: false}]},
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: new Date('2019-03-01')}]},
                {update: [{id: '8470b099-6d95-4dd5-b74b-adfc4598437b'}, {id: /^[0-9][a-z]*$/i}]},
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: []}]},
                {update: [{id: '8470b099-6d95-4dd5-b74b-adfc4598437b'}, {id: {}}]},
                {update: [{id: 'cf641482-399d-11e9-b210-d663bd873d93'}, {id: undefined}]},
                {update: [{id: '8470b099-6d95-4dd5-b74b-adfc4598437b'}, {id: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'id',
                ruleWithOptions: 'uuid',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});