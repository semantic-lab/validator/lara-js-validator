import testHelper from "./testHelper";

describe('wrappers.string', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {name: ''},
                {name: 'Dirk'},
                {name: 'Dirk Nowitzki'},
                {name: 'Roger Federer'},
            ];
            const nullTestCases = [
                {name: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['name']] };
            const testCases = [
                {name: -123.456},
                {name: true},
                {name: new Date('2019-03-01')},
                {name: /^[0-9][a-z]*$/i},
                {name: {}},
                {name: []},
                {name: undefined},
                {name: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: {friend: {name: ''}}},
                {update: {friend: {name: 'Roger'}}},
                {update: {friend: {name: 'Roger Federer'}}},
                {update: {friend: {name: 'Dirk Nowitzki'}}},
            ];
            const nullTestCases = [
                {update: {friend: {name: null}}},
            ];

            testHelper({
                parentPath: ['update', 'friend'],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', 'friend'],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', 'friend', 'name']] };
            const testCases = [
                {update: {friend: {name: 123.456}}},
                {update: {friend: {name: false}}},
                {update: {friend: {name: new Date('2019-03-01')}}},
                {update: {friend: {name: /^[0-9][a-z]*$/i}}},
                {update: {friend: {name: {}}}},
                {update: {friend: {name: []}}},
                {update: {friend: {name: undefined}}},
                {update: {friend: {name: null}}},
            ];

            testHelper({
                parentPath: ['update', 'friend'],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: [{name: ''}, {name: 'Dirk Nowitzki'}, {name: 'Dirk41'}]},
                {update: [{name: 'Roger100'}, {name: 'Roger Federer'}, {name: ''}]},
            ];
            const nullTestCases = [
                {update: [{name: ''}, {name: 'Dirk Nowitzki'}, {name: null}]},
                {update: [{name: null}, {name: null}, {name: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', '2', 'name']] };
            const testCases = [
                {update: [{name: 'Dirk41'}, {name: ''}, {name: -0.4568}]},
                {update: [{name: 'Roger100'}, {name: ''}, {name: false}]},
                {update: [{name: 'Dirk41'}, {name: ''}, {name: new Date('2019-03-01')}]},
                {update: [{name: 'Roger100'}, {name: ''}, {name: /^[0-9][a-z]*$/i}]},
                {update: [{name: 'Dirk41'}, {name: ''}, {name: []}]},
                {update: [{name: 'Roger100'}, {name: ''}, {name: {}}]},
                {update: [{name: 'Dirk41'}, {name: ''}, {name: undefined}]},
                {update: [{name: 'Roger100'}, {name: ''}, {name: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'name',
                ruleWithOptions: 'string',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});