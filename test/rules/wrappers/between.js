import testHelper from "./testHelper";

describe('wrappers.between', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {mainDish: 3},
                {mainDish: 5},
                {mainDish: 6},
                {mainDish: '123'},
                {mainDish: 'ab-cde'},
                {mainDish: [0, 1, 2]},
                {mainDish: [0, 'A', 2, 'B', 3, 'C']},
            ];
            const nullTestCases = [
                {mainDish: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'between:3,6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'between:3,6',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['mainDish']] };
            const testCases = [
                {mainDish: 2.9},
                {mainDish: 6.01},
                {mainDish: 'D1'},
                {mainDish: 'a1-b2-c3'},
                {mainDish: ['dish0']},
                {mainDish: [0, 'A', 2, 'B', 3, 'C', 4, 'D']},
                {mainDish: undefined},
                {mainDish: true},
                {mainDish: {}},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'between:3,6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {aLaCaret: {dish: 3}}},
                {order: {aLaCaret: {dish: 6}}},
                {order: {aLaCaret: {dish: 'a-c'}}},
                {order: {aLaCaret: {dish: 'AB!123'}}},
                {order: {aLaCaret: {dish: ['dish0', 'dish1', 'dish2']}}},
                {order: {aLaCaret: {dish: [0, 'A', 1, 'b', {}, 2]}}},
            ];
            const nullTestCases = [
                {order: {aLaCaret: {dish: null}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'between:3,6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'between:3,6',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'aLaCaret', 'dish']] };
            const testCases = [
                {order: {aLaCaret: {dish: 2.99}}},
                {order: {aLaCaret: {dish: 6.0001}}},
                {order: {aLaCaret: {dish: 'D1'}}},
                {order: {aLaCaret: {dish: 'more then 6 chars'}}},
                {order: {aLaCaret: {dish: []}}},
                {order: {aLaCaret: {dish: [0, 'A', 1, 'b', {}, 2, []]}}},
                {order: {aLaCaret: {dish: undefined}}},
                {order: {aLaCaret: {dish: null}}},
                {order: {aLaCaret: {dish: {}}}},
                {order: {aLaCaret: {dish: false}}},
                {order: {aLaCaret: {}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'between:3,6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {aLaCaret: [{dish: 3}, {dish: 4}, {dish: 5}]},
                {aLaCaret: [{dish: 4}, {dish: 5}, {dish: 6}]},
                {aLaCaret: [{dish: '123'}, {dish: '1234'}, {dish: '12345'}]},
                {aLaCaret: [{dish: '1234'}, {dish: '12345'}, {dish: '123456'}]},
                {aLaCaret: [{dish: [0, 1, 2]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1, 'c', {}]}]},
                {aLaCaret: [{dish: ['a', 'b', 'c', 'd']}, {dish: [0, 'B', 1, 'c', []]}, {dish: [0, 'B', {}, 1, [], {}]}]},
            ];
            const nullTestCases = [
                {aLaCaret: [{dish: 3}, {dish: 4}, {dish: null}]},
                {aLaCaret: [{dish: null}, {dish: null}, {dish: null}]},
            ];

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'between:3,6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'between:3,6',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['aLaCaret', '0', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '0', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '0', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
            ];
            const testCases = [
                {aLaCaret: [{dish: 2}, {dish: 4}, {dish: 5}]},
                {aLaCaret: [{dish: 4}, {dish: 5}, {dish: 7}]},
                {aLaCaret: [{dish: '12'}, {dish: '1234'}, {dish: '12345'}]},
                {aLaCaret: [{dish: '1234'}, {dish: '12345'}, {dish: '1234567'}]},
                {aLaCaret: [{dish: [0, 1]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1, 'c', {}]}]},
                {aLaCaret: [{dish: ['a', 'b', 'c']}, {dish: [0, 'B', 1]}, {dish: [1, 2, 3, 4, 5, 6, 7]}]},
                {aLaCaret: [{dish: 4}, {dish: '12345'}, {dish: undefined}]},
                {aLaCaret: [{dish: 4}, {dish: '12345'}, {dish: null}]},
                {aLaCaret: [{dish: 4}, {dish: '12345'}, {dish: {}}]},
                {aLaCaret: [{dish: 4}, {dish: '12345'}, {dish: false}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['aLaCaret', '*'],
                    fieldName: 'dish',
                    ruleWithOptions: 'between:3,6',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});