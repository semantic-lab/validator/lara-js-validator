import testHelper from './testHelper';

describe('wrappers.alpha', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {title: 'alpha'},
                {title: 'string'},
                {title: 'Str'},
                {title: 'DevOps'},
                {title: 'QA'},
            ];
            const nullTestCases = [
                {title: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['title']] };
            const testCases = [
                {title: '1st alpha'},
                {title: 41},
                {title: 'Dr. CHIANG'},
                {title: 'Dirk Nowitzki'},
                {title: []},
                {title: {}},
                {title: undefined},
                {title: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: {name: {firstName: 'dirk'}}},
                {user: {name: {firstName: 'ROGER'}}},
            ];
            const nullTestCases = [
                {user: {name: {firstName: null}}},
            ];

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', 'name', 'firstName']] };
            const testCases = [
                {user: {name: {firstName: 'Dr.CHIANG'}}},
                {user: {name: {firstName: 'Dirk41'}}},
                {user: {name: {firstName: []}}},
                {user: {name: {firstName: {}}}},
                {user: {name: {firstName: undefined}}},
                {user: {name: {firstName: null}}},
            ];

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{title: 'DevOps'}, {title: 'RD'}, {title: 'Doctor'}]},
            ];
            const nullTestCases = [
                {user: [{title: 'DevOps'}, {title: 'RD'}, {title: null}]},
                {user: [{title: null}, {title: null}, {title: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'title',
                ruleWithOptions: 'alpha',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'title',
                ruleWithOptions: 'alpha',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['user', '0', 'title']] },
                { result: false, fail: [['user', '1', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
            ];
            const testCases = [
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: 'Doctor'}]},
                {user: [{title: 'DevOps'}, {title: 'R&D'}, {title: 'Doctor'}]},
                {user: [{title: 'DevOps'}, {title: 'RD'}, {title: 'Dr CHIANG'}]},
                {user: [{title: 'DevOps'}, {title: 'RD'}, {title: []}]},
                {user: [{title: 'DevOps'}, {title: 'RD'}, {title: {}}]},
                {user: [{title: 'DevOps'}, {title: 'RD'}, {title: undefined}]},
                {user: [{title: 'DevOps'}, {title: 'RD'}, {title: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['user', '*'],
                    fieldName: 'title',
                    ruleWithOptions: 'alpha',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});