import testHelper from "./testHelper";

describe('wrappers.required', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'success message'},
                {data: 200},
                {data: false},
                {data: /^[0-9]*$/i},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {data: []},
                {data: {}},
                {data: undefined},
                {data: null},
                true,
                41,
                {},
                [],
                undefined,
                null,
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {environment: {data: 'success message'}}},
                {system: {environment: {data: 200}}},
                {system: {environment: {data: false}}},
                {system: {environment: {data: /^[0-9]*$/i}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {environment: {data: []}}},
                {system: {environment: {data: {}}}},
                {system: {environment: {data: undefined}}},
                {system: {environment: {data: null}}},
                {system: {environment: true}},
                {system: {environment: 41}},
                {system: {environment: {}}},
                {system: {environment: []}},
                {system: {environment: undefined}},
                {system: {environment: null}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {config: [{data: 'success message'}, {data: 200}, {data: false}, {data: /^[0-9]*$/i}]},
            ];

            testHelper({
                parentPath: ['config', '*'],
                fieldName: 'data',
                ruleWithOptions: 'required',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['config', '0', 'data']] },
                { result: false, fail: [['config', '0', 'data']] },
                { result: false, fail: [['config', '0', 'data']] },
                { result: false, fail: [['config', '0', 'data']] },
                { result: false, fail: [['config', '1', 'data']] },
                { result: false, fail: [['config', '1', 'data']] },
                { result: false, fail: [['config', '1', 'data']] },
                { result: false, fail: [['config', '1', 'data']] },
                { result: false, fail: [['config', '*', 'data']] },
                { result: false, fail: [['config', '*', 'data']] },
            ];
            const testCases = [
                {config: [{data: []}, {data: 'success message'}, {data: 200}]},
                {config: [{data: {}}, {data: 'success message'}, {data: 200}]},
                {config: [{data: undefined}, {data: 'success message'}, {data: 200}]},
                {config: [{data: null}, {data: 'success message'}, {data: 200}]},
                {config: [{data: 'success message'}, true, {data: 200}]},
                {config: [{data: 'success message'}, 41, {data: 200}]},
                {config: [{data: 'success message'}, {}, {data: 200}]},
                {config: [{data: 'success message'}, [], {data: 200}]},
                {config: [{data: 'success message'}, {data: 200}, undefined]},
                {config: [{data: 'success message'}, {data: 200}, null]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['config', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required',
                    isNullable: true,
                    presentOnly: true
                }, [testCase], expects[index]);
            });
        });
    });
});
