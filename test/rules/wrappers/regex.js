import testHelper from "./testHelper";

describe('wrappers.regex', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {phone: '0900000000'},
                {phone: '0911000002'},
                {phone: '0928102345'},
                {phone: '0975283746'},
                {phone: '0911111111'},
            ];
            const nullTestCases = [
                {phone: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['phone']] };
            const testCases = [
                {phone: '0911'},
                {phone: '08123456787'},
                {phone: 'abc'},
                {phone: '091234567A'},
                {phone: 'y123409837'},
                {phone: 0},
                {phone: 41},
                {phone: {}},
                {phone: []},
                {phone: undefined},
                {phone: null},
                {phone: new Date('2019-03-01')},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {user: {phone: '0928093456'}}},
                {order: {user: {phone: '0911234724'}}},
                {order: {user: {phone: '0900000000'}}},
            ];
            const nullTestCases = [
                {order: {user: {phone: null}}},
            ];

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'user', 'phone']] };
            const testCases = [
                {order: {user: {phone: '0928'}}},
                {order: {user: {phone: '0811234724'}}},
                {order: {user: {phone: 'A900000000'}}},
                {order: {user: {phone: 928317939}}},
                {order: {user: {phone: true}}},
                {order: {user: {phone: {}}}},
                {order: {user: {phone: []}}},
                {order: {user: {phone: undefined}}},
                {order: {user: {phone: null}}},
            ];

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: '0900000000'}]},
            ];
            const nullTestCases = [
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: null}]},
                {user: [{phone: null}, {phone: null}, {phone: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'phone',
                ruleWithOptions: 'regex:/^09\\d{8}$/i',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['user', '0', 'phone']] },
                { result: false, fail: [['user', '1', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
                { result: false, fail: [['user', '2', 'phone']] },
            ];
            const testCases = [
                {user: [{phone: '091203745'}, {phone: '0911000000'}, {phone: '0900000000'}]},
                {user: [{phone: '0912037456'}, {phone: 'A911000000'}, {phone: '0900000000'}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: '0900-00000'}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: 89}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: true}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: []}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: {}}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: undefined}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: null}]},
                {user: [{phone: '0912037456'}, {phone: '0911000000'}, {phone: new Date('2019-03-03')}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['user', '*'],
                    fieldName: 'phone',
                    ruleWithOptions: 'regex:/^09\\d{8}$/i',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});
