import testHelper from "./testHelper";

describe('wrappers.confirmed', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {password: 'A12bc', password_confirmation: 'A12bc'},
                {password: 0.41, password_confirmation: 0.41},
                {password: false, password_confirmation: false},
                {password: {index: 1}, password_confirmation: {index: 1}},
                {password: /^[0-9a-zA-z]*$/i, password_confirmation: /^[0-9a-zA-z]*$/i},
                {password: [], password_confirmation: []},
                {password: {}, password_confirmation: {}},
                {password: undefined, password_confirmation: undefined},
                {password: null, password_confirmation: null},
            ];
            const nullTestCases = [
                {password: null, password_confirmation: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['password']] };
            const testCases = [
                {password: 'A12bc', password_confirmation: 'a1-2bc'},
                {password: 0.41, password_confirmation: 4.1},
                {password: false, password_confirmation: true},
                {password: {index: 1}, password_confirmation: {index: 2}},
                {password: /^[0-9a-zA-z]*$/i, password_confirmation: /^[0-9]*$/i},
                {password: [], password_confirmation: [1, 2, 3]},
                {password: {}, password_confirmation: {index: 2}},
                {password: undefined, password_confirmation: null},
                {password: null, password_confirmation: undefined},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: {user: {password: 'A12bc', password_confirmation: 'A12bc'}}},
                {data: {user: {password:  0.41, password_confirmation:  0.41}}},
                {data: {user: {password: false, password_confirmation: false}}},
                {data: {user: {password: {index: 1}, password_confirmation: {index: 1}}}},
                {data: {user: {password: /^[0-9a-zA-z]*$/i, password_confirmation: /^[0-9a-zA-z]*$/i}}},
                {data: {user: {password: [], password_confirmation: []}}},
                {data: {user: {password: {}, password_confirmation: {}}}},
                {data: {user: {password: undefined, password_confirmation: undefined}}},
                {data: {user: {password: null, password_confirmation: null}}},
            ];
            const nullTestCases = [
                {data: {user: {password: null, password_confirmation: null}}},
            ];

            testHelper({
                parentPath: ['data', 'user'],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['data', 'user'],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data', 'user', 'password']] };
            const testCases = [
                {data: {user: {password: 'A12bc', password_confirmation: 'a12bc'}}},
                {data: {user: {password: 0.41, password_confirmation: 0.1}}},
                {data: {user: {password: false, password_confirmation: true}}},
                {data: {user: {password: {index: 1}, password_confirmation: {index: 2}}}},
                {data: {user: {password: /^[0-9a-zA-z]*$/i, password_confirmation: /^[0-9a-zA-z]*/i}}},
                {data: {user: {password: [], password_confirmation: [1, 2]}}},
                {data: {user: {password: undefined, password_confirmation: null}}},
                {data: {user: {password: null, password_confirmation: undefined}}},
            ];

            testHelper({
                parentPath: ['data', 'user'],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: [{password: 'A12bc', password_confirmation: 'A12bc'}, {password: 'A12bc', password_confirmation: 'A12bc'}]},
                {data: [{password: 0.41, password_confirmation: 0.41}, {password: 0.41, password_confirmation: 0.41}]},
                {data: [{password: false, password_confirmation: false}, {password: false, password_confirmation: false}]},
                {data: [{password: {index: 1}, password_confirmation: {index: 1}}, {password: {index: 1}, password_confirmation: {index: 1}}]},
                {data: [{password: /^[0-9a-zA-z]*$/i, password_confirmation: /^[0-9a-zA-z]*$/i}, {password: /^[0-9a-zA-z]*$/i, password_confirmation: /^[0-9a-zA-z]*$/i}]},
                {data: [{password: [], password_confirmation: []}, {password: [], password_confirmation: []}]},
                {data: [{password: {}, password_confirmation: {}}, {password: {}, password_confirmation: {}}]},
                {data: [{password: undefined, password_confirmation: undefined}, {password: undefined, password_confirmation: undefined}]},
                {data: [{password: null, password_confirmation: null}, {password: null, password_confirmation: null}]},
            ];
            const nullTestCases = [
                {data: [{password: null, password_confirmation: null}, {password: null, password_confirmation: null}]},
            ];

            testHelper({
                parentPath: ['data', '*'],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['data', '*'],
                fieldName: 'password',
                ruleWithOptions: 'confirmed',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['data', '1', 'password']] },
                { result: false, fail: [['data', '0', 'password']] },
                { result: false, fail: [['data', '0', 'password']] },
                { result: false, fail: [['data', '1', 'password']] },
                { result: false, fail: [['data', '1', 'password']] },
                { result: false, fail: [['data', '0', 'password']] },
                { result: false, fail: [['data', '0', 'password']] },
                { result: false, fail: [['data', '1', 'password']] },
                { result: false, fail: [['data', '1', 'password']] },
            ];
            const testCases = [
                {data: [{password: 'A12bc', password_confirmation: 'A12bc'}, {password: 'a1-2bc', password_confirmation: 'A12bc'}]},
                {data: [{password: 4.1, password_confirmation: 0.41}, {password: 0.41, password_confirmation: 0.41}]},
                {data: [{password: true, password_confirmation: false}, {password: false, password_confirmation: false}]},
                {data: [{password: {index: 1}, password_confirmation: {index: 1}}, {password: {index: 2}, password_confirmation: {index: 1}}]},
                {data: [{password: /^[0-9a-zA-z]*$/i, password_confirmation: /^[0-9a-zA-z]*$/i}, {password: /^[0-9a-zA-z]*$/gi, password_confirmation: /^[0-9a-zA-z]*$/i}]},
                {data: [{password: [1, 2, 3], password_confirmation: []}, {password: [], password_confirmation: []}]},
                {data: [{password: {index: 3}, password_confirmation: {}}, {password: {}, password_confirmation: {}}]},
                {data: [{password: undefined, password_confirmation: undefined}, {password: null, password_confirmation: undefined}]},
                {data: [{password: null, password_confirmation: null}, {password: undefined, password_confirmation: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['data', '*'],
                    fieldName: 'password',
                    ruleWithOptions: 'confirmed',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});