import testHelper from "./testHelper";

describe('wrappers.numeric', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {price: -123.456},
                {price: -0.999999999999991},
                {price: 0},
                {price: 1.0000000000000001},
                {price: 999999999999999999},
            ];
            const nullTestCases = [
                {price: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['price']] };
            const testCases = [
                {price: 'user'},
                {price: true},
                {price: new Date('2019-03-01')},
                {price: /^[0-9][a-z]*$/i},
                {price: {}},
                {price: []},
                {price: undefined},
                {price: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {dish: {price: -123.456}}},
                {order: {dish: {price: -0.999999999999991}}},
                {order: {dish: {price: 0}}},
                {order: {dish: {price: 1.0000000000000001}}},
                {order: {dish: {price: 999999999999999999}}},
            ];
            const nullTestCases = [
                {order: {dish: {price: null}}},
            ];

            testHelper({
                parentPath: ['order', 'dish'],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'dish'],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'dish', 'price']] };
            const testCases = [
                {order: {dish: {price: 'user'}}},
                {order: {dish: {price: false}}},
                {order: {dish: {price: new Date('2019-03-01')}}},
                {order: {dish: {price: /^[0-9][a-z]*$/i}}},
                {order: {dish: {price: {}}}},
                {order: {dish: {price: []}}},
                {order: {dish: {price: undefined}}},
                {order: {dish: {price: null}}},
            ];

            testHelper({
                parentPath: ['order', 'dish'],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {dish: [{price: -123.456}, {price: -0.999999999999991}, {price: 0}]},
                {dish: [{price: 0}, {price: 1.0000000000000001}, {price: 999999999999999999}]},
            ];
            const nullTestCases = [
                {dish: [{price: -123.456}, {price: -0.999999999999991}, {price: null}]},
                {dish: [{price: null}, {price: null}, {price: null}]},
            ];

            testHelper({
                parentPath: ['dish', '*'],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['dish', '*'],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['dish', '2', 'price']] };
            const testCases = [
                {dish: [{price: -41}, {price: 0}, {price: 'user'}]},
                {dish: [{price: -41}, {price: 0}, {price: false}]},
                {dish: [{price: -41}, {price: 0}, {price: new Date('2019-03-01')}]},
                {dish: [{price: -41}, {price: 0}, {price: /^[0-9][a-z]*$/i}]},
                {dish: [{price: -41}, {price: 0}, {price: []}]},
                {dish: [{price: -41}, {price: 0}, {price: {}}]},
                {dish: [{price: -41}, {price: 0}, {price: undefined}]},
                {dish: [{price: -41}, {price: 0}, {price: null}]},
            ];

            testHelper({
                parentPath: ['dish', '*'],
                fieldName: 'price',
                ruleWithOptions: 'numeric',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});