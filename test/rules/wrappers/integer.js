import testHelper from "./testHelper";

describe('wrappers.integer', () => {
    const rule = 'integer';
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {price: -123},
                {price: -1},
                {price: 0},
                {price: 1},
                {price: 999999999999999999},
            ];
            const nullTestCases = [
                {price: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['price']] };
            const testCases = [
                {price: -123.456},
                {price: 456.852},
                {price: 'user'},
                {price: true},
                {price: new Date('2019-03-01')},
                {price: /^[0-9][a-z]*$/i},
                {price: {}},
                {price: []},
                {price: undefined},
                {price: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {dish: {price: -123}}},
                {order: {dish: {price: -1}}},
                {order: {dish: {price: 0}}},
                {order: {dish: {price: 1}}},
                {order: {dish: {price: 999999999999999999}}},
            ];
            const nullTestCases = [
                {order: {dish: {price: null}}},
            ];

            testHelper({
                parentPath: ['order', 'dish'],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'dish'],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'dish', 'price']] };
            const testCases = [
                {order: {dish: {price: 123.456}}},
                {order: {dish: {price: 852.9}}},
                {order: {dish: {price: 'user'}}},
                {order: {dish: {price: false}}},
                {order: {dish: {price: new Date('2019-03-01')}}},
                {order: {dish: {price: /^[0-9][a-z]*$/i}}},
                {order: {dish: {price: {}}}},
                {order: {dish: {price: []}}},
                {order: {dish: {price: undefined}}},
                {order: {dish: {price: null}}},
            ];

            testHelper({
                parentPath: ['order', 'dish'],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {dish: [{price: -123}, {price: -1}, {price: 0}]},
                {dish: [{price: 0}, {price: 1}, {price: 999999999999999999}]},
            ];
            const nullTestCases = [
                {dish: [{price: 0}, {price: 1}, {price: null}]},
                {dish: [{price: null}, {price: null}, {price: null}]},
            ];

            testHelper({
                parentPath: ['dish', '*'],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['dish', '*'],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['dish', '2', 'price']] };
            const testCases = [
                {dish: [{price: -41}, {price: 0}, {price: -0.4568}]},
                {dish: [{price: -41}, {price: 0}, {price: 3.1415}]},
                {dish: [{price: -41}, {price: 0}, {price: 'user'}]},
                {dish: [{price: -41}, {price: 0}, {price: false}]},
                {dish: [{price: -41}, {price: 0}, {price: new Date('2019-03-01')}]},
                {dish: [{price: -41}, {price: 0}, {price: /^[0-9][a-z]*$/i}]},
                {dish: [{price: -41}, {price: 0}, {price: []}]},
                {dish: [{price: -41}, {price: 0}, {price: {}}]},
                {dish: [{price: -41}, {price: 0}, {price: undefined}]},
                {dish: [{price: -41}, {price: 0}, {price: null}]},
            ];

            testHelper({
                parentPath: ['dish', '*'],
                fieldName: 'price',
                ruleWithOptions: 'integer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});