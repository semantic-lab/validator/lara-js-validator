import testHelper from "./testHelper";

describe('wrappers.max', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {mainDish: 5},
                {mainDish: 6},
                {mainDish: '410'},
                {mainDish: ['dish0', 'dish1', 'dish2']},
            ];
            const nullTestCases = [
                {mainDish: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'max:6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'max:6',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['mainDish']] };
            const testCases = [
                {mainDish: 7},
                {mainDish: '6 chars'},
                {mainDish: [0, 1, 2, 3, 4, 5, 6]},
                {mainDish: undefined},
                {mainDish: null},
                {mainDish: {}},
                41,
                'ABC',
                true,
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'max:6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {aLaCaret: {dish: 5}}},
                {order: {aLaCaret: {dish: 6}}},
                {order: {aLaCaret: {dish: 'abcd'}}},
                {order: {aLaCaret: {dish: 'abcdef'}}},
                {order: {aLaCaret: {dish: ['dish0', 'dish1']}}},
            ];
            const nullTestCases = [
                {order: {aLaCaret: {dish: null}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'max:6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'max:6',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'aLaCaret', 'dish']] };
            const testCases = [
                {order: {aLaCaret: {dish: 410}}},
                {order: {aLaCaret: {dish: 'chars more then 6'}}},
                {order: {aLaCaret: {dish: [0, 1, 2, 3, 4, 5, 6]}}},
                {order: {aLaCaret: {dish: undefined}}},
                {order: {aLaCaret: {dish: 120}}},
                {order: {aLaCaret: {dish: {}}}},
                {order: {aLaCaret: {dish: {}}}},
                {order: {aLaCaret: 41}},
                {order: {aLaCaret: true}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'max:6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {aLaCaret: [{dish: 6}, {dish: 5}, {dish: 0}]},
                {aLaCaret: [{dish: 'Abcde'}, {dish: '6chars'}, {dish: 'abc'}]},
                {aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}]},
            ];
            const nullTestCases = [
                {aLaCaret: [{dish: 6}, {dish: 5}, {dish: null}]},
                {aLaCaret: [{dish: null}, {dish: null}, {dish: null}]},
            ];

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'max:6',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'max:6',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['aLaCaret', '0', 'dish']] },
                { result: false, fail: [['aLaCaret', '0', 'dish']] },
                { result: false, fail: [['aLaCaret', '1', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
            ];
            const testCases = [
                {aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}]},
                {aLaCaret: [{dish: '7 chars'}, {dish: 'Abcd'}, {dish: 'abc'}]},
                {aLaCaret: [{dish: 5}, {dish: [0, 1, 2, 3, 4, 5, 6]}]},
                {aLaCaret: [{dish: 4}, {dish: 3}, {dish: '7 chars'}]},
                {aLaCaret: [{dish: 3}, {dish: 2}, {dish: 'more then 6 chars'}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['aLaCaret', '*'],
                    fieldName: 'dish',
                    ruleWithOptions: 'max:6',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});