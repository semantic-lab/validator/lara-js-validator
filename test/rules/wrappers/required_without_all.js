import testHelper from "./testHelper";

describe('wrappers.required_without_all', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'success message'},
                {data: 200},
                {data: false},
                {data: /^[0-9]*$/i},
                {data: [], error: true, errorMessage: true, authFail: true},
                {data: {}, error: true, errorMessage: true},
                {data: undefined, error: true, authFail: true},
                {data: null, errorMessage: true, authFail: true},
                {data: null, error: true},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_without_all:error,errorMessage,authFail',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {data: []},
                {data: {}},
                {data: undefined},
                {data: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_without_all:error,errorMessage,authFail',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {environment: {data: 'success message'}}, api: {}},
                {system: {environment: {data: 200}}, api: {}},
                {system: {environment: {data: false}}, api: {}},
                {system: {environment: {data: /^[0-9]*$/i}}, api: {}},
                {system: {environment: {data: []}}, api: {error: true, errorMessage: true}},
                {system: {environment: {data: {}}}, api: {error: true, errorMessage: true}},
                {system: {environment: {data: undefined}}, api: {error: true, errorMessage: true}},
                {system: {environment: {data: null}}, api: {error: true, errorMessage: true}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_without_all:api.error,api.errorMessage',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {environment: {data: []}}, api: {}},
                {system: {environment: {data: {}}}, api: {}},
                {system: {environment: {data: undefined}}},
                {system: {environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_without_all:api.error,api.errorMessage',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('both array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: 200},
                            {data: false},
                            {data: /^[0-9]*$/i}
                        ]},
                    {system: [
                            {data: 'success message', error: false, errorMessage: false},
                            {data: '', error: undefined, errorMessage: undefined},
                            {data: [], error: undefined, errorMessage: undefined},
                            {data: undefined, error: undefined, errorMessage: undefined},
                            {data: null, error: undefined, errorMessage: undefined},
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without_all:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['system', '1', 'data']] };
                const testCases = [
                    {system: [{data: 'success message'}, {data: ''}]},
                    {system: [{data: 'success message'}, {data: []}]},
                    {system: [{data: 'success message'}, {data: {}}]},
                    {system: [{data: 'success message'}, {data: undefined} ]},
                    {system: [{data: 'success message'}, {data: null}]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without_all:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
        describe('fields in rule is not array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: 200},
                            {data: false},
                            {data: /^[0-9]*$/i}
                        ]},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: null},
                            {data: undefined}
                        ], error: false, errorMessage: false},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: null},
                            {data: undefined}
                        ], error: false},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without_all:error,errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['system', '1', 'data']] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: null},
                            {data: undefined}
                        ]},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: null},
                            {data: undefined}
                        ]},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: null},
                            {data: undefined}
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without_all:error,errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
        describe('validate field is not array, fields in rule is array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {data: 'success message', system: [{error: false, errorMessage: false}, {error: false, errorMessage: false}]},
                    {data: 200, system: [{error: false, errorMessage: false}, {error: false, errorMessage: false}]},
                    {data: false},
                    {data: /^[0-9]*$/i},
                    {data: 'success message'},
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without_all:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['data']] };
                const testCases = [
                    {data: [], system: [{}, {}]},
                    {data: {}, system: [{}, {}]},
                    {data: undefined, system: []},
                    {data: null, system: []},
                    {data: []},
                    {data: {}},
                    {data: undefined},
                    {data: null},
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without_all:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
    });
    describe('no field in rule', () => {
        const expect = { result: false, fail: [['data']] };
        const testCases = [
            {data: ''},
            {data: {}},
            {data: []},
            {data: null},
            {data: undefined},
            {},
            [],
            '',
            41,
            undefined,
            null,
        ];

        testHelper({
            parentPath: [],
            fieldName: 'data',
            ruleWithOptions: 'required_without_all',
            isNullable: false,
            presentOnly: false
        }, testCases, expect);
    });
});
