import testHelper from "./testHelper";

describe('wrappers.lt', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {mainDish: 410, appetizer: 80},
                {mainDish: '410', appetizer: '80'},
                {mainDish: ['dish0', 'dish1', 'dish2'], appetizer: ['snack0', 'snack1']},
            ];
            const nullTestCases = [
                {mainDish: 410, appetizer: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'appetizer',
                ruleWithOptions: 'lt:mainDish',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'appetizer',
                ruleWithOptions: 'lt:mainDish',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['appetizer']] };
            const testCases = [
                {mainDish: 120, appetizer: 240},
                {mainDish: '6chars', appetizer: '7 chars'},
                {mainDish: ['dish0'], appetizer: ['snack0', 'snack1']},
                {mainDish: 240, appetizer: '7 chars'},
                {mainDish: '7 chars', appetizer: ['dish0']},
                {mainDish: undefined, appetizer: 240},
                {mainDish: 120, appetizer: null},
                {mainDish: {}, appetizer: {}},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'appetizer',
                ruleWithOptions: 'lt:mainDish',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {aLaCaret: {dish: 800}, sideDish: {appetizer: 410}}},
                {order: {aLaCaret: {dish: 'abcd'}, sideDish: {appetizer: 'abc'}}},
                {order: {aLaCaret: {dish: ['dish0', 'dish1']}, sideDish: {appetizer: ['snack0']}}},
            ];
            const nullTestCases = [
                {order: {aLaCaret: {dish: 800}, sideDish: {appetizer: null}}},
            ];

            testHelper({
                parentPath: ['order', 'sideDish'],
                fieldName: 'appetizer',
                ruleWithOptions: 'lt:order.aLaCaret.dish',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'sideDish'],
                fieldName: 'appetizer',
                ruleWithOptions: 'lt:order.aLaCaret.dish',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'sideDish', 'appetizer']] };
            const testCases = [
                {order: {aLaCaret: {dish: 410}, sideDish: {appetizer: 411}}},
                {order: {aLaCaret: {dish: 'ab'}, sideDish: {appetizer: 'abc'}}},
                {order: {aLaCaret: {dish: []}, sideDish: {appetizer: ['snack0']}}},
                {order: {aLaCaret: {dish: 240}, sideDish: {appetizer: '7 chars'}}},
                {order: {aLaCaret: {dish: '7 chars'}, sideDish: {appetizer: ['snack0']}}},
                {order: {aLaCaret: {dish: undefined}, sideDish: {appetizer: 240}}},
                {order: {aLaCaret: {dish: 120}, sideDish: {appetizer: null}}},
                {order: {aLaCaret: {dish: {}}, sideDish: {appetizer: {}}}},
                {order: {aLaCaret: {dish: {}}, sideDish: {}}},
                {order: {aLaCaret: {}, sideDish: {appetizer: {}}}},
            ];

            testHelper({
                parentPath: ['order', 'sideDish'],
                fieldName: 'appetizer',
                ruleWithOptions: 'lt:order.aLaCaret.dish',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('both array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: 95}, {appetizer: 93}, {appetizer: 95}],
                    },
                    {
                        aLaCaret: [{dish: 'Abcde'}, {dish: 'Abcd'}, {dish: 'abc'}],
                        sideDish: [{appetizer: 'Fg'}, {appetizer: 'F'}, {appetizer: 'F'}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0]}, {appetizer: [0]}],
                    },
                ];
                const nullTestCases = [
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: null}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: null}, {appetizer: null}],
                    },
                ];

                testHelper({
                    parentPath: ['sideDish', '*'],
                    fieldName: 'appetizer',
                    ruleWithOptions: 'lt:aLaCaret.*.dish',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);

                testHelper({
                    parentPath: ['sideDish', '*'],
                    fieldName: 'appetizer',
                    ruleWithOptions: 'lt:aLaCaret.*.dish',
                    isNullable: true,
                    presentOnly: false
                }, nullTestCases, expect);
            });
            describe('expect [false]', () => {
                const expects = [
                    { result: false, fail: [['sideDish', '0', 'appetizer']] },
                    { result: false, fail: [['sideDish', '0', 'appetizer']] },
                    { result: false, fail: [['sideDish', '1', 'appetizer']] },
                    { result: false, fail: [['sideDish', '1', 'appetizer']] },
                    { result: false, fail: [['sideDish', '0', 'appetizer']] },
                    { result: false, fail: [['sideDish', '0', 'appetizer']] },
                    { result: false, fail: [['sideDish', '0', 'appetizer']] },
                    { result: false, fail: [['sideDish', '0', 'appetizer']] },
                    { result: false, fail: [['sideDish', '0', 'appetizer']] },
                ];
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: 101}, {appetizer: 93}],
                    },
                    {
                        aLaCaret: [{dish: 'Abcde'}, {dish: 'Abcd'}, {dish: 'abc'}],
                        sideDish: [{appetizer: '6chars'}, {appetizer: 'F'}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0, 1, 2, 3]}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: 100}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: 'appetizer value'}, {appetizer: [0, 1, 2, 3]}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: 'appetizer value'}, {appetizer: 0}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}],
                        sideDish: [{appetizer: undefined}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: null}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{}],
                    },
                ];

                testCases.forEach((testCase, index) => {
                    testHelper({
                        parentPath: ['sideDish', '*'],
                        fieldName: 'appetizer',
                        ruleWithOptions: 'lt:aLaCaret.*.dish',
                        isNullable: false,
                        presentOnly: false
                    }, [testCase], expects[index]);
                });
            });
        });
        describe('fields in rule is not array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        appetizer: 101
                    },
                    {
                        aLaCaret: [{dish: 'ABCDE'}, {dish: '123456'}, {dish: '12S_W-5!6'}],
                        appetizer: '0123456789'
                    },
                ];

                testHelper({
                    parentPath: ['aLaCaret', '*'],
                    fieldName: 'dish',
                    ruleWithOptions: 'lt:appetizer',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expects = [
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                    { result: false, fail: [['aLaCaret', '1', 'dish']] },
                ];
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 94}, {dish: 96}],
                        appetizer: 95
                    },
                    {
                        aLaCaret: [{dish: 'ABCDE'}, {dish: '123456'}, {dish: '12S_W-5!6'}],
                        appetizer: '012345'
                    },
                ];

                testCases.forEach((testCase, index) => {
                    testHelper({
                        parentPath: ['aLaCaret', '*'],
                        fieldName: 'dish',
                        ruleWithOptions: 'lt:appetizer',
                        isNullable: false,
                        presentOnly: false
                    }, [testCase], expects[index]);
                });
            });
        });
        describe('validate field is not array, fields in rule is array', () => {
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['dish']] };
                const testCases = [
                    {
                        dish: 100,
                        sideDish: [{appetizer: 95}, {appetizer: 93}, {appetizer: 95}],
                    },
                    {
                        dish: 'A123asd5',
                        sideDish: [{appetizer: 'Fg'}, {appetizer: 'F'}, {appetizer: 'FA'}],
                    },
                    {
                        dish: [0, 1, 2, 3],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0]}, {appetizer: ['A', 'C']}],
                    }
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'dish',
                    ruleWithOptions: 'lt:sideDish.*.appetizer',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
    });
    describe('no field in rule [false]', () => {
        const expect = { result: false, fail: [['data']] };
        const testCases = [
            {data: ''},
            {data: {}},
            {data: []},
            {data: null},
            {data: undefined},
        ];

        testHelper({
            parentPath: [],
            fieldName: 'data',
            ruleWithOptions: 'lt',
            isNullable: false,
            presentOnly: false
        }, testCases, expect);
    });
});
