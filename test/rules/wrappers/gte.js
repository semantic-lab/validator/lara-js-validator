import testHelper from "./testHelper";

describe('wrappers.gte', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {mainDish: 410, appetizer: 80},
                {mainDish: 410, appetizer: 410},
                {mainDish: '410', appetizer: '80'},
                {mainDish: '410', appetizer: 'ABC'},
                {mainDish: ['dish0', 'dish1', 'dish2'], appetizer: ['snack0', 'snack1']},
                {mainDish: ['dish0', 'dish1', 'dish2'], appetizer: ['snack0', 'snack1', 'snack2']},
            ];
            const nullTestCases = [
                {mainDish: null, appetizer: 80},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'gte:appetizer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'gte:appetizer',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['mainDish']] };
            const testCases = [
                {mainDish: 120, appetizer: 240},
                {mainDish: '6chars', appetizer: '7 chars'},
                {mainDish: ['dish0'], appetizer: ['snack0', 'snack1']},
                {mainDish: 240, appetizer: '7 chars'},
                {mainDish: '7 chars', appetizer: ['dish0']},
                {mainDish: undefined, appetizer: 240},
                {mainDish: 120, appetizer: null},
                {mainDish: {}, appetizer: {}},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'gte:appetizer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {aLaCaret: {dish: 800}, sideDish: {appetizer: 410}}},
                {order: {aLaCaret: {dish: 800}, sideDish: {appetizer: 800}}},
                {order: {aLaCaret: {dish: 'abcd'}, sideDish: {appetizer: 'abc'}}},
                {order: {aLaCaret: {dish: 'abcd'}, sideDish: {appetizer: 'ABCD'}}},
                {order: {aLaCaret: {dish: ['dish0', 'dish1']}, sideDish: {appetizer: ['snack0']}}},
                {order: {aLaCaret: {dish: ['dish0', 'dish1']}, sideDish: {appetizer: ['snack0', {}]}}},
            ];
            const nullTestCases = [
                {order: {aLaCaret: {dish: null}, sideDish: {appetizer: 410}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'gte:order.sideDish.appetizer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'gte:order.sideDish.appetizer',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'aLaCaret', 'dish']] };
            const testCases = [
                {order: {aLaCaret: {dish: 410}, sideDish: {appetizer: 411}}},
                {order: {aLaCaret: {dish: 'ab'}, sideDish: {appetizer: 'abc'}}},
                {order: {aLaCaret: {dish: []}, sideDish: {appetizer: ['snack0']}}},
                {order: {aLaCaret: {dish: 240}, sideDish: {appetizer: '7 chars'}}},
                {order: {aLaCaret: {dish: '7 chars'}, sideDish: {appetizer: ['snack0']}}},
                {order: {aLaCaret: {dish: undefined}, sideDish: {appetizer: 240}}},
                {order: {aLaCaret: {dish: 120}, sideDish: {appetizer: null}}},
                {order: {aLaCaret: {dish: {}}, sideDish: {appetizer: {}}}},
                {order: {aLaCaret: {dish: {}}, sideDish: {}}},
                {order: {aLaCaret: {}, sideDish: {appetizer: {}}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'gte:order.sideDish.appetizer',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('both array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: 95}, {appetizer: 93}, {appetizer: 96}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: 96}, {appetizer: 96}, {appetizer: 90}],
                    },
                    {
                        aLaCaret: [{dish: 'Abcde'}, {dish: 'Abcd'}, {dish: 'abc'}],
                        sideDish: [{appetizer: 'Fg'}, {appetizer: 'F'}, {appetizer: '123'}],
                    },
                    {
                        aLaCaret: [{dish: 'Abcde'}, {dish: 'Abcd'}, {dish: 'abc'}],
                        sideDish: [{appetizer: 'abc'}, {appetizer: '1234'}, {appetizer: 'abc'}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0]}, {appetizer: [0]}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0, 1, 2]}, {appetizer: [0, 1, 2]}],
                    },
                ];
                const nullTestCases = [
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: null}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0, 1, 2]}],
                    },
                    {
                        aLaCaret: [{dish: null}, {dish: null}, {dish: null}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0, 1, 2]}],
                    },
                ];

                testHelper({
                    parentPath: ['aLaCaret', '*'],
                    fieldName: 'dish',
                    ruleWithOptions: 'gte:sideDish.*.appetizer',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);

                testHelper({
                    parentPath: ['aLaCaret', '*'],
                    fieldName: 'dish',
                    ruleWithOptions: 'gte:sideDish.*.appetizer',
                    isNullable: true,
                    presentOnly: false
                }, nullTestCases, expect);
            });
            describe('expect [false]', () => {
                const expects = [
                    { result: false, fail: [['aLaCaret', '2', 'dish']] },
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                    { result: false, fail: [['aLaCaret', '2', 'dish']] },
                    { result: false, fail: [['aLaCaret', '1', 'dish']] },
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                ];
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: 97}, {appetizer: 93}],
                    },
                    {
                        aLaCaret: [{dish: 'Abcde'}, {dish: 'Abcd'}, {dish: 'abc'}],
                        sideDish: [{appetizer: '6chars'}, {appetizer: 'F'}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: [0, 1, 2, 3]}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: ['a', 1]}, {appetizer: 100}],
                    },
                    {
                        aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}],
                        sideDish: [{appetizer: 'appetizer value'}, {appetizer: [0, 1, 2, 3]}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: 'appetizer value'}, {appetizer: 0}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}],
                        sideDish: [{appetizer: undefined}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{appetizer: null}],
                    },
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        sideDish: [{}],
                    },
                ];

                testCases.forEach((testCase, index) => {
                    testHelper({
                        parentPath: ['aLaCaret', '*'],
                        fieldName: 'dish',
                        ruleWithOptions: 'gte:sideDish.*.appetizer',
                        isNullable: true,
                        presentOnly: false
                    }, [testCase], expects[index]);
                });
            });
        });
        describe('fields in rule is not array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        appetizer: 96
                    },
                    {
                        aLaCaret: [{dish: 'ABCDE'}, {dish: '123456'}, {dish: '12S_W-5!6'}],
                        appetizer: 'abcde'
                    },
                ];

                testHelper({
                    parentPath: ['aLaCaret', '*'],
                    fieldName: 'dish',
                    ruleWithOptions: 'gte:appetizer',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expects = [
                    { result: false, fail: [['aLaCaret', '2', 'dish']] },
                    { result: false, fail: [['aLaCaret', '0', 'dish']] },
                ];
                const testCases = [
                    {
                        aLaCaret: [{dish: 100}, {dish: 98}, {dish: 96}],
                        appetizer: 97
                    },
                    {
                        aLaCaret: [{dish: 'ABCDE'}, {dish: '123456'}, {dish: '12S_W-5!6'}],
                        appetizer: '0123456789'
                    },
                ];

                testCases.forEach((testCase, index) => {
                    testHelper({
                        parentPath: ['aLaCaret', '*'],
                        fieldName: 'dish',
                        ruleWithOptions: 'gte:appetizer',
                        isNullable: false,
                        presentOnly: false
                    }, [testCase], expects[index]);
                });
            });
        });
        describe('validate field is not array, fields in rule is array', () => {
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['dish']] };
                const testCases = [
                    {
                        dish: 100,
                        sideDish: [{appetizer: 95}, {appetizer: 93}, {appetizer: 101}],
                    },
                    {
                        dish: 'A123asd5',
                        sideDish: [{appetizer: 'Fg'}, {appetizer: 'A123asd56'}, {appetizer: 'FA'}],
                    },
                    {
                        dish: [0, 1, 2, 3],
                        sideDish: [{appetizer: ['a', 0, 1, 2, 3]}, {appetizer: [0]}, {appetizer: ['A', 'C']}],
                    }
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'dish',
                    ruleWithOptions: 'gte:sideDish.*.appetizer',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
    });
    describe('no field in rule [false]', () => {
        const expect = { result: false, fail: [['data']] };
        const testCases = [
            {data: ''},
            {data: {}},
            {data: []},
            {data: null},
            {data: undefined},
        ];
        testHelper({
            parentPath: [],
            fieldName: 'data',
            ruleWithOptions: 'gte',
            isNullable: false,
            presentOnly: false
        }, testCases, expect);
    });
});
