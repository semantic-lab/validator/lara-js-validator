import testHelper from "./testHelper";

describe('wrappers.in', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {orders: 'Big-Mac'},
                {orders: 'Coke-Cola'},
                {orders: 'Large-Fries'},
            ];
            const nullTestCases = [
                {orders: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['orders']] };
            const testCases = [
                {orders: 'big-Mac'},
                {orders: 'CokeCola'},
                {orders: 41},
                {orders: []},
                {orders: {}},
                {orders: undefined},
                {orders: null},
            ];
            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: {order: {meals: 'Big-Mac'}}},
                {user: {order: {meals: 'Coke-Cola'}}},
                {user: {order: {meals: 'Large-Fries'}}},
            ];
            const nullTestCases = [
                {user: {order: {meals: null}}},
            ];

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', 'order', 'meals']] };
            const testCases = [
                {user: {order: {meals: 'big-Mac'}}},
                {user: {order: {meals: 'CokeCola'}}},
                {user: {order: {meals: 41}}},
                {user: {order: {meals: []}}},
                {user: {order: {meals: {}}}},
                {user: {order: {meals: undefined}}},
                {user: {order: {meals: null}}},
            ];

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{orders: 'Big-Mac'}, {orders: 'Coke-Cola'}, {orders: 'Large-Fries'}]},
            ];
            const nullTestCases = [
                {user: [{orders: 'Big-Mac'}, {orders: 'Coke-Cola'}, {orders: null}]},
                {user: [{orders: null}, {orders: null}, {orders: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'orders',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'orders',
                ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['user', '0', 'orders']] },
                { result: false, fail: [['user', '1', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
            ];
            const testCases = [
                {user: [{orders: 'big-Mac'}, {orders: 'Coke-Cola'}, {orders: 'Large-Fries'}]},
                {user: [{orders: 'Big-Mac'}, {orders: 'CokeCola'}, {orders: 'Large-Fries'}]},
                {user: [{orders: 'Big-Mac'}, {orders: 'Coke-Cola'}, {orders: 41}]},
                {user: [{orders: 'Big-Mac'}, {orders: 'Coke-Cola'}, {orders: []}]},
                {user: [{orders: 'Big-Mac'}, {orders: 'Coke-Cola'}, {orders: {}}]},
                {user: [{orders: 'Big-Mac'}, {orders: 'Coke-Cola'}, {orders: undefined}]},
                {user: [{orders: 'Big-Mac'}, {orders: 'Coke-Cola'}, {orders: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['user', '*'],
                    fieldName: 'orders',
                    ruleWithOptions: 'in:Big-Mac,Coke-Cola,Large-Fries',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});
