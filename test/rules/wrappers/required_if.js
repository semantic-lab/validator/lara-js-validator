import testHelper from "./testHelper";

describe('wrappers.required_if', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {auth: 'user', data: 'success message'},
                {auth: 'user', data: 200},
                {auth: 'user', data: false},
                {auth: 'user', data: /^[0-9]*$/i},
                {auth: 'admin', data: []},
                {auth: 'admin', data: {}},
                {auth: 'admin', data: undefined},
                {auth: 'admin', data: null},
                true,
                41,
                {},
                [],
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_if:auth,user',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {auth: 'user', data: []},
                {auth: 'user', data: {}},
                {auth: 'user', data: undefined},
                {auth: 'user', data: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_if:auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {auth: 'user', environment: {data: 'success message'}}},
                {system: {auth: 'user', environment: {data: 200}}},
                {system: {auth: 'user', environment: {data: false}}},
                {system: {auth: 'user', environment: {data: /^[0-9]*$/i}}},
                {system: {auth: 'admin', environment: {data: []}}},
                {system: {auth: 'admin', environment: {data: {}}}},
                {system: {auth: 'admin', environment: {data: undefined}}},
                {system: {auth: 'admin', environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_if:system.auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {auth: 'user', environment: {data: []}}},
                {system: {auth: 'user', environment: {data: {}}}},
                {system: {auth: 'user', environment: {data: undefined}}},
                {system: {auth: 'user', environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_if:system.auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: [
                        {auth: 'user', data: 'success message'},
                        {auth: 'user', data: 200},
                        {auth: 'user', data: false},
                        {auth: 'user', data: /^[0-9]*$/i}
                    ]},
                {system: [
                        {auth: 'admin', data: 'success message'},
                        {auth: 'dev', data: ''},
                        {auth: 'dev', data: []},
                        {auth: 'dev', data: undefined},
                        {auth: 'dev', data: null},
                    ]},
                {system: [{auth: 'user', data: 'success message'}, {auth: 'dev', data: ''}]},
                {system: [{auth: 'user', data: 'success message'}, {auth: 'dev', data: []}]},
                {system: [{auth: 'user', data: 'success message'}, {auth: 'dev', data: {}}]},
                {system: [{auth: 'user', data: 'success message'}, {auth: 'dev', data: undefined} ]},
                {system: [{auth: 'user', data: 'success message'}, {auth: 'dev', data: null}]},
            ];

            testHelper({
                parentPath: ['system', '*'],
                fieldName: 'data',
                ruleWithOptions: 'required_if:system.*.auth,user,admin',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['system', '1', 'data']] },
                { result: false, fail: [['system', '1', 'data']] },
                { result: false, fail: [['system', '1', 'data']] },
                { result: false, fail: [['system', '0', 'data']] },
                { result: false, fail: [['system', '0', 'data']] },
            ];
            const testCases = [
                {system: [{auth: 'user', data: 'success message'}, {auth: 'user', data: ''}]},
                {system: [{auth: 'dev', data: 'success message'}, {auth: 'user', data: []}]},
                {system: [{auth: 'dev', data: 'success message'}, {auth: 'user', data: {}}]},
                {system: [{auth: 'admin', data: undefined}, {auth: 'user', data: 'success message'} ]},
                {system: [{auth: 'admin', data: null}, {auth: 'user', data: 'success message'}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_if:system.*.auth,user,admin',
                    isNullable: true,
                    presentOnly: true
                }, [testCase], expects[index]);
            });
        });
    });
});
