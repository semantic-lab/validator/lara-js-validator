import testHelper from "./testHelper";

describe('wrappers.digits_between', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {validCode: 12340},
                {validCode: 134},
                {validCode: 74185},
                {validCode: 492},
                {validCode: 95468},
                {validCode: 9568},
            ];
            const nullTestCases = [
                {validCode: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['validCode']] };
            const testCases = [
                {validCode: 51},
                {validCode: 13502849},
                {validCode: 'user'},
                {validCode: true},
                {validCode: new Date('2019-03-01')},
                {validCode: /^[0-9][a-z]*$/i},
                {validCode: {}},
                {validCode: []},
                {validCode: undefined},
                {validCode: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: {user: {validCode: 915}}},
                {update: {user: {validCode: 7452}}},
                {update: {user: {validCode: 46527}}},
                {update: {user: {validCode: 1597}}},
                {update: {user: {validCode: 114}}},
            ];
            const nullTestCases = [
                {update: {user: {validCode: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', 'user', 'validCode']] };
            const testCases = [
                {update: {user: {validCode: 25}}},
                {update: {user: {validCode: 8574139}}},
                {update: {user: {validCode: 'user'}}},
                {update: {user: {validCode: false}}},
                {update: {user: {validCode: new Date('2019-03-01')}}},
                {update: {user: {validCode: /^[0-9][a-z]*$/i}}},
                {update: {user: {validCode: {}}}},
                {update: {user: {validCode: []}}},
                {update: {user: {validCode: undefined}}},
                {update: {user: {validCode: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: [{validCode: 157}, {validCode: 4692}, {validCode: 74851}]},
                {update: [{validCode: 25489}, {validCode: 3254}, {validCode: 994}]},
            ];
            const nullTestCases = [
                {update: [{validCode: 25489}, {validCode: 3254}, {validCode: null}]},
                {update: [{validCode: null}, {validCode: null}, {validCode: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'validCode',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', '2', 'price']] };
            const testCases = [
                {update: [{price: 15743}, {price: 36254}, {price: 0}]},
                {update: [{price: 15743}, {price: 36254}, {price: 2587463}]},
                {update: [{price: 15743}, {price: 36254}, {price: 'user'}]},
                {update: [{price: 25489}, {price: 46592}, {price: false}]},
                {update: [{price: 15743}, {price: 36254}, {price: new Date('2019-03-01')}]},
                {update: [{price: 25489}, {price: 46592}, {price: /^[0-9][a-z]*$/i}]},
                {update: [{price: 15743}, {price: 36254}, {price: []}]},
                {update: [{price: 25489}, {price: 46592}, {price: {}}]},
                {update: [{price: 15743}, {price: 36254}, {price: undefined}]},
                {update: [{price: 25489}, {price: 46592}, {price: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'price',
                ruleWithOptions: 'digits_between:3,5',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});