import testHelper from "./testHelper";

const d20190401 = new Date('2019-04-01');
const d20190303 = new Date('2019-03-03');
const d19911227 = new Date('1991-12-27');
const t20190401 = d20190401.getTime();
const t20190303 = d20190303.getTime();
const t19911227 = d19911227.getTime();

describe('wrappers.before', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {deadline: '1991-12-27'},
                {deadline: '1991/12/27'},
                {deadline: '1991.12.27'},
                {deadline: d19911227},
                {deadline: t19911227},
                {deadline: null},
            ];
            const nullTestCases = [
                {deadline: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['deadline']] };
            const testCases = [
                {deadline: '2019-04-01'},
                {deadline: '2019/04/01'},
                {deadline: '2019.04.01'},
                {deadline: d20190401},
                {deadline: t20190401},
                {deadline: []},
                {deadline: {}},
                {deadline: undefined},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {JS: {exercise: {deadline: '1991-12-27'}}},
                {JS: {exercise: {deadline: '1991/12/27'}}},
                {JS: {exercise: {deadline: '1991.12.27'}}},
                {JS: {exercise: {deadline: d19911227}}},
                {JS: {exercise: {deadline: t19911227}}},
                {JS: {exercise: {deadline: null}}},
            ];
            const nullTestCases = [
                {JS: {exercise: {deadline: null}}}
            ];

            testHelper({
                parentPath: ['JS', 'exercise'],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['JS', 'exercise'],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['JS', 'exercise', 'deadline']] };
            const testCases = [
                {JS: {exercise: {deadline: '2019-04-01'}}},
                {JS: {exercise: {deadline: '2019/04/01'}}},
                {JS: {exercise: {deadline: '2019.04.01'}}},
                {JS: {exercise: {deadline: d20190401}}},
                {JS: {exercise: {deadline: t20190401}}},
                {JS: {exercise: {deadline: []}}},
                {JS: {exercise: {deadline: {}}}},
                {JS: {exercise: {deadline: undefined}}},
            ];

            testHelper({
                parentPath: ['JS', 'exercise'],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {exercise: [{deadline: '1991-12-27'}, {deadline: '1991/12/27'}, {deadline: '1991.12.27'}]},
                {exercise: [{deadline: d19911227}, {deadline: t19911227}, {deadline: null}]},
            ];
            const nullTestCases = [
                {exercise: [{deadline: d19911227}, {deadline: t19911227}, {deadline: null}]},
                {exercise: [{deadline: null}, {deadline: null}, {deadline: null}]},
            ];

            testHelper({
                parentPath: ['exercise', '*'],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['exercise', '*'],
                fieldName: 'deadline',
                ruleWithOptions: 'before:2019-03-03',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['exercise', '0', 'deadline']] },
                { result: false, fail: [['exercise', '1', 'deadline']] },
                { result: false, fail: [['exercise', '2', 'deadline']] },
                { result: false, fail: [['exercise', '2', 'deadline']] },
                { result: false, fail: [['exercise', '2', 'deadline']] },
                { result: false, fail: [['exercise', '2', 'deadline']] },
                { result: false, fail: [['exercise', '2', 'deadline']] },
                { result: false, fail: [['exercise', '2', 'deadline']] },
            ];
            const testCases = [
                {exercise: [{deadline: '2019-04-01'}, {deadline: '1991/12/27'}, {deadline: '1991.12.27'}]},
                {exercise: [{deadline: '1991-12-27'}, {deadline: '2019/04/01'}, {deadline: '1991.12.27'}]},
                {exercise: [{deadline: '1991-12-27'}, {deadline: '1991/12/27'}, {deadline: '2019.04.01'}]},
                {exercise: [{deadline: '1991-12-27'}, {deadline: '1991/12/27'}, {deadline: d20190401}]},
                {exercise: [{deadline: '1991-12-27'}, {deadline: '1991/12/27'}, {deadline: t20190401}]},
                {exercise: [{deadline: '1991-12-27'}, {deadline: '1991/12/27'}, {deadline: []}]},
                {exercise: [{deadline: '1991-12-27'}, {deadline: '1991/12/27'}, {deadline: {}}]},
                {exercise: [{deadline: '1991-12-27'}, {deadline: '1991/12/27'}, {deadline: undefined}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['exercise', '*'],
                    fieldName: 'deadline',
                    ruleWithOptions: 'before:2019-03-03',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});
