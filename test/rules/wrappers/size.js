import testHelper from "./testHelper";

describe('wrappers.size', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {mainDish: 3},
                {mainDish: '410'},
                {mainDish: 'ABC'},
                {mainDish: ['dish0', 'dish1', 'dish2']},
            ];
            const nullTestCases = [
                {mainDish: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'size:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'size:3',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['mainDish']] };
            const testCases = [
                {mainDish: 120},
                {mainDish: '6chars'},
                {mainDish: ['dish0']},
                {mainDish: 240},
                {mainDish: []},
                {mainDish: {}},
                {mainDish: undefined},
                {mainDish: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'size:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {aLaCaret: {dish: 3}}},
                {order: {aLaCaret: {dish: '537'}}},
                {order: {aLaCaret: {dish: 'abc'}}},
                {order: {aLaCaret: {dish: ['dish0', 'dish1', {}]}}},
            ];
            const nullTestCases = [
                {order: {aLaCaret: {dish: null}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'size:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'size:3',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'aLaCaret', 'dish']] };
            const testCases = [
                {order: {aLaCaret: {dish: 4}}},
                {order: {aLaCaret: {dish: 'abdc'}}},
                {order: {aLaCaret: {dish: '753-'}}},
                {order: {aLaCaret: {dish: [{}]}}},
                {order: {aLaCaret: {dish: []}}},
                {order: {aLaCaret: {dish: {}}}},
                {order: {aLaCaret: {dish: undefined}}},
                {order: {aLaCaret: {dish: null}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'size:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: 3}]},
                {aLaCaret: [{dish: 'abc'}, {dish: '988'}, {dish: '!?#'}]},
                {aLaCaret: [{dish: [{}, [], '']}, {dish: [{}, [], '']}, {dish: [{}, [], '']}]},
            ];
            const nullTestCases = [
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: null}]},
                {aLaCaret: [{dish: null}, {dish: null}, {dish: null}]},
            ];

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'size:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'size:3',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['aLaCaret', '2', 'dish']] };
            const testCases = [
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: 410}]},
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: 'a1c2'}]},
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: [{}, '']}]},
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: []}]},
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: {}}]},
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: undefined}]},
                {aLaCaret: [{dish: 3}, {dish: 3}, {dish: null}]},
            ];

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'size:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});