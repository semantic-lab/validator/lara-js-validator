import testHelper from "./testHelper";

describe('wrappers.min', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {mainDish: 3.1},
                {mainDish: 3},
                {mainDish: '3.14'},
                {mainDish: '410'},
                {mainDish: [0, 1, 2, 3]},
                {mainDish: ['dish0', 'dish1', 'dish2']},
            ];
            const nullTestCases = [
                {mainDish: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'min:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'min:3',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['mainDish']] };
            const testCases = [
                {mainDish: -0.01},
                {mainDish: 'D1'},
                {mainDish: ['dish0']},
                {mainDish: undefined},
                {mainDish: true},
                {mainDish: {}},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mainDish',
                ruleWithOptions: 'min:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {aLaCaret: {dish: 3.01}}},
                {order: {aLaCaret: {dish: 3}}},
                {order: {aLaCaret: {dish: 'a-c'}}},
                {order: {aLaCaret: {dish: 'AB123'}}},
                {order: {aLaCaret: {dish: ['dish0', 'dish1', 'dish2']}}},
                {order: {aLaCaret: {dish: [0, 'A', 1, 'b']}}},
            ];
            const nullTestCases = [
                {order: {aLaCaret: {dish: null}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'min:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'min:3',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'aLaCaret', 'dish']] };
            const testCases = [
                {order: {aLaCaret: {dish: 2.99}}},
                {order: {aLaCaret: {dish: 'D1'}}},
                {order: {aLaCaret: {dish: []}}},
                {order: {aLaCaret: {dish: undefined}}},
                {order: {aLaCaret: {dish: null}}},
                {order: {aLaCaret: {dish: {}}}},
                {order: {aLaCaret: {dish: false}}},
                {order: {aLaCaret: {}}},
            ];

            testHelper({
                parentPath: ['order', 'aLaCaret'],
                fieldName: 'dish',
                ruleWithOptions: 'min:3',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {aLaCaret: [{dish: 4.01}, {dish: 5}, {dish: 96}]},
                {aLaCaret: [{dish: 4}, {dish: 4}, {dish: 4}]},
                {aLaCaret: [{dish: 'Abcde'}, {dish: 'Ab-cd'}, {dish: 'abcd'}]},
                {aLaCaret: [{dish: 'char'}, {dish: 'four'}, {dish: 'min4'}]},
                {aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D', 'E']}, {dish: [0, 'B', 1, 'c', {}]}]},
                {aLaCaret: [{dish: [0, 1, 2, 3]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', {}, 1]}]},
            ];
            const nullTestCases = [
                {aLaCaret: [{dish: 4.01}, {dish: 5}, {dish: null}]},
                {aLaCaret: [{dish: null}, {dish: null}, {dish: null}]},
            ];

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'min:4',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['aLaCaret', '*'],
                fieldName: 'dish',
                ruleWithOptions: 'min:4',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['aLaCaret', '0', 'dish']] },
                { result: false, fail: [['aLaCaret', '0', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
                { result: false, fail: [['aLaCaret', '2', 'dish']] },
            ];
            const testCases = [
                {aLaCaret: [{dish: 2.99}, {dish: 0}, {dish: 96}]},
                {aLaCaret: [{dish: 'A'}, {dish: '1'}, {dish: 'abc'}]},
                {aLaCaret: [{dish: [0, 1, 2, 3, 4]}, {dish: ['A', 'B', 'C', 'D']}, {dish: [0, 'B', 1]}]},
                {aLaCaret: [{dish: 100}, {dish: 98}, {dish: undefined}]},
                {aLaCaret: [{dish: 100}, {dish: 98}, {dish: null}]},
                {aLaCaret: [{dish: 100}, {dish: 98}, {dish: {}}]},
                {aLaCaret: [{dish: 100}, {dish: 98}, {dish: false}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['aLaCaret', '*'],
                    fieldName: 'dish',
                    ruleWithOptions: 'min:4',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});