import testHelper from "./testHelper";

const d20190401 = new Date('2019-04-01');
const d20190303 = new Date('2019-03-03');
const d19911227 = new Date('1991-12-27');
const t20190401 = d20190401.getTime();
const t20190303 = d20190303.getTime();
const t19911227 = d19911227.getTime();

describe('wrappers.date', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {beginAt: '2019-04-01'},
                {beginAt: '2019/04/01'},
                {beginAt: '2019.04.01'},
                {beginAt: d20190401},
                {beginAt: t20190401},
            ];
            const nullTestCases = [
                {beginAt: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['beginAt']] };
            const testCases = [
                {beginAt: 'string'},
                {beginAt: /^[0-9a-zA-Z]$/i},
                {beginAt: []},
                {beginAt: {}},
                {beginAt: undefined},
                {beginAt: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {NBA: {Dallas_Mavericks: {beginAt: '2019-04-01'}}},
                {NBA: {Dallas_Mavericks: {beginAt: '2019/04/01'}}},
                {NBA: {Dallas_Mavericks: {beginAt: '2019.04.01'}}},
                {NBA: {Dallas_Mavericks: {beginAt: d20190401}}},
                {NBA: {Dallas_Mavericks: {beginAt: t20190401}}},
            ];
            const nullTestCases = [
                {NBA: {Dallas_Mavericks: {beginAt: null}}},
            ];

            testHelper({
                parentPath: ['NBA', 'Dallas_Mavericks'],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['NBA', 'Dallas_Mavericks'],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['NBA', 'Dallas_Mavericks', 'beginAt']] };
            const testCases = [
                {NBA: {Dallas_Mavericks: {beginAt: 'string'}}},
                {NBA: {Dallas_Mavericks: {beginAt: /^[0-9a-zA-Z]$/i}}},
                {NBA: {Dallas_Mavericks: {beginAt: []}}},
                {NBA: {Dallas_Mavericks: {beginAt: {}}}},
                {NBA: {Dallas_Mavericks: {beginAt: undefined}}},
                {NBA: {Dallas_Mavericks: {beginAt: null}}},
            ];

            testHelper({
                parentPath: ['NBA', 'Dallas_Mavericks'],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: '2019.04.01'}]},
                {games: [{beginAt: d20190401}, {beginAt: t20190401}]},
            ];
            const nullTestCases = [
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: null}]},
                {games: [{beginAt: null}, {beginAt: null}, {beginAt: null}]},
            ];

            testHelper({
                parentPath: ['games', '*'],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['games', '*'],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['games', '2', 'beginAt']] };
            const testCases = [
                {games: [{beginAt: '1991-12-27'}, {beginAt: '2019/04/01'}, {beginAt: 'string'}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '1991/12/27'}, {beginAt: /^[0-9a-zA-Z]$/i}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: []}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: {}}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: undefined}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: null}]},
            ];
            testHelper({
                parentPath: ['games', '*'],
                fieldName: 'beginAt',
                ruleWithOptions: 'date',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});
