import testHelper from './testHelper';

describe('wrappers().alpha_num', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {title: 'alphaNum'},
                {title: 'String'},
                {title: 'STR'},
                {title: 'Nowitzki41'},
                {title: '20'},
            ];
            const nullTestCases = [
                {title: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha_num',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha_num',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['title']] };
            const testCases = [
                {title: 'alpha Num'},
                {title: 'Dirk Nowitzki_41'},
                {title: 'Roger-Federer'},
                {title: 41},
                {title: []},
                {title: {}},
                {title: undefined},
                {title: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha_num',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: {name: {firstName: 'dirk41'}}},
                {user: {name: {firstName: 'ROGER100'}}},
            ];
            const nullTestCases = [
                {user: {name: {firstName: null}}},
            ];

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha_num',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha_num',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', 'name', 'firstName']] };
            const testCases = [
                {user: {name: {firstName: 'Dr.CHIANG'}}},
                {user: {name: {firstName: 'Dirk '}}},
                {user: {name: {firstName: 'Dirk!'}}},
                {user: {name: {firstName: 'Dirk-41'}}},
                {user: {name: {firstName: 'Roger_100'}}},
                {user: {name: {firstName: []}}},
                {user: {name: {firstName: {}}}},
                {user: {name: {firstName: undefined}}},
                {user: {name: {firstName: null}}},
            ];

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha_num',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: 'Doctor01'}]},
            ];
            const nullTestCases = [
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: null}]},
                {user: [{title: null}, {title: null}, {title: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'title',
                ruleWithOptions: 'alpha_num',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'title',
                ruleWithOptions: 'alpha_num',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '1', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
            ];
            const testCases = [
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: 'Doctor!'}]},
                {user: [{title: 'DevOps41'}, {title: 'R&D'}, {title: 'Doc_tor'}]},
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: 'Dr CHIANG'}]},
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: []}]},
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: {}}]},
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: undefined}]},
                {user: [{title: 'DevOps41'}, {title: 'RD'}, {title: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['user', '*'],
                    fieldName: 'title',
                    ruleWithOptions: 'alpha_num',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});