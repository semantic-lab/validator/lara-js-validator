import testHelper from "./testHelper";

describe('wrappers.not_regex', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {mail: 'us-user@yahoo.com'},
                {mail: 'tw-user@yahoo.com.tw'},
                {mail: 'aws123@amazon.com'},
                {mail: 'employee@company.com'},
                {mail: '123@123.com'},
                {mail: '0928374094'},
                {mail: 0},
                {mail: 41},
                {mail: {}},
                {mail: []},
                {mail: undefined},
                {mail: null},
                {mail: new Date('2019-03-01')},
            ];
            const nullTestCases = [
                {mail: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mail',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'mail',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['mail']] };
            const testCases = [
                {mail: '123@gmail.com'},
                {mail: 'user@gmail.com'},
                {mail: 'employee@gmail.com'},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'mail',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {user: {email: 'us-user@yahoo.com'}}},
                {order: {user: {email: 'tw-user@yahoo.com.tw'}}},
                {order: {user: {email: 'aws123@amazon.com'}}},
                {order: {user: {email: '123@123.com'}}},
                {order: {user: {email: 'A900000000'}}},
                {order: {user: {email: 928317939}}},
                {order: {user: {email: true}}},
                {order: {user: {email: {}}}},
                {order: {user: {email: []}}},
                {order: {user: {email: undefined}}},
                {order: {user: {email: null}}},
            ];
            const nullTestCases = [
                {order: {user: {email: null}}},
            ];

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'email',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'email',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'user', 'email']] };
            const testCases = [
                {order: {user: {email: '123@gmail.com'}}},
                {order: {user: {email: 'user@gmail.com'}}},
                {order: {user: {email: 'employee@gmail.com'}}},
            ];

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'email',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: '123@123.com'}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: 89}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: true}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: []}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: {}}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: undefined}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: null}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: new Date('2019-03-03')}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: '0928365549'}]},
            ];
            const nullTestCases = [
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: null}]},
                {user: [{email: null}, {email: null}, {email: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'email',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'email',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', '2', 'email']] };
            const testCases = [
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: 'user@gmail.com'}]},
                {user: [{email: 'us-user@yahoo.com'}, {email: 'aws123@amazon.com'}, {email: 'employee@gmail.com'}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'email',
                ruleWithOptions: 'not_regex:/^.*@gmail.com$/i',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});
