import testHelper from "./testHelper";

describe('wrappers.ip', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {ip: '8.8.8.8'},
                {ip: '127.0.0.1'},
                {ip: '155.135.53.85'},
                {ip: '192.168.2.123'},
            ];
            const nullTestCases = [
                {ip: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['ip']] };
            const testCases = [
                {ip: '256.123.51.46'},
                {ip: '192.168.300.4'},
                {ip: '198.51.100.0/24'},
                {ip: 'a02e3b0b-c3b8-xf36-8962-5f3d27122a99'},
                {ip: -123.456},
                {ip: 'id'},
                {ip: true},
                {ip: new Date('2019-03-01')},
                {ip: /^[0-9][a-z]*$/i},
                {ip: {}},
                {ip: []},
                {ip: undefined},
                {ip: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: {user: {ip: '8.8.8.8'}}},
                {update: {user: {ip: '127.0.0.1'}}},
                {update: {user: {ip: '155.135.53.85'}}},
                {update: {user: {ip: '192.168.2.123'}}},
            ];
            const nullTestCases = [
                {update: {user: {ip: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', 'user', 'ip']] };
            const testCases = [
                {update: {user: {ip: '256.123.51.46'}}},
                {update: {user: {ip: '192.168.300.4'}}},
                {update: {user: {ip: '198.51.100.0/24'}}},
                {update: {user: {ip: '8470b099-6d95-4dd5-adfc4598437b'}}},
                {update: {user: {ip: 'id'}}},
                {update: {user: {ip: 123.456}}},
                {update: {user: {ip: false}}},
                {update: {user: {ip: new Date('2019-03-01')}}},
                {update: {user: {ip: /^[0-9][a-z]*$/i}}},
                {update: {user: {ip: {}}}},
                {update: {user: {ip: []}}},
                {update: {user: {ip: undefined}}},
                {update: {user: {ip: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: [{ip: '8.8.8.8'}, {ip: '127.0.0.1'}]},
                {update: [{ip: '155.135.53.85'}, {ip: '192.168.2.123'}]},
            ];
            const nullTestCases = [
                {update: [{ip: '8.8.8.8'}, {ip: null}]},
                {update: [{ip: null}, {ip: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', '1', 'ip']] };
            const testCases = [
                {update: [{ip: '8.8.8.8'}, {ip: '256.123.51.46'}]},
                {update: [{ip: '8.8.8.8'}, {ip: '192.168.300.4'}]},
                {update: [{ip: '8.8.8.8'}, {ip: '198.51.100.0/24'}]},
                {update: [{ip: '8.8.8.8'}, {ip: 'cf64170c-399d-11e9-b210-d663bd3d93'}]},
                {update: [{ip: '8.8.8.8'}, {ip: 'id'}]},
                {update: [{ip: '8.8.8.8'}, {ip: -0.4568}]},
                {update: [{ip: '8.8.8.8'}, {ip: false}]},
                {update: [{ip: '8.8.8.8'}, {ip: new Date('2019-03-01')}]},
                {update: [{ip: '8.8.8.8'}, {ip: /^[0-9][a-z]*$/i}]},
                {update: [{ip: '8.8.8.8'}, {ip: []}]},
                {update: [{ip: '8.8.8.8'}, {ip: {}}]},
                {update: [{ip: '8.8.8.8'}, {ip: undefined}]},
                {update: [{ip: '8.8.8.8'}, {ip: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'ip',
                ruleWithOptions: 'ip',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});