import testHelper from "./testHelper";

describe('wrappers.filled', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'success message'},
                {data: 200},
                {data: false},
                {data: /^[0-9]*$/i},
                {data1: 200},
                41,
                true,
                '',
                [],
                {},
                undefined,
                null,
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'filled',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {data: []},
                {data: {}},
                {data: undefined},
                {data: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'filled',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {environment: {data: 'success message'}}},
                {system: {environment: {data: 200}}},
                {system: {environment: {data: false}}},
                {system: {environment: {data: /^[0-9]*$/i}}},
                {system: {environment: true}},
                {system: {environment: 41}},
                {system: {environment: {}}},
                {system: {environment: []}},
                {system: {environment: undefined}},
                {system: {environment: null}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'filled',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {environment: {data: []}}},
                {system: {environment: {data: {}}}},
                {system: {environment: {data: undefined}}},
                {system: {environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'filled',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {config: [{data: 'success message'}, {data: 200}, {data: false}, {data: /^[0-9]*$/i}]},
                {config: [{data: 'success message'}, {data: 200}, true]},
                {config: [{data: 'success message'}, {data: 200}, 41]},
                {config: [{data: 'success message'}, {data: 200}, {}]},
                {config: [{data: 'success message'}, {data: 200}, []]},
                {config: [{data: 'success message'}, {data: 200}, undefined]},
                {config: [{data: 'success message'}, {data: 200}, null]},
            ];

            testHelper({
                parentPath: ['config', '*'],
                fieldName: 'data',
                ruleWithOptions: 'filled',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['config', '*', 'data']] };
            const testCases = [
                {config: [{data: 'success message'}, {data: 200}, {data: []}]},
                {config: [{data: 'success message'}, {data: 200}, {data: {}}]},
                {config: [{data: 'success message'}, {data: 200}, {data: undefined}]},
                {config: [{data: 'success message'}, {data: 200}, {data: null}]},
            ];

            testHelper({
                parentPath: ['config', '*'],
                fieldName: 'data',
                ruleWithOptions: 'filled',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});