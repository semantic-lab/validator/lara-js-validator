import testHelper from "./testHelper";

describe('wrappers.present', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'success message'},
                {data: 200},
                {data: false},
                {data: /^[0-9]*$/i},
                {data: []},
                {data: {}},
                {data: undefined},
                {data: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'present',
                isNullable: false,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {status: 200},
                true,
                41,
                {},
                [],
                undefined,
                null,
            ];
            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'present',
                isNullable: false,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {environment: {data: 'success message'}}},
                {system: {environment: {data: 200}}},
                {system: {environment: {data: false}}},
                {system: {environment: {data: /^[0-9]*$/i}}},
                {system: {environment: {data: []}}},
                {system: {environment: {data: {}}}},
                {system: {environment: {data: undefined}}},
                {system: {environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'present',
                isNullable: false,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {environment: {status: 200}}},
                {system: {environment: true}},
                {system: {environment: 41}},
                {system: {environment: {}}},
                {system: {environment: []}},
                {system: {environment: undefined}},
                {system: {environment: null}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'present',
                isNullable: false,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {config: [{data: 'success message'}, {data: 200}, {data: false}]},
                {config: [{data: /^[0-9]*$/i}, {data: []}, {data: {}}]},
                {config: [{data: undefined}, {data: null}]},
            ];

            testHelper({
                parentPath: ['config', '*'],
                fieldName: 'data',
                ruleWithOptions: 'present',
                isNullable: false,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['config', '2', 'data']] };
            const testCases = [
                {config: [{data: 'success message'}, {data: 200}, {status: 200}]},
                {config: [{data: 'success message'}, {data: 200}, true]},
                {config: [{data: 'success message'}, {data: 200}, 41]},
                {config: [{data: 'success message'}, {data: 200}, {}]},
                {config: [{data: 'success message'}, {data: 200}, []]},
                {config: [{data: 'success message'}, {data: 200}, undefined]},
                {config: [{data: 'success message'}, {data: 200}, null]},
            ];

            testHelper({
                parentPath: ['config', '*'],
                fieldName: 'data',
                ruleWithOptions: 'present',
                isNullable: false,
                presentOnly: true
            }, testCases, expect);
        });
    });
});