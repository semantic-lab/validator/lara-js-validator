import testHelper from "./testHelper";

describe('wrappers.required_without', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'success message', error: {id: 1}, errorMessage: 'token error'},
                {data: 200, error: {id: 1}, authFail: true},
                {data: false, errorMessage: 'token error', authFail: true},
                {data: /^[0-9]*$/i},
                {data: 'success message'},
                {data: [], error: true, errorMessage: true, authFail: true},
                {data: {}, error: true, errorMessage: true, authFail: true},
                {data: undefined, error: true, errorMessage: true, authFail: true},
                {data: null, error: true, errorMessage: true, authFail: true},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_without:error,errorMessage,authFail',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {data: []},
                {data: {}, errorMessage: undefined},
                {data: undefined, error: undefined, authFail: undefined},
                {data: null, error: undefined, errorMessage: undefined},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_without:error,errorMessage,authFail',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {environment: {data: 'success message'}}, api: {error: 'api has error'}},
                {system: {environment: {data: 200}}, api: {error: ''}},
                {system: {environment: {data: false}}},
                {system: {environment: {data: /^[0-9]*$/i}}},
                {system: {environment: {data: []}}, api: {error: true, errorMessage: true}},
                {system: {environment: {data: {}}}, api: {error: true, errorMessage: true}},
                {system: {environment: {data: undefined}}, api: {error: true, errorMessage: true}},
                {system: {environment: {data: null}}, api: {error: true, errorMessage: true}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_without:api.error,api.errorMessage',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {environment: {data: []}}, api: {error: ''}},
                {system: {environment: {data: {}}}, api: {error: ''}},
                {system: {environment: {data: undefined}}, api: {error: ''}},
                {system: {environment: {data: null}}, api: {error: ''}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_without:api.error,api.errorMessage',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('both array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message', error: 'api has error'},
                            {data: 200, error: ''},
                            {data: false, error: undefined},
                            {data: /^[0-9]*$/i, error: null}
                        ]},
                    {system: [
                            {data: 'success message', error: '', errorMessage: ''},
                            {data: '', error: true, errorMessage: true},
                            {data: [], error: true, errorMessage: true},
                            {data: undefined, error: true, errorMessage: true},
                            {data: null, error: true, errorMessage: true},
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['system', '1', 'data']] };
                const testCases = [
                    {system: [{data: 'success message', error: undefined}, {data: '', error: undefined}]},
                    {system: [{data: 'success message', error: undefined}, {data: [], error: undefined}]},
                    {system: [{data: 'success message', error: undefined}, {data: {}, error: undefined}]},
                    {system: [{data: 'success message', error: undefined}, {data: undefined, error: undefined} ]},
                    {system: [{data: 'success message', error: undefined}, {data: null, error: undefined}]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
        describe('fields in rule is not array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: 200},
                            {data: false},
                            {data: /^[0-9]*$/i}
                        ]},
                    {system: [
                            {data: 'success message'},
                            {data: 200},
                            {data: false},
                            {data: /^[0-9]*$/i}
                        ], error: false},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ], error: false, errorMessage: false},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without:error,errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['system', '1', 'data']] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ], error: false},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without:error,errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
        describe('validate field is not array, fields in rule is array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {data: 'success message', system: [{error: false, errorMessage: false}, {error: false, errorMessage: false}]},
                    {data: 200, system: [{error: false, errorMessage: false}, {error: false, errorMessage: false}]},
                    {data: false},
                    {data: /^[0-9]*$/i},
                    {data: 'success message'},
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['data']] };
                const testCases = [
                    {data: [], system: [{error: false, errorMessage: false}, {errorMessage: false}]},
                    {data: {}, system: [{error: false}, {error: false, errorMessage: false}]},
                    {data: undefined, system: [{error: false, errorMessage: false}, {errorMessage: false}]},
                    {data: null, system: [{error: false}, {error: false, errorMessage: false}]},
                    {data: []},
                    {data: {}},
                    {data: undefined},
                    {data: null},
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'data',
                    ruleWithOptions: 'required_without:system.*.error,system.*.errorMessage',
                    isNullable: false,
                    presentOnly: false
                }, testCases, expect);
            });
        });
    });
    describe('no field in rule', () => {
        const expect = { result: false, fail: [['data']] };
        const testCases = [
            {data: ''},
            {data: {}},
            {data: []},
            {data: null},
            {data: undefined},
            {},
            [],
            '',
            41,
            undefined,
            null,
        ];

        testHelper({
            parentPath: [],
            fieldName: 'data',
            ruleWithOptions: 'required_without',
            isNullable: false,
            presentOnly: false
        }, testCases, expect);
    });
});
