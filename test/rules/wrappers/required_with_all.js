import testHelper from "./testHelper";

describe('wrappers.required_with_all', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'success message', error: {id: 1}, errorMessage: 'token error'},
                {data: 200, error: {id: 2}, errorMessage: 'token error'},
                {data: false, error: {id: 3}, errorMessage: ''},
                {data: /^[0-9]*$/i, error: {id: 4}, errorMessage: []},
                {data: 'success message', error: {id: 5}, errorMessage: {}},
                {data: 'success message', error: {id: 6}, errorMessage: null},
                {data: 'success message', error: {id: 7}, errorMessage: undefined},
                {data: [], error: {id: 1}},
                {data: {}, error: {id: 2}},
                {data: undefined, error: {id: 3}},
                {data: null, error: {id: 4}},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_with_all:error,errorMessage',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {data: [], error: {id: 1}, errorMessage: false},
                {data: {}, error: {id: 2}, errorMessage: false},
                {data: undefined, error: {id: 3}, errorMessage: false},
                {data: null, error: {id: 4}, errorMessage: false},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_with_all:error,errorMessage',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {environment: {data: 'success message'}}, api: {error: 'api has error', errorMessage: 'message for api error'}},
                {system: {environment: {data: 200}}, api: {error: '', errorMessage: undefined}},
                {system: {environment: {data: false}}, api: {error: '', errorMessage: undefined}},
                {system: {environment: {data: /^[0-9]*$/i}}, api: {error: '', errorMessage: undefined}},
                {system: {environment: {data: []}}, api: {error: ''}},
                {system: {environment: {data: {}}}, api: []},
                {system: {environment: {data: undefined}}, api: undefined},
                {system: {environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_with_all:api.error,api.errorMessage',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {environment: {data: []}}, api: {error: false, errorMessage: false}},
                {system: {environment: {data: {}}}, api: {error: false, errorMessage: false}},
                {system: {environment: {data: undefined}}, api: {error: false, errorMessage: false}},
                {system: {environment: {data: null}}, api: {error: false, errorMessage: false}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_with_all:api.error,api.errorMessage',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('both array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message', error: 'api has error', errorMessage: 'message for api error'},
                            {data: 200, error: ''},
                            {data: false, error: undefined},
                            {data: /^[0-9]*$/i, error: null}
                        ]},
                    {system: [
                            {data: '', error: '', errorMessage: ''},
                            {data: {}, error: '', errorMessage: ''},
                            {data: [], error: '', errorMessage: ''},
                            {data: undefined, error: '', errorMessage: ''},
                            {data: null},
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with_all:system.*.error,system.*.errorMessage',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['system', '1', 'data']] };
                const testCases = [
                    {system: [{data: 'success message', error: false, errorMessage: false}, {data: '', error: 'false', errorMessage: false}]},
                    {system: [{data: 'success message', error: false, errorMessage: false}, {data: [], error: 'false', errorMessage: false}]},
                    {system: [{data: 'success message', error: false, errorMessage: false}, {data: {}, error: 'false', errorMessage: false}]},
                    {system: [{data: 'success message', error: false, errorMessage: false}, {data: undefined, error: false, errorMessage: 'false'} ]},
                    {system: [{data: 'success message', error: false, errorMessage: false}, {data: null, error: false, errorMessage: 'false'}]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with_all:system.*.error,system.*.errorMessage',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
        });
        describe('fields in rule is not array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: 200},
                            {data: false},
                            {data: /^[0-9]*$/i}
                        ], error: true, errorMessage: 'api has error'},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ], error: true},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with_all:error,errorMessage',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['system', '1', 'data']] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ], error: true, errorMessage: 'api has error'},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with_all:error,errorMessage',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
        });
        describe('validate field is array, fields in rule is array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {
                        data: true,
                        api: [
                            {error: false, errorMessage: true},
                            {error: false, errorMessage: true},
                            {error: false, errorMessage: true},
                        ]
                    },
                    {
                        data: 'undefined',
                        api: [
                            {error: false, errorMessage: true},
                            {error: false, errorMessage: true},
                            {error: false, errorMessage: true},
                        ]
                    }
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with_all:api.*.error,api.*.errorMessage',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
        });
    });
    describe('no field in rule', () => {
        const expect = { result: true, fail: [] };
        const testCases = [
            {data: ''},
            {data: {}},
            {data: []},
            {data: null},
            {data: undefined},
            {},
            [],
            '',
            41,
            undefined,
            null,
        ];

        testHelper({
            parentPath: [],
            fieldName: 'data',
            ruleWithOptions: 'required_with_all',
            isNullable: true,
            presentOnly: true
        }, testCases, expect);
    });
});
