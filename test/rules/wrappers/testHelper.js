import assert from 'assert';
import wrappers from '../../../src/rules/wrappers';
import RuleMeta from '../../../src/rules/RuleMeta';

/**
 *
 * @param {Object} rule
 * @param {Array} rule.parentPath
 * @param {String} rule.fieldName
 * @param {String} rule.ruleWithOptions
 * @param {Boolean} rule.isNullable
 * @param {Boolean} rule.presentOnly
 * @param {Array} testCases
 * @param {Object} expect
 * @param {Boolean} expect.result
 * @param {Array} expect.fail
 */
export default function (rule, testCases, expect) {
    testCases.forEach((testCase, index) => {
        const ruleMeta = new RuleMeta(testCase);
        ruleMeta.setFieldPath(rule.parentPath, rule.fieldName)
            .setParentValues()
            .resetRule()
            .setRule(rule.ruleWithOptions, rule.isNullable, rule.presentOnly);

        const path = ruleMeta.fieldPath.join('.');
        const ruleWithOptions = ruleMeta.rule;
        const nullable = (ruleMeta.isNullable) ? '(Allow Null)' : '(Not Allow Null)';
        const testCaseInString = JSON.stringify(testCase);
        const testTitle = `[${path}: ${ruleWithOptions}${nullable} => ${testCaseInString}]`;
        const ruleValidation = wrappers[ruleMeta.ruleName](ruleMeta);

        it(testTitle, () => {
            assert.strictEqual(
                JSON.stringify(ruleValidation),
                JSON.stringify(expect)
            );
        });
    });

}