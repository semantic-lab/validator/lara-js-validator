import testHelper from "./testHelper";

describe('wrappers.email', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {email: 'simple@example.com'},
                {email: 'very.common@example.com'},
                {email: 'disposable.style.email.with+symbol@example.com'},
                {email: 'other.email-with-hyphen@example.com'},
                {email: 'fully-qualified-domain@example.com'},
                {email: 'user.name+tag+sorting@example.com'},
                {email: 'x@example.com'},
                {email: 'example-indeed@strange-example.com'},
                {email: 'example@s.example'},
                {email: '" "@example.org'},
                {email: '"john..doe"@example.org'},
            ];
            const nullTestCases = [
                {email: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['email']] };
            const testCases = [
                {email: 'Abc.example.com'},
                {email: 'A@b@c@example.com'},
                {email: 'a"b(c)d,e:f;g<h>i[j\\k]l@example.com'},
                {email: 'just"not"right@example.com'},
                {email: 'this is"not\\allowed@example.com'},
                {email: 'this\\ still\\"not\\\\allowed@example.com'},
                {email: -123.456},
                {email: 'id'},
                {email: true},
                {email: new Date('2019-03-01')},
                {email: /^[0-9][a-z]*$/i},
                {email: {}},
                {email: []},
                {email: undefined},
                {email: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: {user: {email: 'simple@example.com'}}},
                {update: {user: {email: 'very.common@example.com'}}},
                {update: {user: {email: 'disposable.style.email.with+symbol@example.com'}}},
                {update: {user: {email: 'other.email-with-hyphen@example.com'}}},
                {update: {user: {email: 'fully-qualified-domain@example.com'}}},
                {update: {user: {email: 'user.name+tag+sorting@example.com'}}},
                {update: {user: {email: 'x@example.com'}}},
                {update: {user: {email: 'example-indeed@strange-example.com'}}},
                {update: {user: {email: 'example@s.example'}}},
                {update: {user: {email: '" "@example.org'}}},
                {update: {user: {email: '"john..doe"@example.org'}}},
            ];
            const nullTestCases = [
                {update: {user: {email: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', 'user', 'email']] };
            const testCases = [
                {update: {user: {email: 'Abc.example.com'}}},
                {update: {user: {email: 'A@b@c@example.com'}}},
                {update: {user: {email: 'a"b(c)d,e:f;g<h>i[j\\k]l@example.com'}}},
                {update: {user: {email: 'just"not"right@example.com'}}},
                {update: {user: {email: 'this is"not\\allowed@example.com'}}},
                {update: {user: {email: 'this\\ still\\"not\\\\allowed@example.com'}}},
                {update: {user: {email: 'id'}}},
                {update: {user: {email: 123.456}}},
                {update: {user: {email: false}}},
                {update: {user: {email: new Date('2019-03-01')}}},
                {update: {user: {email: /^[0-9][a-z]*$/i}}},
                {update: {user: {email: {}}}},
                {update: {user: {email: []}}},
                {update: {user: {email: undefined}}},
                {update: {user: {email: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: [{email: 'simple@example.com'}, {email: 'disposable.style.email.with+symbol@example.com'}]},
                {update: [{email: 'very.common@example.com'}, {email: 'x@example.com'}]},
            ];
            const nullTestCases = [
                {update: [{email: 'very.common@example.com'}, {email: null}]},
                {update: [{email: null}, {email: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', '1', 'email']] };
            const testCases = [
                {update: [{email: 'simple@example.com'}, {email: 'just"not"right@example.com'}]},
                {update: [{email: 'simple@example.com'}, {email: 'Abc.example.com'}]},
                {update: [{email: 'simple@example.com'}, {email: 'this is"not\\allowed@example.com'}]},
                {update: [{email: 'simple@example.com'}, {email: 'a"b(c)d,e:f;g<h>i[j\\k]l@example.com'}]},
                {update: [{email: 'simple@example.com'}, {email: 'this\\ still\\"not\\\\allowed@example.com'}]},
                {update: [{email: 'x@example.com'}, {email: 'id'}]},
                {update: [{email: 'simple@example.com'}, {email: -0.4568}]},
                {update: [{email: 'x@example.com'}, {email: false}]},
                {update: [{email: 'simple@example.com'}, {email: new Date('2019-03-01')}]},
                {update: [{email: 'x@example.com'}, {email: /^[0-9][a-z]*$/i}]},
                {update: [{email: 'simple@example.com'}, {email: []}]},
                {update: [{email: 'x@example.com'}, {email: {}}]},
                {update: [{email: 'simple@example.com'}, {email: undefined}]},
                {update: [{email: 'x@example.com'}, {email: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'email',
                ruleWithOptions: 'email',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});