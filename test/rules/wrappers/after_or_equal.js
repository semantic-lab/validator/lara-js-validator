import testHelper from "./testHelper";

const d20190401 = new Date('2019-04-01');
const d20190303 = new Date('2019-03-03');
const d19911227 = new Date('1991-12-27');
const t20190401 = d20190401.getTime();
const t20190303 = d20190303.getTime();
const t19911227 = d19911227.getTime();

describe('wrappers.after_or_equal', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {beginAt: '2019-04-01'},
                {beginAt: '2019/04/01'},
                {beginAt: '2019.04.01'},
                {beginAt: '2019-03-03'},
                {beginAt: d20190401},
                {beginAt: t20190401},
                {beginAt: d20190303},
                {beginAt: t20190303},
            ];
            const nullTestCases = [
                {beginAt: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['beginAt']] };
            const testCases = [
                {beginAt: '1991-12-27'},
                {beginAt: '1991/12/27'},
                {beginAt: '1991.12.27'},
                {beginAt: 'yyyy-MM-dd'},
                {beginAt: 'abcd12gh'},
                {beginAt: d19911227},
                {beginAt: t19911227},
                {beginAt: []},
                {beginAt: {}},
                {beginAt: undefined},
                {beginAt: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {NBA: {Dallas_Mavericks: {beginAt: '2019-04-01'}}},
                {NBA: {Dallas_Mavericks: {beginAt: '2019/04/01'}}},
                {NBA: {Dallas_Mavericks: {beginAt: '2019.04.01'}}},
                {NBA: {Dallas_Mavericks: {beginAt: '2019-03-03'}}},
                {NBA: {Dallas_Mavericks: {beginAt: d20190401}}},
                {NBA: {Dallas_Mavericks: {beginAt: t20190401}}},
                {NBA: {Dallas_Mavericks: {beginAt: d20190303}}},
                {NBA: {Dallas_Mavericks: {beginAt: t20190303}}},
            ];
            const nullTestCases = [
                {NBA: {Dallas_Mavericks: {beginAt: null}}},
            ];

            testHelper({
                parentPath: ['NBA', 'Dallas_Mavericks'],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['NBA', 'Dallas_Mavericks'],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['NBA', 'Dallas_Mavericks', 'beginAt']] };
            const testCases = [
                {NBA: {Dallas_Mavericks: {beginAt: '1991-12-27'}}},
                {NBA: {Dallas_Mavericks: {beginAt: '1991/12/27'}}},
                {NBA: {Dallas_Mavericks: {beginAt: '1991.12.27'}}},
                {NBA: {Dallas_Mavericks: {beginAt: d19911227}}},
                {NBA: {Dallas_Mavericks: {beginAt: t19911227}}},
                {NBA: {Dallas_Mavericks: {beginAt: []}}},
                {NBA: {Dallas_Mavericks: {beginAt: {}}}},
                {NBA: {Dallas_Mavericks: {beginAt: undefined}}},
                {NBA: {Dallas_Mavericks: {beginAt: null}}},
            ];

            testHelper({
                parentPath: ['NBA', 'Dallas_Mavericks'],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: '2019.04.01'}]},
                {games: [{beginAt: d20190401}, {beginAt: t20190401}]},
                {games: [{beginAt: d20190303}, {beginAt: t20190303}, {beginAt: '2019-03-03'}]},
            ];
            const nullTestCases = [
                {games: [{beginAt: d20190303}, {beginAt: t20190303}, {beginAt: null}]},
                {games: [{beginAt: null}, {beginAt: null}, {beginAt: null}]},
            ];

            testHelper({
                parentPath: ['games', '*'],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['games', '*'],
                fieldName: 'beginAt',
                ruleWithOptions: 'after_or_equal:2019-03-03',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['games', '0', 'beginAt']] },
                { result: false, fail: [['games', '1', 'beginAt']] },
                { result: false, fail: [['games', '2', 'beginAt']] },
                { result: false, fail: [['games', '2', 'beginAt']] },
                { result: false, fail: [['games', '2', 'beginAt']] },
                { result: false, fail: [['games', '2', 'beginAt']] },
                { result: false, fail: [['games', '2', 'beginAt']] },
                { result: false, fail: [['games', '2', 'beginAt']] },
                { result: false, fail: [['games', '2', 'beginAt']] },
            ];
            const testCases = [
                {games: [{beginAt: '1991-12-27'}, {beginAt: '2019/04/01'}, {beginAt: '2019.04.01'}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '1991/12/27'}, {beginAt: '2019.04.01'}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: '1991.12.27'}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: d19911227}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: t19911227}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: []}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: {}}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: undefined}]},
                {games: [{beginAt: '2019-04-01'}, {beginAt: '2019/04/01'}, {beginAt: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['games', '*'],
                    fieldName: 'beginAt',
                    ruleWithOptions: 'after_or_equal:2019-03-03',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});
