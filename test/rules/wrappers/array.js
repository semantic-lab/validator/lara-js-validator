import testHelper from './testHelper';

describe('wrappers.array', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {orders: []},
                {orders: ['Big Mac', 'Big Coke']},
                {orders: [4, 1]},
                {orders: [{id: 41}, {id: 20}]},
                {orders: [undefined, null]},
                {orders: [[]]},
            ];
            const nullTestCases = [
                {orders: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'array',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'array',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['orders']] };
            const testCases = [
                {orders: 'Big Mac'},
                {orders: 41},
                {orders: new Date('2019-03-02')},
                {orders: {}},
                {orders: undefined},
                {orders: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'orders',
                ruleWithOptions: 'array',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: {order: {meals: []}}},
                {user: {order: {meals: ['Big Mac', 'Big Coke']}}},
                {user: {order: {meals: [4, 1]}}},
                {user: {order: {meals: [{id: 41}, {id: 20}]}}},
                {user: {order: {meals: [undefined, null]}}},
                {user: {order: {meals: [[]]}}},
            ];
            const nullTestCases = [
                {user: {order: {meals: null}}},
            ];

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'array',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'array',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', 'order', 'meals']] };
            const testCases = [
                {user: {order: {meals: 'Big Mac'}}},
                {user: {order: {meals: 41}}},
                {user: {order: {meals: new Date('2019-03-02')}}},
                {user: {order: {meals: {}}}},
                {user: {order: {meals: undefined}}},
                {user: {order: {meals: null}}},
            ];

            testHelper({
                parentPath: ['user', 'order'],
                fieldName: 'meals',
                ruleWithOptions: 'array',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{orders: ['Big Mac', 'Big Coke']}, {orders: [4, 1]}, {orders: [undefined, null]}]},
            ];
            const nullTestCases = [
                {user: [{orders: ['Big Mac', 'Big Coke']}, {orders: [4, 1]}, {orders: null}]},
                {user: [{orders: null}, {orders: null}, {orders: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'orders',
                ruleWithOptions: 'array',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'orders',
                ruleWithOptions: 'array',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['user', '0', 'orders']] },
                { result: false, fail: [['user', '1', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
                { result: false, fail: [['user', '2', 'orders']] },
            ];
            const testCases = [
                {user: [{orders: 'Big Mac'}, {orders: [4, 1]}, {orders: [undefined, null]}]},
                {user: [{orders: ['Big Mac', 'Big Coke']}, {orders: 41}, {orders: [undefined, null]}]},
                {user: [{orders: ['Big Mac', 'Big Coke']}, {orders: [4, 1]}, {orders: {id: 4}}]},
                {user: [{orders: ['Big Mac', 'Big Coke']}, {orders: [4, 1]}, {orders: undefined}]},
                {user: [{orders: ['Big Mac', 'Big Coke']}, {orders: [4, 1]}, {orders: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['user', '*'],
                    fieldName: 'orders',
                    ruleWithOptions: 'array',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});
