import testHelper from "./testHelper";

describe('wrappers.required_with', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {data: 'success message', error: {id: 1}, errorMessage: 'token error', authFail: true},
                {data: 200, error: {id: 1}, errorMessage: 'token error', authFail: true},
                {data: false, error: {id: 1}, errorMessage: 'token error', authFail: true},
                {data: /^[0-9]*$/i, error: {id: 1}, errorMessage: 'token error', authFail: true},
                {data: 'success message', error: undefined},
                {data: 'success message', errorMessage: undefined},
                {data: 'success message', authFail: undefined},
                {data: []},
                {data: {}},
                {data: undefined},
                {data: null},
                true,
                41,
                {},
                [],
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_with:error,errorMessage,authFail',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {data: [], error: true},
                {data: {}, errorMessage: true},
                {data: undefined, authFail: true},
                {data: null, authFail: true},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_with:error,errorMessage,authFail',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {environment: {data: 'success message'}}, api: {error: 'api has error', errorMessage: 'message for api error'}},
                {system: {environment: {data: 200}}, api: {error: '', errorMessage: undefined}},
                {system: {environment: {data: false}}, api: {error: null}},
                {system: {environment: {data: /^[0-9]*$/i}}, api: {errorMessage: {}}},
                {system: {environment: {data: []}}, api: {}},
                {system: {environment: {data: {}}}, api: []},
                {system: {environment: {data: undefined}}, api: undefined},
                {system: {environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_with:api.error,api.errorMessage',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {environment: {data: []}}, api: {error: '', errorMessage: true}},
                {system: {environment: {data: {}}}, api: {error: '', errorMessage: true}},
                {system: {environment: {data: undefined}}, api: {error: true, errorMessage: undefined}},
                {system: {environment: {data: null}}, api: {error: true, errorMessage: undefined}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_with:api.error,api.errorMessage',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('both array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message', error: 'api has error', errorMessage: 'message for api error'},
                            {data: 200, error: ''},
                            {data: false, error: undefined},
                            {data: /^[0-9]*$/i, error: null}
                        ]},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with:system.*.error,system.*.errorMessage',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expects = [
                    { result: false, fail: [['system', '0', 'data']] },
                    { result: false, fail: [['system', '0', 'data']] },
                    { result: false, fail: [['system', '0', 'data']] },
                    { result: false, fail: [['system', '1', 'data']] },
                    { result: false, fail: [['system', '1', 'data']] },
                ];
                const testCases = [
                    {system: [{data: '', error: false}, {data: '', error: undefined}]},
                    {system: [{data: '', error: false}, {data: [], error: undefined}]},
                    {system: [{data: '', error: false}, {data: {}, error: undefined}]},
                    {system: [{data: 'success message', error: undefined}, {data: {}, errorMessage: false} ]},
                    {system: [{data: 'success message', error: undefined}, {data: {}, errorMessage: false}]},
                ];

                testCases.forEach((testCase, index) => {
                    testHelper({
                        parentPath: ['system', '*'],
                        fieldName: 'data',
                        ruleWithOptions: 'required_with:system.*.error,system.*.errorMessage',
                        isNullable: true,
                        presentOnly: true
                    }, [testCase], expects[index]);
                });
            });
        });
        describe('fields in rule is not array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: 200},
                            {data: false},
                            {data: /^[0-9]*$/i}
                        ], error: 'api has error'},
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ]},
                ];

                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with:error',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
            describe('expect [false]', () => {
                const expect = { result: false, fail: [['system', '1', 'data']] };
                const testCases = [
                    {system: [
                            {data: 'success message'},
                            {data: ''},
                            {data: []},
                            {data: undefined},
                            {data: null},
                        ], error: 'api has error'},
                ];
                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with:error',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
        });
        describe('validate field is not array, fields in rule is array', () => {
            describe('expect [true]', () => {
                const expect = { result: true, fail: [] };
                const testCases = [
                    {data: true, api: [{error: false}, {error: false}, {error: false}]},
                    {data: 'null', api: [{error: false}, {error: false}, {error: false}]}
                ];

                testHelper({
                    parentPath: [],
                    fieldName: 'data',
                    ruleWithOptions: 'required_with:api.*.error',
                    isNullable: true,
                    presentOnly: true
                }, testCases, expect);
            });
        });
    });
    describe('no field in rule', () => {
        const expect = { result: true, fail: [] };
        const testCases = [
            {data: ''},
            {data: {}},
            {data: []},
            {data: null},
            {data: undefined},
            {},
            [],
            '',
            41,
            undefined,
            null,
        ];

        testHelper({
            parentPath: [],
            fieldName: 'data',
            ruleWithOptions: 'required_with',
            isNullable: true,
            presentOnly: true
        }, testCases, expect);
    });
});
