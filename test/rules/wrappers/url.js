import testHelper from "./testHelper";

describe('wrappers.url', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {url: 'http://foo.com/blah_blah'},
                {url: 'http://www.example.com/wpstyle/?p=364'},
                {url: 'https://www.example.com/foo/?bar=baz&inga=42&quux'},
                {url: 'http://userid@example.com:8080'},
                {url: 'https://www.youtube.com/watch?v=bfXOu9BnW98'},
            ];
            const nullTestCases = [
                {url: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'url',
                ruleWithOptions: 'url',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'url',
                ruleWithOptions: 'url',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['url']] };
            const testCases = [
                {url: 'http://'},
                {url: 'foo.com'},
                {url: '//a'},
                {url: 'a02e3b0b-c3b8-xf36-8962-5f3d27122a99'},
                {url: -123.456},
                {url: 'id'},
                {url: true},
                {url: new Date('2019-03-01')},
                {url: /^[0-9][a-z]*$/i},
                {url: {}},
                {url: []},
                {url: undefined},
                {url: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'url',
                ruleWithOptions: 'url',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: {user: {page: 'http://foo.com/blah_blah'}}},
                {update: {user: {page: 'http://www.example.com/wpstyle/?p=364'}}},
                {update: {user: {page: 'https://www.example.com/foo/?bar=baz&inga=42&quux'}}},
                {update: {user: {page: 'http://userid@example.com:8080'}}},
                {update: {user: {page: 'https://www.youtube.com/watch?v=bfXOu9BnW98'}}},
            ];
            const nullTestCases = [
                {update: {user: {page: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'page',
                ruleWithOptions: 'url',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'page',
                ruleWithOptions: 'url',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', 'user', 'page']] };
            const testCases = [
                {update: {user: {page: 'http://'}}},
                {update: {user: {page: 'foo.com'}}},
                {update: {user: {page: '//a'}}},
                {update: {user: {page: '8470b099-6d95-4dd5-adfc4598437b'}}},
                {update: {user: {page: 'id'}}},
                {update: {user: {page: 123.456}}},
                {update: {user: {page: false}}},
                {update: {user: {page: new Date('2019-03-01')}}},
                {update: {user: {page: /^[0-9][a-z]*$/i}}},
                {update: {user: {page: {}}}},
                {update: {user: {page: []}}},
                {update: {user: {page: undefined}}},
                {update: {user: {page: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'page',
                ruleWithOptions: 'url',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: [{url: 'http://foo.com/blah_blah'}, {url: 'http://www.example.com/wpstyle/?p=364'}]},
                {update: [{url: 'https://www.example.com/foo/?bar=baz&inga=42&quux'}, {url: 'https://www.youtube.com/watch?v=bfXOu9BnW98'}]},
            ];
            const nullTestCases = [
                {update: [{url: 'http://foo.com/blah_blah'}, {url: null}]},
                {update: [{url: null}, {url: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'url',
                ruleWithOptions: 'url',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'url',
                ruleWithOptions: 'url',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', '1', 'url']] };
            const testCases = [
                {update: [{url: 'http://foo.com/blah_blah'}, {url: 'http://'}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: 'foo.com'}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: '//a'}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: 'cf64170c-399d-11e9-b210-d663bd3d93'}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: 'id'}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: -0.4568}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: false}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: new Date('2019-03-01')}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: /^[0-9][a-z]*$/i}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: []}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: {}}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: undefined}]},
                {update: [{url: 'http://foo.com/blah_blah'}, {url: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'url',
                ruleWithOptions: 'url',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});