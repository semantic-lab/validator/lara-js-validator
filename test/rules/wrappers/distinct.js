import testHelper from "./testHelper";

describe('wrappers.distinct', () => {
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {orders: [{id: 0}, {id: 1}, {id: 2}]},
                {orders: [{id: 'A'}, {id: 'B'}, {id: 'C'}]},
                {orders: [{id: 'Big Mac'}, {id: 'Coke Cola'}, {id: 'Large Fries'}]},
                {orders: []},
            ];
            const nullTestCases = [
                {orders: [{id: 0}, {id: 1}, {id: null}]},
            ];

            testHelper({
                parentPath: ['orders', '*'],
                fieldName: 'id',
                ruleWithOptions: 'distinct',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['orders', '*'],
                fieldName: 'id',
                ruleWithOptions: 'distinct',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['orders', '2', 'id']] },
                { result: false, fail: [['orders', '2', 'id']] },
                { result: false, fail: [['orders', '1', 'id']] },
            ];
            const testCases = [
                {orders: [{id: 0}, {id: 1}, {id: 1}]},
                {orders: [{id: 'A'}, {id: 'B'}, {id: 'A'}]},
                {orders: [{id: 'Big Mac'}, {id: 'Big Mac'}, {id: 'Large Fries'}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['orders', '*'],
                    fieldName: 'id',
                    ruleWithOptions: 'distinct',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});
