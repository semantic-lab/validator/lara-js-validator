import testHelper from "./testHelper";

describe('wrappers.boolean', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {toDelete: false},
                {toDelete: true},
                {toDelete: 0},
                {toDelete: 1},
                {toDelete: '0'},
                {toDelete: '1'},
            ];
            const nullTestCases = [
                {toDelete: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['toDelete']] };
            const testCases = [
                {toDelete: 'user'},
                {toDelete: 41},
                {toDelete: new Date('2019-03-01')},
                {toDelete: /^[0-9][a-z]*$/i},
                {toDelete: {}},
                {toDelete: []},
                {toDelete: undefined},
                {toDelete: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: {user: {toDelete: false}}},
                {update: {user: {toDelete: true}}},
                {update: {user: {toDelete: 0}}},
                {update: {user: {toDelete: 1}}},
                {update: {user: {toDelete: '0'}}},
                {update: {user: {toDelete: '1'}}},
            ];
            const nullTestCases = [
                {update: {user: {toDelete: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', 'user', 'toDelete']] };
            const testCases = [
                {update: {user: {toDelete: 'user'}}},
                {update: {user: {toDelete: 41}}},
                {update: {user: {toDelete: new Date('2019-03-01')}}},
                {update: {user: {toDelete: /^[0-9][a-z]*$/i}}},
                {update: {user: {toDelete: {}}}},
                {update: {user: {toDelete: []}}},
                {update: {user: {toDelete: undefined}}},
                {update: {user: {toDelete: null}}},
            ];

            testHelper({
                parentPath: ['update', 'user'],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: 0}]},
                {update: [{toDelete: 1}, {toDelete: '0'}, {toDelete: '1'}]},
            ];
            const nullTestCases = [
                {update: [{toDelete: 1}, {toDelete: '0'}, {toDelete: null}]},
                {update: [{toDelete: null}, {toDelete: null}, {toDelete: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['update', '2', 'toDelete']] };
            const testCases = [
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: 'user'}]},
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: 41}]},
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: new Date('2019-03-01')}]},
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: /^[0-9][a-z]*$/i}]},
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: []}]},
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: {}}]},
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: undefined}]},
                {update: [{toDelete: true}, {toDelete: false}, {toDelete: null}]},
            ];

            testHelper({
                parentPath: ['update', '*'],
                fieldName: 'toDelete',
                ruleWithOptions: 'boolean',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});