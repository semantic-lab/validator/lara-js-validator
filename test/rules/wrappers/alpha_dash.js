import testHelper from "./testHelper";

describe('wrappers().alpha_dash', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {title: 'al_Pha'},
                {title: 'al-Pha'},
                {title: 'Roger-Federer'},
                {title: 'Dirk_Nowitzki'},
                {title: 'Nowitzki_41'},
                {title: 'Dirk-100'},
            ];
            const nullTestCases = [
                {title: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha_dash',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha_dash',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['title']] };
            const testCases = [
                {title: 'al_Pha!'},
                {title: 'al-Pha?'},
                {title: 'Roger Federer'},
                {title: 'Dirk Nowitzki 41'},
                {title: 0},
                {title: true},
                {title: false},
                {title: []},
                {title: {}},
                {title: null},
                {title: undefined},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'title',
                ruleWithOptions: 'alpha_dash',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: {name: {firstName: 'di_rk'}}},
                {user: {name: {firstName: 'RO-GER'}}},
                {user: {name: {firstName: '41-dirk'}}},
                {user: {name: {firstName: '100-ROGER'}}},
            ];
            const nullTestCases = [
                {user: {name: {firstName: null}}},
            ];

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha_dash',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha_dash',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', 'name', 'firstName']] };
            const testCases = [
                {user: {name: {firstName: 'Dr.CHIANG'}}},
                {user: {name: {firstName: 'Dirk '}}},
                {user: {name: {firstName: 'Dirk!'}}},
                {user: {name: {firstName: []}}},
                {user: {name: {firstName: {}}}},
                {user: {name: {firstName: undefined}}},
                {user: {name: {firstName: null}}},
            ];

            testHelper({
                parentPath: ['user', 'name'],
                fieldName: 'firstName',
                ruleWithOptions: 'alpha_dash',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{title: 'DevOps41'}, {title: 'R-D'}, {title: 'Doc_tor'}]},
            ];
            const nullTestCases = [
                {user: [{title: 'DevOps41'}, {title: 'R-D'}, {title: null}]},
                {user: [{title: null}, {title: null}, {title: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'title',
                ruleWithOptions: 'alpha_dash',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'title',
                ruleWithOptions: 'alpha_dash',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['user', '0', 'title']] },
                { result: false, fail: [['user', '1', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
                { result: false, fail: [['user', '2', 'title']] },
            ];
            const testCases = [
                {user: [{title: 'DevOps 41'}, {title: 'R-D'}, {title: 'Doc_tor'}]},
                {user: [{title: 'DevOps41'}, {title: 'R&D'}, {title: 'Doc_tor'}]},
                {user: [{title: 'DevOps41'}, {title: 'R-D'}, {title: 'Dr CHIANG'}]},
                {user: [{title: 'DevOps41'}, {title: 'R-D'}, {title: []}]},
                {user: [{title: 'DevOps41'}, {title: 'R-D'}, {title: {}}]},
                {user: [{title: 'DevOps41'}, {title: 'R-D'}, {title: undefined}]},
                {user: [{title: 'DevOps41'}, {title: 'R-D'}, {title: null}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['user', '*'],
                    fieldName: 'title',
                    ruleWithOptions: 'alpha_dash',
                    isNullable: false,
                    presentOnly: false
                }, [testCase], expects[index]);
            });
        });
    });
});