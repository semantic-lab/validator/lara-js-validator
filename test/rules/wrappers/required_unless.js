import testHelper from "./testHelper";

describe('wrappers.required_unless', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {auth: 'admin', data: 'success message'},
                {auth: 'admin', data: 200},
                {auth: 'admin', data: false},
                {auth: 'admin', data: /^[0-9]*$/i},
                {auth: 'user', data: []},
                {auth: 'user', data: {}},
                {auth: 'user', data: undefined},
                {auth: 'user', data: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_unless:auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['data']] };
            const testCases = [
                {auth: 'admin', data: []},
                {auth: 'admin', data: {}},
                {auth: 'admin', data: undefined},
                {auth: 'admin', data: null},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'data',
                ruleWithOptions: 'required_unless:auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: {auth: 'admin', environment: {data: 'success message'}}},
                {system: {auth: 'admin', environment: {data: 200}}},
                {system: {auth: 'admin', environment: {data: false}}},
                {system: {auth: 'admin', environment: {data: /^[0-9]*$/i}}},
                {system: {auth: 'user', environment: {data: []}}},
                {system: {auth: 'user', environment: {data: {}}}},
                {system: {auth: 'user', environment: {data: undefined}}},
                {system: {auth: 'user', environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_unless:system.auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['system', 'environment', 'data']] };
            const testCases = [
                {system: {auth: 'admin', environment: {data: []}}},
                {system: {auth: 'admin', environment: {data: {}}}},
                {system: {auth: 'admin', environment: {data: undefined}}},
                {system: {auth: 'admin', environment: {data: null}}},
            ];

            testHelper({
                parentPath: ['system', 'environment'],
                fieldName: 'data',
                ruleWithOptions: 'required_unless:system.auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        const rule = 'required_unless:system.*.auth,user';
        const ruleName = rule.split(':')[0];
        const paths = ['system', '*', 'data'];
        const auth = 'admin';
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {system: [
                        {auth: 'admin', data: 'success message'},
                        {auth: 'admin', data: 200},
                        {auth: 'admin', data: false},
                        {auth: 'admin', data: /^[0-9]*$/i}
                    ]},
                {system: [
                        {auth: 'user', data: ''}, // not need be required
                        {auth: 'user', data: []}, // not need be required
                        {auth: 'user', data: undefined}, // not need be required
                        {auth: 'user', data: null}, // not need be required
                    ]},
            ];

            testHelper({
                parentPath: ['system', '*'],
                fieldName: 'data',
                ruleWithOptions: 'required_unless:system.*.auth,user',
                isNullable: true,
                presentOnly: true
            }, testCases, expect);
        });
        describe('expect [false]', () => {
            const expects = [
                { result: false, fail: [['system', '1', 'data']] },
                { result: false, fail: [['system', '1', 'data']] },
                { result: false, fail: [['system', '0', 'data']] },
                { result: false, fail: [['system', '0', 'data']] },
                { result: false, fail: [['system', '0', 'data']] },
            ];
            const testCases = [
                {system: [{auth: 'admin', data: 'success message'}, {auth: 'admin', data: ''}]},
                {system: [{auth: 'admin', data: 'success message'}, {auth: 'admin', data: []}]},
                {system: [{auth: 'admin', data: {}}, {auth: 'admin', data: 'success message'}]},
                {system: [{auth: 'admin', data: undefined}, {auth: 'admin', data: 'success message'}]},
                {system: [{auth: 'admin', data: null}, {auth: 'admin', data: 'success message'}]},
            ];

            testCases.forEach((testCase, index) => {
                testHelper({
                    parentPath: ['system', '*'],
                    fieldName: 'data',
                    ruleWithOptions: 'required_unless:system.*.auth,user',
                    isNullable: true,
                    presentOnly: true
                }, [testCase], expects[index]);
            });
        });
    });
});
