import testHelper from "./testHelper";

describe('wrappers.starts_with', () => {
    describe('one layer data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {address: 'Taichung City 402'},
                {address: 'Taichung South Dist.'},
                {address: 'Taipei City 110'},
                {address: 'Taipei Xinyi Dist.'},
                {address: 'Tainan City 71755'},
                {address: 'Tainan Rende Dist.'},
            ];
            const nullTestCases = [
                {address: null}
            ];

            testHelper({
                parentPath: [],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: [],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['address']] };
            const testCases = [
                {address: 'South Dist.'},
                {address: 'Xinyi Dist.'},
                {address: 'Rende Dist.'},
                {address: 0},
                {address: 41},
                {address: {}},
                {address: []},
                {address: undefined},
                {address: null},
                {address: new Date('2019-03-01')},
            ];

            testHelper({
                parentPath: [],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('three layers data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {order: {user: {address: 'Taichung City 402'}}},
                {order: {user: {address: 'Taichung South Dist.'}}},
                {order: {user: {address: 'Taipei City 110'}}},
                {order: {user: {address: 'Taipei Xinyi Dist.'}}},
                {order: {user: {address: 'Tainan City 71755'}}},
                {order: {user: {address: 'Tainan Rende Dist.'}}},
            ];
            const nullTestCases = [
                {order: {user: {address: null}}},
            ];

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['order', 'user', 'address']] };
            const testCases = [
                {order: {user: {address: 'South Dist.'}}},
                {order: {user: {address: 'Xinyi Dist.'}}},
                {order: {user: {address: 'Rende Dist.'}}},
                {order: {user: {address: 0}}},
                {order: {user: {address: 41}}},
                {order: {user: {address: new Date('2019-03-01')}}},
                {order: {user: {address: []}}},
                {order: {user: {address: {}}}},
                {order: {user: {address: undefined}}},
                {order: {user: {address: null}}},
            ];

            testHelper({
                parentPath: ['order', 'user'],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
    describe('array data', () => {
        describe('expect [true]', () => {
            const expect = { result: true, fail: [] };
            const testCases = [
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: 'Tainan City 71755'}]},
            ];
            const nullTestCases = [
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: null}]},
                {user: [{address: null}, {address: null}, {address: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: true,
                presentOnly: false
            }, nullTestCases, expect);
        });
        describe('expect [false]', () => {
            const expect = { result: false, fail: [['user', '2', 'address']] };
            const testCases = [
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: 'South Dist.'}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: 'Xinyi Dist.'}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: 'Rende Dist.'}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: 0}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: new Date('2019-03-01')}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: []}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: {}}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: undefined}]},
                {user: [{address: 'Taichung City 402'}, {address: 'Taipei City 110'}, {address: null}]},
            ];

            testHelper({
                parentPath: ['user', '*'],
                fieldName: 'address',
                ruleWithOptions: 'starts_with:Taipei,Taichung,Tainan',
                isNullable: false,
                presentOnly: false
            }, testCases, expect);
        });
    });
});