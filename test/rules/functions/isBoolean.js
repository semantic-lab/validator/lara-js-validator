import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isBoolean', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('[true]', () => {
            assert.strictEqual(rules.isBoolean(true), expect);
        });
        it('[false]', () => {
            assert.strictEqual(rules.isBoolean(false), expect);
        });
        it('[1]', () => {
            assert.strictEqual(rules.isBoolean(1), expect);
        });
        it('[0]', () => {
            assert.strictEqual(rules.isBoolean(0), expect);
        });
        it('["1"]', () => {
            assert.strictEqual(rules.isBoolean('1'), expect);
        });
        it('["0"]', () => {
            assert.strictEqual(rules.isBoolean('0'), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[41]', () => {
            assert.strictEqual(rules.isBoolean(41), expect);
        });
        it('[{}]', () => {
            assert.strictEqual(rules.isBoolean({}), expect);
        });
        it('[[]]', () => {
            assert.strictEqual(rules.isBoolean([]), expect);
        });
        it('[undefined]', () => {
            assert.strictEqual(rules.isBoolean(undefined), expect);
        });
        it('["string"]', () => {
            assert.strictEqual(rules.isBoolean('string'), expect);
        });
        it('[null]', () => {
            assert.strictEqual(rules.isBoolean(null), expect);
        });
    });
});