import assert from 'assert';
import rules from '../../../src/rules/index';
const fileTestCaseSkipMessage = '\n\t\t(skip while File is not in test environment)';

function createFile () {
    return new File(
        ['I am a text file'],
        'test.txt',
        {
            type: "text/plain",
            lastModified: new Date()
        }
    );
}

describe('Rules().greaterThan', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["6chars" > 5]', () => {
            assert.strictEqual(rules.greaterThan('6chars', 5), expect);
        });
        it('[41 > 40]', () => {
            assert.strictEqual(rules.greaterThan(41, 40), expect);
        });
        it('[[1, 2, 3] > 2]', () => {
            assert.strictEqual(rules.greaterThan([1, 2, 3], 2), expect);
        });
        it(`[File(16) > 15]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.greaterThan(file, 15), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["6chars" > 8]', () => {
            assert.strictEqual(rules.greaterThan('6chars', 8), expect);
        });
        it('[41 > 42]', () => {
            assert.strictEqual(rules.greaterThan(41, 42), expect);
        });
        it('[[1, 2, 3] > 4]', () => {
            assert.strictEqual(rules.greaterThan([1, 2, 3], 4), expect);
        });
        it(`[File(16) > 17]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.greaterThan(file, 17), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
});