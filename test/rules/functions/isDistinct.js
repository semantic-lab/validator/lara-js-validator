import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isDistinct', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('[[0, 1, 2]]', () => {
            assert.strictEqual(rules.isDistinct([0, 1, 2]), expect);
        });
        it('[["A", "B", "C"]]', () => {
            assert.strictEqual(rules.isDistinct(['A', 'B', 'C']), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[[0, 1, 0]]', () => {
            assert.strictEqual(rules.isDistinct([0, 1, 0]), expect);
        });
        it('[["A", "B", "B"]]', () => {
            assert.strictEqual(rules.isDistinct(['A', 'B', 'B']), expect);
        });
        it('[[[], "B", []]]', () => {
            assert.strictEqual(rules.isDistinct([[], 'B', []]), expect);
        });
        it('[["A", null, null]]', () => {
            assert.strictEqual(rules.isDistinct(['A', null, null]), expect);
        });
        it('[[undefined, "B", undefined]]', () => {
            assert.strictEqual(rules.isDistinct([undefined, 'B', undefined]), expect);
        });
    });
});
