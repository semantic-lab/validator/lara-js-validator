import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().notRegex', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["01234", /^[0123]*$/gi]', () => {
            assert.strictEqual(rules.notRegex('01234', /^[0123]*$/gi), expect);
        });
        it('["01234", /^(?![01])[0123]*$/gi]', () => {
            assert.strictEqual(rules.notRegex('01234', /^(?![01])[0123]*$/gi), expect);
        });
        it('["10234", /^(?![01])[0123]*$/gi]', () => {
            assert.strictEqual(rules.notRegex('10234', /^(?![01])[0123]*$/gi), expect);
        });
        it('["0928", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.notRegex('0928', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
        it('["0000000000", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.notRegex('0000000000', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["01234", /^[0-9]*$/gi]', () => {
            assert.strictEqual(rules.notRegex('01234', /^[0-9]*$/gi), expect);
        });
        it('["01234", /^\\d*$/gi]', () => {
            assert.strictEqual(rules.notRegex('01234', /^\d*$/gi), expect);
        });
        it('["1qaz2wsx", /^[0-9a-zA-Z]*$/gi]', () => {
            assert.strictEqual(rules.notRegex('1qaz2wsx', /^[0-9a-zA-Z]*$/gi), expect);
        });
        it('["0928317900", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.notRegex('0928317900', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
        it('["0900000000", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.notRegex('0900000000', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
    });
});
