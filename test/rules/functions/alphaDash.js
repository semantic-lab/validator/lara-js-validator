import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().alphaDash', () => {
    describe('expect [true]', () => {
        const expect = true;
        const validAlphas = [
            'abc', 'aBc', 'ABC',
            'a1', 'Z1', '123',
            'a-bc', 'ab_c', '-_',
        ];
        validAlphas.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.alphaDash(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        const invalidAlphas = [
            'abc?', 'abc!', 'abc.', '@#$%&*',
            undefined, null, [], {}, true, false,
            /^[0-9a-zA-Z]*$/i
        ];
        invalidAlphas.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.alphaDash(ele), expect);
            });
        });
    });
});