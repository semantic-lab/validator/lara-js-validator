import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().nullable', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["string"]', () => {
            assert.strictEqual(rules.nullable('string'), expect);
        });
        it('[41]', () => {
            assert.strictEqual(rules.nullable(41), expect);
        });
        it('[Date("20119-03-01")]', () => {
            assert.strictEqual(rules.nullable(new Date('2019-03-01')), expect);
        });
        it('[[]]', () => {
            assert.strictEqual(rules.nullable([]), expect);
        });
        it('[{}]', () => {
            assert.strictEqual(rules.nullable({}), expect);
        });
        it('[null]', () => {
            assert.strictEqual(rules.nullable(null), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[undefined]', () => {
            assert.strictEqual(rules.nullable(undefined), expect);
        });
    });
});