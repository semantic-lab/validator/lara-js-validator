import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().digits', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('[-123.45, 3]', () => {
            assert.strictEqual(rules.digits(-123.45, 3), expect);
        });
        it('[-1.45, 1]', () => {
            assert.strictEqual(rules.digits(-1.45, 1), expect);
        });
        it('[-1, 1]', () => {
            assert.strictEqual(rules.digits(-1, 1), expect);
        });
        it('[-0.23456, 1]', () => {
            assert.strictEqual(rules.digits(-0.23456, 1), expect);
        });
        it('[0, 1]', () => {
            assert.strictEqual(rules.digits(0, 1), expect);
        });
        it('[0.789, 1]', () => {
            assert.strictEqual(rules.digits(0.789, 1), expect);
        });
        it('[1, 1]', () => {
            assert.strictEqual(rules.digits(1, 1), expect);
        });
        it('[1.45, 1]', () => {
            assert.strictEqual(rules.digits(1.45, 1), expect);
        });
        it('[1789.56, 4]', () => {
            assert.strictEqual(rules.digits(1789.56, 4), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[-123.45, 4]', () => {
            assert.strictEqual(rules.digits(-123.45, 4), expect);
        });
        it('[0, 0]', () => {
            assert.strictEqual(rules.digits(0, 0), expect);
        });
        it('[1789.56, 3]', () => {
            assert.strictEqual(rules.digits(1789.56, 3), expect);
        });
        it('[[], 1]', () => {
            assert.strictEqual(rules.digits([], 1), expect);
        });
        it('[{}, 1]', () => {
            assert.strictEqual(rules.digits({}, 1), expect);
        });
        it('[undefined, 1]', () => {
            assert.strictEqual(rules.digits(undefined, 1), expect);
        });
        it('[null, 1]', () => {
            assert.strictEqual(rules.digits(null, 1), expect);
        });
    });
});