import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().required', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["data", "!@#$._-"]', () => {
            assert.strictEqual(rules.required('data'), expect);
            assert.strictEqual(rules.required('!@#$._-'), expect);
        });
        it('[0, 1, 123]', () => {
            assert.strictEqual(rules.required(0), expect);
            assert.strictEqual(rules.required(1), expect);
            assert.strictEqual(rules.required(123), expect);
        });
        it('[true, false]', () => {
            assert.strictEqual(rules.required(true), expect);
            assert.strictEqual(rules.required(false), expect);
        });
        it('[/^[0-9a-zA-Z]*$/g]', () => {
            assert.strictEqual(rules.required(/^[0-9a-zA-Z]*$/g), expect);
        });
        it('[[0, 1, 2], ["A", "B", "C"]]', () => {
            assert.strictEqual(rules.required([0, 1, 2]), expect);
            assert.strictEqual(rules.required(["A", "B", "C"]), expect);
        });
        it('[{index: 0}, {prop0: 0, prop1: 1}]', () => {
            assert.strictEqual(rules.required({index: 0}), expect);
            assert.strictEqual(rules.required({prop0: 0, prop1: 1}), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[undefined]', () => {
            assert.strictEqual(rules.required(undefined), expect);
        });
        it('[null]', () => {
            assert.strictEqual(rules.required(null), expect);
        });
        it('[""]', () => {
            assert.strictEqual(rules.required(''), expect);
        });
        it('[[]]', () => {
            assert.strictEqual(rules.required([]), expect);
        });
        it('[{}]', () => {
            assert.strictEqual(rules.required({}), expect);
        });
    });
});