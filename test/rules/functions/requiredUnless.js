import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().requiredUnless', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["data", ["account", "password"]]', () => {
            assert.strictEqual(rules.requiredUnless('data', 'account', 'password'), expect);
        });
        it('[false, [41, 42]]', () => {
            assert.strictEqual(rules.requiredUnless(false, 41, 42), expect);
        });
        it('[41, [["A", "B"], ["A"]]', () => {
            assert.strictEqual(rules.requiredUnless(41, ['A', 'B'], ['A']), expect);
        });
        it('[{index: 0}, [{index: 41}, {index: 42}]', () => {
            assert.strictEqual(rules.requiredUnless({index: 0}, {index: 41}, {index: 42}), expect);
        });
        it('[undefined, ["account", "account"]]', () => {
            assert.strictEqual(rules.requiredUnless(undefined, 'account', 'account'), expect);
        });
        it('[null, [41, 41]]', () => {
            assert.strictEqual(rules.requiredUnless(null, 41, 41), expect);
        });
        it('[[], [["A", "B"], ["A", "B"]]', () => {
            assert.strictEqual(rules.requiredUnless([], ['A', 'B'], ['A', 'B']), expect);
        });
        it('[{}, [true, true]', () => {
            assert.strictEqual(rules.requiredUnless({}, true, true), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[undefined, ["account", "password"]]', () => {
            assert.strictEqual(rules.requiredUnless(undefined, 'account', 'password'), expect);
        });
        it('[null, [41, 42]]', () => {
            assert.strictEqual(rules.requiredUnless(null, 41, 42), expect);
        });
        it('[[], [["A", "B"], ["A"]]', () => {
            assert.strictEqual(rules.requiredUnless([], ['A', 'B'], ['A']), expect);
        });
        it('[{}, [true, false]', () => {
            assert.strictEqual(rules.requiredUnless({}, true, false), expect);
        });
    });
});