import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().alphaNum', () => {
    describe('expect [true]', () => {
        const expect = true;
        const validAlphas = [
            'abc', 'aBc', 'ABC',
            'a1', 'Z1', '123',
        ];
        validAlphas.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.alphaNum(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        const invalidAlphas = [
            'a-bc', '12_3', '-_',
            'abc?', 'abc!', 'abc.', '@#$%&*',
        ];
        invalidAlphas.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.alphaNum(ele), expect);
            });
        });
    });
});