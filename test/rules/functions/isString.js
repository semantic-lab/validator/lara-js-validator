import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isString', () => {
    describe('expect [true]', () => {
        const expect = true;
        const testValues = [
            'string', '', '123', '[0-9]',
            '.!@#$%^&&*()_-+=',
        ];
        testValues.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.isString(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        const testValues = [
            123, /[0-9]/gi,
            [], {}, undefined, null,
        ];
        testValues.forEach((ele) => {
            it(`[${ele}]`, () => {
                assert.strictEqual(rules.isString(ele), expect);
            });
        });
    });
});