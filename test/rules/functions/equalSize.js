import assert from 'assert';
import rules from '../../../src/rules/index';
const fileTestCaseSkipMessage = '\n\t\t(skip while File is not in test environment)';

function createFile () {
    return new File(
        ['I am a text file'],
        'test.txt',
        {
            type: "text/plain",
            lastModified: new Date()
        }
    );
}

describe('Rules().equalSize', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["6chars" = 6]', () => {
            assert.strictEqual(rules.equalSize('6chars', 6), expect);
        });
        it('[41 = 41]', () => {
            assert.strictEqual(rules.equalSize(41, 41), expect);
        });
        it('[[1, 2, 3] = 3]', () => {
            assert.strictEqual(rules.equalSize([1, 2, 3], 3), expect);
        });
        it(`[File(16) = 16]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.equalSize(file, 16), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["6chars" = 7]', () => {
            assert.strictEqual(rules.equalSize('6chars', 7), expect);
        });
        it('[41 = 40]', () => {
            assert.strictEqual(rules.equalSize(41, 40), expect);
        });
        it('[[1, 2, 3] = 4]', () => {
            assert.strictEqual(rules.equalSize([1, 2, 3], 4), expect);
        });
        it(`[File(16) = 17]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.equalSize(file, 17), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
});