import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().startWith', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["A1234", ["A1234"]]', () => {
            assert.strictEqual(rules.startWith('A1234', ['A1234']), expect);
        });
        it('["A1234", ["A"]]', () => {
            assert.strictEqual(rules.startWith('A1234', ['A1234']), expect);
        });
        it('["A1234", ["A", "1", "2"]]', () => {
            assert.strictEqual(rules.startWith('A1234', ['A', '1', '2']), expect);
        });
        it('["1A234", ["A", "1", "2"]]', () => {
            assert.strictEqual(rules.startWith('1A234', ['A', '1', '2']), expect);
        });
        it('["2A134", ["A", "1", "2"]]', () => {
            assert.strictEqual(rules.startWith('2A134', ['A', '1', '2']), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["A1234", ["12A34"]]', () => {
            assert.strictEqual(rules.startWith('A1234', ['12A34']), expect);
        });
        it('["A1234", ["1"]]', () => {
            assert.strictEqual(rules.startWith('A1234', ['1']), expect);
        });
        it('["2A134", ["a", "b", "c"]]', () => {
            assert.strictEqual(rules.startWith('2A134', ['a', 'b', 'c']), expect);
        });
        it('[[], ["a", "b", "c"]]', () => {
            assert.strictEqual(rules.startWith([], ['a', 'b', 'c']), expect);
        });
        it('[{}, ["a", "b", "c"]]', () => {
            assert.strictEqual(rules.startWith({}, ['a', 'b', 'c']), expect);
        });
        it('[undefined, ["a", "b", "c"]]', () => {
            assert.strictEqual(rules.startWith(undefined, ['a', 'b', 'c']), expect);
        });
        it('[null, ["a", "b", "c"]]', () => {
            assert.strictEqual(rules.startWith(null, ['a', 'b', 'c']), expect);
        });
    });
});
