import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().alpha', () => {
    describe('expect [true]', () => {
        const expect = true;
        const validAlphas = ['abc', 'aBc', 'ABC'];
        validAlphas.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.alpha(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        const invalidAlphas = [
            'a1', 'Z1', '123',
            'a-bc', 'ab_c', '-_',
            'abc?', 'abc!', 'abc.', '@#$%&*',
        ];
        invalidAlphas.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.alpha(ele), expect);
            });
        });
    });
});