import assert from 'assert';
import rules from '../../../src/rules/index';
const d20190217 = new Date('2019-02-17');
const d19911227 = new Date('1991-12-27');
const t20190217 = d20190217.getTime();
const t19911227 = d19911227.getTime();
const s20190217 = '2019-02-17';

describe('Rules().equalDate', () => {
    describe('expect [true]', () => {
        const expert = true;
        it('[d20190217 = d20190217] (also compare with different instance)', () => {
            assert.strictEqual(rules.equalDate(d20190217, d20190217), expert);
            assert.strictEqual(rules.equalDate(d20190217, new Date('2019-02-17')), expert);
        });
        it('[d20190217 = t20190217]', () => {
            assert.strictEqual(rules.equalDate(d20190217, t20190217), expert);
        });
        it('[t20190217 = t20190217]', () => {
            assert.strictEqual(rules.equalDate(t20190217, t20190217), expert);
        });
    });
    describe('expect [false]', () => {
        const expert = false;
        it('[d20190217 = d19911227]', () => {
            assert.strictEqual(rules.equalDate(d20190217, d19911227), expert);
            assert.strictEqual(rules.equalDate(d20190217, new Date('1991-12-27')), expert);
        });
        it('[d20190217 = t19911227]', () => {
            assert.strictEqual(rules.equalDate(d20190217, t19911227), expert);
        });
        it('[t20190217 = t19911227]', () => {
            assert.strictEqual(rules.equalDate(t20190217, t19911227), expert);
        });
        it('[s20190217 = t20190217] (type error)', () => {
            assert.strictEqual(rules.equalDate(s20190217, t20190217), expert);
        });
    });
});