import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isEmail', () => {
    describe('expect [true]', () => {
        const expect = true;
        const wikipediaTestValues = [
            'simple@example.com',
            'very.common@example.com',
            'disposable.style.email.with+symbol@example.com',
            'other.email-with-hyphen@example.com',
            'fully-qualified-domain@example.com',
            'user.name+tag+sorting@example.com',
            'x@example.com',
            'example-indeed@strange-example.com',
            'example@s.example',
            '" "@example.org',
            '"john..doe"@example.org',
        ];
        wikipediaTestValues.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.isEmail(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        const wikipediaTestValues = [
            'Abc.example.com',
            'A@b@c@example.com',
            'a"b(c)d,e:f;g<h>i[j\\k]l@example.com',
            'just"not"right@example.com',
            'this is"not\\allowed@example.com',
            'this\\ still\\"not\\\\allowed@example.com',
        ];
        wikipediaTestValues.forEach((ele) => {
            it(`["${ele}"]`, () => {
                assert.strictEqual(rules.isEmail(ele), expect);
            });
        });
    });
});