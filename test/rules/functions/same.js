import assert from 'assert';
import rules from '../../../src/rules/index';

class SmartPhone {
    constructor(phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

describe('Rules().same', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["A12bC", "A12bC"]', () => {
            assert.strictEqual(rules.same('A12bC', 'A12bC'), expect);
        });
        it('[0.41, 0.41]', () => {
            assert.strictEqual(rules.same(0.41, 0.41), expect);
        });
        it('[true, true]', () => {
            assert.strictEqual(rules.same(true, true), expect);
        });
        it('[undefined, undefined]', () => {
            assert.strictEqual(rules.same(undefined, undefined), expect);
        });
        it('[null, null]', () => {
            assert.strictEqual(rules.same(null, null), expect);
        });
        it('[[], []]', () => {
            assert.strictEqual(rules.same([], []), expect);
        });
        it('[[1, 2, 3], [1, 2, 3]]', () => {
            assert.strictEqual(rules.same([1, 2, 3], [1, 2, 3]), expect);
        });
        it('[{}, {}]', () => {
            assert.strictEqual(rules.same({}, {}), expect);
        });
        it('[{index: 1}, {index: 1}]', () => {
            assert.strictEqual(rules.same({index: 1}, {index: 1}), expect);
        });
        it('[SmartPhone("0900000000"), SmartPhone("0900000000")]', () => {
            assert.strictEqual(rules.same(new SmartPhone('0900000000'), new SmartPhone('0900000000')), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["A12bC", "A1xbC"]', () => {
            assert.strictEqual(rules.same('A12bC', 'A1xbC'), expect);
        });
        it('[0.41, 41]', () => {
            assert.strictEqual(rules.same(0.41, 41), expect);
        });
        it('[true, false]', () => {
            assert.strictEqual(rules.same(true, false), expect);
        });
        it('[undefined, null]', () => {
            assert.strictEqual(rules.same(undefined, null), expect);
        });
        it('[null, undefined]', () => {
            assert.strictEqual(rules.same(null, undefined), expect);
        });
        it('[[], [1, 2, 3]]', () => {
            assert.strictEqual(rules.same([], [1, 2, 3]), expect);
        });
        it('[{index: 0}, {}]', () => {
            assert.strictEqual(rules.same({index: 0}, {}), expect);
        });
    });
});