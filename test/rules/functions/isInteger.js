import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isInteger', () => {
    describe('expect [true]', () => {
        const expect = true;
        const testValues = [
            -999999, -1, 0, 1, 10, 999999999999999
        ];
        testValues.forEach((ele) => {
            it(`[${ele}]`, () => {
                assert.strictEqual(rules.isInteger(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        const testValues = [
            '-9999', '0', 0.123, 10.345,
            [], {}, undefined, null,
        ];
        testValues.forEach((ele) => {
            it(`[${ele}]`, () => {
                assert.strictEqual(rules.isInteger(ele), expect);
            });
        });
    });
});