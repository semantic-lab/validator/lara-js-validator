import assert from 'assert';
import rules from '../../../src/rules/index';
const d20190217 = new Date('2019-02-17');
const d19911227 = new Date('1991-12-27');
const t20190217 = d20190217.getTime();
const t19911227 = d19911227.getTime();
const s20190217 = '2019-02-17';

describe('Rules().beforeDate', () => {
    describe('expect [true]', () => {
        const expert = true;
        it('[d19911227 < d20190217]', () => {
            assert.strictEqual(rules.beforeDate(d19911227, d20190217), expert);
        });
        it('[t19911227 < d20190217]', () => {
            assert.strictEqual(rules.beforeDate(t19911227, d20190217), expert);
        });
        it('[d19911227 < t20190217]', () => {
            assert.strictEqual(rules.beforeDate(d19911227, t20190217), expert);
        });
        it('[t19911227 < t20190217]', () => {
            assert.strictEqual(rules.beforeDate(t19911227, t20190217), expert);
        });
    });
    describe('expect [false]', () => {
        const expert = false;
        it('[d20190217 < d19911227]', () => {
            assert.strictEqual(rules.beforeDate(d20190217, d19911227), expert);
        });
        it('[d20190217 < t19911227]', () => {
            assert.strictEqual(rules.beforeDate(d20190217, t19911227), expert);
        });
        it('[t20190217 < d19911227]', () => {
            assert.strictEqual(rules.beforeDate(t20190217, d19911227), expert);
        });
        it('[t20190217 < t19911227]', () => {
            assert.strictEqual(rules.beforeDate(t20190217, t19911227), expert);
        });
        it('[s20190217 < t20190217] (type error)', () => {
            assert.strictEqual(rules.beforeDate(s20190217, t20190217), expert);
        });
    });
});