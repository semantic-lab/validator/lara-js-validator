import assert from 'assert';
import rules from '../../../src/rules/index';
const fileTestCaseSkipMessage = '\n\t\t(skip while File is not in test environment)';

function createFile () {
    return new File(
        ['I am a text file'],
        'test.txt',
        {
            type: "text/plain",
            lastModified: new Date()
        }
    );
}

describe('Rules().between', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('[5 <= "6chars" <= 7, 6 <= "6chars" <= 6]', () => {
            assert.strictEqual(rules.between(5, "6chars", 7), expect);
            assert.strictEqual(rules.between(6, "6chars", 6), expect);
        });
        it('[40 <= 41 <= 42, 41 <= 41 <= 41]', () => {
            assert.strictEqual(rules.between(40, 41, 42), expect);
            assert.strictEqual(rules.between(41, 41, 41), expect);
        });
        it('[2 <= [1, 2, 3] <= 4, 3 <= [1, 2, 3] <= 3]', () => {
            assert.strictEqual(rules.between(2, [1, 2, 3], 4), expect);
            assert.strictEqual(rules.between(3, [1, 2, 3], 3), expect);
        });
        it(`[15 <= File(16) <= 17, 16 <= File(16) <= 16]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.between(15, file, 17), expect);
                assert.strictEqual(rules.between(16, file, 16), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[7 <= "6chars" <= 8, 5 <= "6chars" <= 4]', () => {
            assert.strictEqual(rules.between(7, "6chars", 8), expect);
            assert.strictEqual(rules.between(5, "6chars", 4), expect);
        });
        it('[42 <= 41 <= 43, 40 <= 41 <= 39]', () => {
            assert.strictEqual(rules.between(42, 41, 43), expect);
            assert.strictEqual(rules.between(40, 41, 39), expect);
        });
        it('[4 <= [1, 2, 3] <= 5, 2 <= [1, 2, 3] <= 1]', () => {
            assert.strictEqual(rules.between(4, [1, 2, 3], 5), expect);
            assert.strictEqual(rules.between(2, [1, 2, 3], 1), expect);
        });
        it(`[17 <= File(16) <= 18, 15 <= File(16) <= 14]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.between(17, file, 18), expect);
                assert.strictEqual(rules.between(15, file, 14), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
});