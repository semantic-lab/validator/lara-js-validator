import assert from 'assert';
import rules from '../../../src/rules/index';
const fileTestCaseSkipMessage = '\n\t\t(skip while File is not in test environment)';

function createFile () {
    return new File(
        ['I am a text file'],
        'test.txt',
        {
            type: "text/plain",
            lastModified: new Date()
        }
    );
}

describe('Rules().greaterThanOrEqual', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["6chars" >= 6, "6chars" >= 5]', () => {
            assert.strictEqual(rules.greaterThanOrEqual('6chars', 5), expect);
            assert.strictEqual(rules.greaterThanOrEqual('6chars', 6), expect);
        });
        it('[41 >= 40, 41 >= 41]', () => {
            assert.strictEqual(rules.greaterThanOrEqual(41, 40), expect);
            assert.strictEqual(rules.greaterThanOrEqual(41, 41), expect);
        });
        it('[[1, 2, 3] >= 2, [1, 2, 3] >= 3]', () => {
            assert.strictEqual(rules.greaterThanOrEqual([1, 2, 3], 2), expect);
            assert.strictEqual(rules.greaterThanOrEqual([1, 2, 3], 3), expect);
        });
        it(`[File(16) >= 15, File(16) >= 16]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.greaterThanOrEqual(file, 15), expect);
                assert.strictEqual(rules.greaterThanOrEqual(file, 16), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["6chars" >= 7]', () => {
            assert.strictEqual(rules.greaterThanOrEqual('6chars', 7), expect);
        });
        it('[41 >= 42]', () => {
            assert.strictEqual(rules.greaterThanOrEqual(41, 42), expect);
        });
        it('[[1, 2, 3] >= 4]', () => {
            assert.strictEqual(rules.greaterThanOrEqual([1, 2, 3], 4), expect);
        });
        it(`[File(16) >= 17]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.greaterThanOrEqual(file, 17), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
});