import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().numericLength', () => {
    describe('expect [> -1]', () => {
        it('[-123.45]', () => {
            assert.strictEqual(rules.numericLength(-123.45), 3);
        });
        it('[-1.45]', () => {
            assert.strictEqual(rules.numericLength(-1.45), 1);
        });
        it('[-1]', () => {
            assert.strictEqual(rules.numericLength(-1), 1);
        });
        it('[-0.23456]', () => {
            assert.strictEqual(rules.numericLength(-0.23456), 1);
        });
        it('[0]', () => {
            assert.strictEqual(rules.numericLength(0), 1);
        });
        it('[0.789]', () => {
            assert.strictEqual(rules.numericLength(0.789), 1);
        });
        it('[1]', () => {
            assert.strictEqual(rules.numericLength(1), 1);
        });
        it('[1.45]', () => {
            assert.strictEqual(rules.numericLength(1.45), 1);
        });
        it('[1789.56]', () => {
            assert.strictEqual(rules.numericLength(1789.56), 4);
        });
    });
    describe('expect [-1]', () => {
        const expect = 0;
        it('["0.123"]', () => {
            assert.strictEqual(rules.numericLength('0.123'), expect);
        });
        it('[true]', () => {
            assert.strictEqual(rules.numericLength(true), expect);
        });
        it('[false]', () => {
            assert.strictEqual(rules.numericLength(false), expect);
        });
        it('[[]]', () => {
            assert.strictEqual(rules.numericLength([]), expect);
        });
        it('[{}]', () => {
            assert.strictEqual(rules.numericLength({}), expect);
        });
        it('[undefined]', () => {
            assert.strictEqual(rules.numericLength(undefined), expect);
        });
        it('[null]', () => {
            assert.strictEqual(rules.numericLength(null), expect);
        });
    });
});