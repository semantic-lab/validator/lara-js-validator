import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().accepted', () => {
    describe('except [true]', () => {
        it('["yes", "YES"] (case insensitive)', () => {
            assert.strictEqual(rules.accepted('yes'), true);
            assert.strictEqual(rules.accepted('YES'), true);
        });
        it('["on", "ON"] (case insensitive)', () => {
            assert.strictEqual(rules.accepted('on'), true);
            assert.strictEqual(rules.accepted('ON'), true);
        });
        it('["true", "TRUE"] (case insensitive)', () => {
            assert.strictEqual(rules.accepted('true'), true);
            assert.strictEqual(rules.accepted('TRUE'), true);
        });
        it('["1"]', () => {
            assert.strictEqual(rules.accepted('1'), true);
        });
        it('[1]', () => {
            assert.strictEqual(rules.accepted(1), true);
        });
        it('[true]', () => {
            assert.strictEqual(rules.accepted(true), true);
        });
    });
    describe('except [false]', () => {
        it('["yes1"]', () => {
            assert.strictEqual(rules.accepted('yes1'), false);
        });
        it('["no"]', () => {
            assert.strictEqual(rules.accepted('no'), false);
        });
        it('["off"]', () => {
            assert.strictEqual(rules.accepted('off'), false);
        });
        it('["false"]', () => {
            assert.strictEqual(rules.accepted('false'), false);
        });
        it('["0", "1.1", "2", "100"]', () => {
            assert.strictEqual(rules.accepted('0'), false);
            assert.strictEqual(rules.accepted('1.1'), false);
            assert.strictEqual(rules.accepted('2'), false);
            assert.strictEqual(rules.accepted('100'), false);
        });
        it('[0, 1.1, 2, 100]', () => {
            assert.strictEqual(rules.accepted(0), false);
            assert.strictEqual(rules.accepted(1.1), false);
            assert.strictEqual(rules.accepted(2), false);
            assert.strictEqual(rules.accepted(100), false);
        });
        it('[false]', () => {
            assert.strictEqual(rules.accepted(false), false);
        });
    });
});