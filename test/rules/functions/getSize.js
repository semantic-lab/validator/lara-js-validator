import assert from 'assert';
import rules from '../../../src/rules/index';
const fileTestCaseSkipMessage = '\n\t\t(skip while File is not in test environment)';

function createFile () {
    return new File(
        ['I am a text file'],
        'test.txt',
        {
            type: "text/plain",
            lastModified: new Date()
        }
    );
}

describe('Rules().getSize', () => {
    describe('expect [return size]', () => {
        it('["string"]: 6', () => {
            assert.strictEqual(rules.getSize('string'), 6);
        });
        it('[41]: 41', () => {
            assert.strictEqual(rules.getSize(41), 41);
        });
        it('[[{index: 0}, {index: 1}, {index: 2}]]: 3', () => {
            assert.strictEqual(rules.getSize(
                [{index: 0}, {index: 1}, {index: 2}]),
                3
            );
        });
        it(`[File(16)]: 16${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.getSize(file), 16);
            } catch (error) {
                this.skip();
            }
        });
    });
    describe('expect [null]', () => {
        const expect = null;
        it('[2019-02-18]: null', () => {
            assert.strictEqual(rules.getSize(new Date('2019-02-18')), expect);
        });
        it('[{index: 0}]: null', () => {
            assert.strictEqual(rules.getSize({index: 0}), expect);
        });
    });
});