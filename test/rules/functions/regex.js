import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().regex', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["01234", /^[01234]*$/gi]', () => {
            assert.strictEqual(rules.regex('01234', /^[01234]*$/gi), expect);
        });
        it('["01234", /^[0-9]*$/gi]', () => {
            assert.strictEqual(rules.regex('01234', /^[0-9]*$/gi), expect);
        });
        it('["01234", /^\\d*$/gi]', () => {
            assert.strictEqual(rules.regex('01234', /^\d*$/gi), expect);
        });
        it('["1qaz2wsx", /^[0-9a-zA-Z]*$/gi]', () => {
            assert.strictEqual(rules.regex('1qaz2wsx', /^[0-9a-zA-Z]*$/gi), expect);
        });
        it('["0928317900", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.regex('0928317900', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
        it('["0900000000", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.regex('0900000000', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
        it('["0911111111", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.regex('0911111111', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["01234", /^[0123]*$/gi]', () => {
            assert.strictEqual(rules.regex('01234', /^[0123]*$/gi), expect);
        });
        it('["01234", /^(?![01])[0123]*$/gi]', () => {
            assert.strictEqual(rules.regex('01234', /^(?![01])[0123]*$/gi), expect);
        });
        it('["10234", /^(?![01])[0123]*$/gi]', () => {
            assert.strictEqual(rules.regex('10234', /^(?![01])[0123]*$/gi), expect);
        });
        it('["0928", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.regex('0928', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
        it('["0000000000", /^09\\d{2}-?\\d{3}-?\\d{3}$/i]', () => {
            assert.strictEqual(rules.regex('0000000000', /^09\d{2}-?\d{3}-?\d{3}$/i), expect);
        });
        it('[[], /^[0-9a-zA-Z]*$/gi]', () => {
            assert.strictEqual(rules.regex([], /^[0-9a-zA-Z]*$/gi), expect);
        });
        it('[{}, /^[0-9a-zA-Z]*$/gi]', () => {
            assert.strictEqual(rules.regex({}, /^[0-9a-zA-Z]*$/gi), expect);
        });
        it('[undefined, /^[0-9a-zA-Z]*$/gi]', () => {
            assert.strictEqual(rules.regex(undefined, /^[0-9a-zA-Z]*$/gi), expect);
        });
        it('[null, /^[0-9a-zA-Z]*$/gi]', () => {
            assert.strictEqual(rules.regex(null, /^[0-9a-zA-Z]*$/gi), expect);
        });
    });
});