import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isUUID', () => {
    describe('expect [true]', () => {
        const expect = true;
        const version01TestValues = [
            'cf641482-399d-11e9-b210-d663bd873d93',
            'cf64170c-399d-11e9-b210-d663bd873d93',
            'cf64186a-399d-11e9-b210-d663bd873d93',
            'cf641bd0-399d-11e9-b210-d663bd873d93',
            'cf641d24-399d-11e9-b210-d663bd873d93',
        ];
        const version04TestValues = [
            '715f4edf-5912-4440-af8a-8296129b8501',
            'd4073c23-7234-40bd-8e5b-0e456b1713ec',
            '8470b099-6d95-4dd5-b74b-adfc4598437b',
            '9b348aba-7baf-46c4-b97c-0506285f7757',
            'a02e3b0b-c3b8-4f36-8962-5f3d27122a99',
        ];
        version01TestValues.forEach((ele) => {
            it(`["${ele}", 1]`, () => {
                assert.strictEqual(rules.isUUID(ele, 1), expect);
                assert.strictEqual(rules.isUUID(ele), expect);
            });
        });
        version04TestValues.forEach((ele) => {
            it(`["${ele}", 4]`, () => {
                assert.strictEqual(rules.isUUID(ele, 4), expect);
                assert.strictEqual(rules.isUUID(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["cf641482-399d-11e9-b210-d663bd873d93", 4]', () => {
            assert.strictEqual(rules.isUUID('cf641482-399d-11e9-b210-d663bd873d93', 4), expect);
        });
        it('["715f4edf-5912-4440-af8a-8296129b8501", 1]', () => {
            assert.strictEqual(rules.isUUID('715f4edf-5912-4440-af8a-8296129b8501', 1), expect);
        });
        it('["715s3edf-sd12-6440-ed8a-82s9129b8501", 1]', () => {
            assert.strictEqual(rules.isUUID('715s3edf-sd12-6440-ed8a-82s9129b8501'), expect);
        });
        it('[[]]', () => {
            assert.strictEqual(rules.isUUID([]), expect);
        });
        it('[{}]', () => {
            assert.strictEqual(rules.isUUID({}), expect);
        });
        it('[undefined]', () => {
            assert.strictEqual(rules.isUUID(undefined), expect);
        });
        it('[null]', () => {
            assert.strictEqual(rules.isUUID(null), expect);
        });
    });
});