import assert from 'assert';
import rules from '../../../src/rules/index';
const d20190217 = new Date('2019-02-17');
const d19911227 = new Date('1991-12-27');

describe('Rules().isDate', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('[Date("2019-02-24"])', () => {
            assert.strictEqual(rules.isDate(new Date('2019-02-24')), expect);
        });
        it('[Date("2019.02.24"]")', () => {
            assert.strictEqual(rules.isDate(new Date('2019.02.24')), expect);
        });
        it('[Date("2019/02/24"])', () => {
            assert.strictEqual(rules.isDate(new Date('2019/02/24')), expect);
        });
        it('["2019-02-24"]', () => {
            assert.strictEqual(rules.isDate('2019-02-24'), expect);
        });
        it('["2019.02.24"]', () => {
            assert.strictEqual(rules.isDate('2019.02.24'), expect);
        });
        it('["2019/02/24"]', () => {
            assert.strictEqual(rules.isDate('2019/02/24'), expect);
        });
        it('["2019-02-31"]', () => {
            assert.strictEqual(rules.isDate('2019-02-31'), expect);
        });
        it('[1549900800000]', () => {
            assert.strictEqual(rules.isDate(1549900800000), expect);
        });
        it('[41]', () => {
            assert.strictEqual(rules.isDate(41), expect);
        });

    });
    describe('expect [false]', () => {
        const expect = false;
        it('[Date("2019-13-24"]]', () => {
            assert.strictEqual(rules.isDate(new Date("2019-13-24")), expect);
        });
        it('["2019-02-32"]', () => {
            assert.strictEqual(rules.isDate('2019-02-32'), expect);
        });
        it('[undefined]', () => {
            assert.strictEqual(rules.isDate(undefined), expect);
        });
        it('[null]', () => {
            assert.strictEqual(rules.isDate(null), expect);
        });
        it('[false]', () => {
            assert.strictEqual(rules.isDate(false), expect);
        });
    });
});