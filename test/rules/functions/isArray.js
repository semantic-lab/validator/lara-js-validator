import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isArray', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('[[]]', () => {
            assert.strictEqual(rules.isArray([]), expect);
        });
        it('[["A", "B"]]', () => {
            assert.strictEqual(rules.isArray(['A', 'B']), expect);
        });
        it('[[undefined]]', () => {
            assert.strictEqual(rules.isArray([undefined]), expect);
        });
        it('[[/^[0-9][a-z]*$/g]]', () => {
            assert.strictEqual(rules.isArray([/^[0-9][a-z]*$/g]), expect);
        });
        it('[JSON.parse("[1, 2, 3]")]', () => {
            assert.strictEqual(rules.isArray([JSON.parse("[1, 2, 3]")]), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[41]', () => {
            assert.strictEqual(rules.isArray(41), expect);
        });
        it('["string"]', () => {
            assert.strictEqual(rules.isArray('string'), expect);
        });
        it('[{}]', () => {
            assert.strictEqual(rules.isArray({}), expect);
        });
        it('[/^[0-9][a-z]*$/i]', () => {
            assert.strictEqual(rules.isArray(/^[0-9][a-z]*$/i), expect);
        });
    });
});
