import assert from 'assert';
import rules from '../../../src/rules/index';
const fileTestCaseSkipMessage = '\n\t\t(skip while File is not in test environment)';

function createFile () {
    return new File(
        ['I am a text file'],
        'test.txt',
        {
            type: "text/plain",
            lastModified: new Date()
        }
    );
}

describe('Rules().lessThanOrEqual', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["6chars" <= 6, "6chars" <= 7]', () => {
            assert.strictEqual(rules.lessThanOrEqual('6chars', 6), expect);
            assert.strictEqual(rules.lessThanOrEqual('6chars', 7), expect);
        });
        it('[41 <= 41, 41 <= 42]', () => {
            assert.strictEqual(rules.lessThanOrEqual(41, 41), expect);
            assert.strictEqual(rules.lessThanOrEqual(41, 42), expect);
        });
        it('[[1, 2, 3] <= 3, [1, 2, 3] <= 4]', () => {
            assert.strictEqual(rules.lessThanOrEqual([1, 2, 3], 3), expect);
            assert.strictEqual(rules.lessThanOrEqual([1, 2, 3], 4), expect);
        });
        it(`[File(16) <= 16, File(16) <= 17]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.lessThanOrEqual(file, 16), expect);
                assert.strictEqual(rules.lessThanOrEqual(file, 17), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["6chars" <= 5]', () => {
            assert.strictEqual(rules.lessThanOrEqual('6chars', 5), expect);
        });
        it('[41 <= 40]', () => {
            assert.strictEqual(rules.lessThanOrEqual(41, 40), expect);
        });
        it('[[1, 2, 3] <= 2]', () => {
            assert.strictEqual(rules.lessThanOrEqual([1, 2, 3], 2), expect);
        });
        it(`[File(16) <= 15]${fileTestCaseSkipMessage}`, function() {
            try {
                const file = createFile();
                assert.strictEqual(rules.lessThanOrEqual(file, 15), expect);
            } catch (error) {
                this.skip();
            }
        });
    });
});