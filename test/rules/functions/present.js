import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().present', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["data", {data: "data"}]', () => {
            assert.strictEqual(rules.present('data', {data: 'data'}), expect);
        });
        it('["data", {data: 41}]', () => {
            assert.strictEqual(rules.present('data', {data: 41}), expect);
        });
        it('["data", {data: /^[0-9]*$/i}]', () => {
            assert.strictEqual(rules.present('data', {data: /^[0-9]*$/i}), expect);
        });
        it('["data", {data: undefined}]', () => {
            assert.strictEqual(rules.present('data', {data: undefined}), expect);
        });
        it('["data", {data: null}]', () => {
            assert.strictEqual(rules.present('data', {data: null}), expect);
        });
        it('["data", {data: []}]', () => {
            assert.strictEqual(rules.present('data', {data: []}), expect);
        });
        it('["data", {data: {}}]', () => {
            assert.strictEqual(rules.present("data", {data: {}}), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["data", {column: 123}]', () => {
            assert.strictEqual(rules.present('data', {column: 123}), expect);
        });
        it('[undefined, {data: 41}]', () => {
            assert.strictEqual(rules.present(undefined, {data: 41}), expect);
        });
        it('[null, {data: 41}]', () => {
            assert.strictEqual(rules.present(null, {data: 41}), expect);
        });
        it('[[], {data: 41}]', () => {
            assert.strictEqual(rules.present([], {data: 41}), expect);
        });
        it('[{}, {data: 41}]', () => {
            assert.strictEqual(rules.present({}, {data: 41}), expect);
        });
        it('["data", []]', () => {
            assert.strictEqual(rules.present('data', []), expect);
        });
        it('["data", 41]', () => {
            assert.strictEqual(rules.present('data', 41), expect);
        });
    });
});