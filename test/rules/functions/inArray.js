import assert from 'assert';
import rules from '../../../src/rules/index';

class SmartPhone {
    constructor(phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    getPhone() {
        return this.phoneNumber;
    };
}

describe('Rules().inArray', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["AB", ["A", 1, "AB", true]]', () => {
            assert.strictEqual(rules.inArray('AB', ['A', 1, 'AB', true]), expect);
        });
        it('[true, ["A", 1, "AB", true]]', () => {
            assert.strictEqual(rules.inArray(true, ["A", 1, "AB", true]), expect);
        });
        it('[["A", [], 234], ["A", 1, ["A", [], 234], true]]', () => {
            assert.strictEqual(rules.inArray(['A', [], 234], ['A', 1, ['A', [], 234], true]), expect);
        });
        it('[{index: 0}, ["A", {index: 0}, true]]', () => {
            assert.strictEqual(rules.inArray({index: 0}, ['A', {index: 0}, true]), expect);
        });
        it('[/^[0-9a-zA-Z]*$/i, ["A", /^[0-9a-zA-Z]*$/i, true]]', () => {
            assert.strictEqual(rules.inArray(/^[0-9a-zA-Z]*$/i, ['A', /^[0-9a-zA-Z]*$/i, true]), expect);
        });
        it('[SmartPhone("0900000000"), ["A", SmartPhone("0900000000"), true]]', () => {
            assert.strictEqual(rules.inArray(new SmartPhone('0900000000'), ['A', new SmartPhone('0900000000'), true]), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["AB", ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.inArray('AB', ['A', 1, 'B', true]), expect);
        });
        it('[true, ["A", 1, "AB", false]]', () => {
            assert.strictEqual(rules.inArray(true, ["A", 1, "AB", false]), expect);
        });
        it('[{index: 0}, ["A", {index: 1}, true]]', () => {
            assert.strictEqual(rules.inArray({index: 0}, ['A', {index: 1}, true]), expect);
        });
        it('[SmartPhone("0900000000"), ["A", SmartPhone("0900000001"), true]]', () => {
            assert.strictEqual(rules.inArray(new SmartPhone('0900000000'), ['A', new SmartPhone('0900000001'), true]), expect);
        });
        it('[[], ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.inArray([], ['A', 1, 'B', true]), expect);
        });
        it('[{}, ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.inArray({}, ['A', 1, 'B', true]), expect);
        });
        it('[undefined, ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.inArray(undefined, ['A', 1, 'B', true]), expect);
        });
        it('[null, ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.inArray(null, ['A', 1, 'B', true]), expect);
        });
        it('["AB", []]', () => {
            assert.strictEqual(rules.inArray('AB', []), expect);
        });
        it('[true, {}]', () => {
            assert.strictEqual(rules.inArray(true, {}), expect);
        });
        it('[true, undefined]', () => {
            assert.strictEqual(rules.inArray(true, undefined), expect);
        });
        it('[{index: 0}, null]', () => {
            assert.strictEqual(rules.inArray({index: 0}, null), expect);
        });
    });
});