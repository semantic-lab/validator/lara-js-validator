import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().isNumeric', () => {
    describe('expect [true]', () => {
        const expect = true;
        const testValues = [
            -0.12345, -1, 0, 1, 123.456,
        ];
        testValues.forEach((ele) => {
            it(`[${ele.toString()}]`, () => {
                assert.strictEqual(rules.isNumeric(ele), expect);
            });
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        const testValues = [
            '-0.12345', '-1', '0', '1', '123.456',
            NaN, true, false, {}, [],
        ];
        testValues.forEach((ele) => {
            it(`[${ele.toString()}]`, () => {
                assert.strictEqual(rules.isNumeric(ele), expect);
            });
        });
    });
});