import assert from 'assert';
import rules from '../../../src/rules/index';

describe('Rules().digitsBetween', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('[-123.45, 3, 3]', () => {
            assert.strictEqual(rules.digitsBetween(-123.45, 3, 3), expect);
        });
        it('[-0.45, 1, 1]', () => {
            assert.strictEqual(rules.digitsBetween(-0.45, 1, 1), expect);
        });
        it('[123.45, -2, 3]', () => {
            assert.strictEqual(rules.digitsBetween(123.45, -2, 3), expect);
        });
        it('[123.45, 3, 10]', () => {
            assert.strictEqual(rules.digitsBetween(123.45, 3, 10), expect);
        });
        it('[0, 1, 3]', () => {
            assert.strictEqual(rules.digitsBetween(0, 1, 3), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('[123.45, 3, 1]', () => {
            assert.strictEqual(rules.digitsBetween(123.45, 3, 1), expect);
        });
        it('[123.45, 4, 5]', () => {
            assert.strictEqual(rules.digitsBetween(123.45, 4, 5), expect);
        });
        it('[123.45, 1, 2]', () => {
            assert.strictEqual(rules.digitsBetween(123.45, 1, 2), expect);
        });
        it('[undefined, -2, 2]', () => {
            assert.strictEqual(rules.digitsBetween(undefined, -2, 2), expect);
        });
        it('[null, -2, 2]', () => {
            assert.strictEqual(rules.digitsBetween(null, -2, 2), expect);
        });
        it('[[], -2, 2]', () => {
            assert.strictEqual(rules.digitsBetween([], -2, 2), expect);
        });
        it('[{}, -2, 2]', () => {
            assert.strictEqual(rules.digitsBetween({}, -2, 2), expect);
        });
    });
});