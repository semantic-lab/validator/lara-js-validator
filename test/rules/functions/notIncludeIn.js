import assert from 'assert';
import rules from '../../../src/rules/index';

class SmartPhone {
    constructor(phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    getPhone() {
        return this.phoneNumber;
    };
}

describe('Rules().notIncludeIn', () => {
    describe('expect [true]', () => {
        const expect = true;
        it('["AB", ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.notIncludeIn('AB', ['A', 1, 'B', true]), expect);
        });
        it('["!s", "a41bs!d?"]', () => {
            assert.strictEqual(rules.notIncludeIn('!s', 'a41bs!d?'), expect);
        });
        it('[["A", [], 234], ["A", 1, ["A", [], 234], true]]', () => {
            assert.strictEqual(rules.notIncludeIn(['A', [], 234], ['A', 1, ['A', [], 234], true]), expect);
        });
        it('[{index: 0}, ["A", {index: 0}, true]]', () => {
            assert.strictEqual(rules.notIncludeIn({index: 0}, ['A', {index: 0}, true]), expect);
        });
        it('[SmartPhone("0900000000"), ["A", SmartPhone("0900000000"), true]]', () => {
            assert.strictEqual(rules.notIncludeIn(new SmartPhone('0900000000'), ['A', new SmartPhone('0900000000'), true]), expect);
        });
        it('[[], ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.notIncludeIn([], ['A', 1, 'B', true]), expect);
        });
        it('[{}, ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.notIncludeIn({}, ['A', 1, 'B', true]), expect);
        });
        it('[undefined, ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.notIncludeIn(undefined, ['A', 1, 'B', true]), expect);
        });
        it('[null, ["A", 1, "B", true]]', () => {
            assert.strictEqual(rules.notIncludeIn(null, ['A', 1, 'B', true]), expect);
        });
        it('["AB", []]', () => {
            assert.strictEqual(rules.notIncludeIn('AB', []), expect);
        });
        it('[true, {}]', () => {
            assert.strictEqual(rules.notIncludeIn(true, {}), expect);
        });
        it('[true, undefined]', () => {
            assert.strictEqual(rules.notIncludeIn(true, undefined), expect);
        });
        it('[{index: 0}, null]', () => {
            assert.strictEqual(rules.notIncludeIn({index: 0}, null), expect);
        });
    });
    describe('expect [false]', () => {
        const expect = false;
        it('["AB", ["A", 1, "AB", true]]', () => {
            assert.strictEqual(rules.notIncludeIn('AB', ['A', 1, 'AB', true]), expect);
        });
        it('[true, ["A", 1, "AB", true]]', () => {
            assert.strictEqual(rules.notIncludeIn(true, ["A", 1, "AB", true]), expect);
        });
        it('["!s", "a41b!sd?"]', () => {
            assert.strictEqual(rules.notIncludeIn('!s', 'a41b!sd?'), expect);
        });
    });
});
