const lib = require('./lib/index');

module.exports = {
    LaraValidator: lib.LaraValidator,
    wrappers: lib.wrappers,
    rules: lib.rules
};
