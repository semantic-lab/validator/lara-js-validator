import validWrappers from './rules/wrappers';
import RuleMeta from './rules/RuleMeta';
import _ from 'lodash';

export default class LaraValidator {
    /**
     * Validator
     * @param {Object} rules - the rules config
     * @param {Object} data - the data which going to valid
     * @param {Object} expansionValidators - the expansion validation rules
     * @param {Object|undefined} customRulesMessage - the custom rules message, it's useful for i18n
     */
    constructor(rules, data, expansionValidators = {}, customRulesMessage = undefined) {
        this.setRules(rules);
        this.setData(data);
        this.wrappers = _.clone(validWrappers);
        this.setExpansionValidators(expansionValidators);
        this.setCustomRulesMessage(customRulesMessage);
        this.errorMessage = {};
    };

    setRules(rules) {
        this.ruleConfig = rules || {};
    };

    setData(data) {
        this.data = data || {};
    };

    setExpansionValidators(expansionWrappers = {}) {
        this.wrappers = _.merge(this.wrappers, expansionWrappers);
    };

    setCustomRulesMessage(customRulesMessage) {
        this.customRulesMessage = customRulesMessage;
    };

    /**
     *
     * @return {boolean|*}
     */
    valid() {
        this.errorMessage = {};
        this._recursive(this.ruleConfig, []);
        this.isValid = Object.keys(this.errorMessage).length <= 0;
        return this.isValid;
    };

    /**
     *
     * @param ruleConfig
     * @param parentPath
     * @private
     */
    _recursive(ruleConfig, parentPath = []) {
        Object.keys(ruleConfig).forEach((fieldName) => {
            if (fieldName !== 'rules') {
                const ruleValues = ruleConfig[fieldName];
                const ruleValuesIsString = typeof ruleValues === 'string';
                const ruleValuesIsArray = Array.isArray(ruleValues);
                const hasSubFields = typeof ruleValues === 'object';
                if (ruleValuesIsString) {
                    // field validation
                    const fieldAllRules = ruleValues.split('|');
                    this._fieldValidation(fieldAllRules, parentPath, fieldName, 'string', ruleConfig);
                } else {
                    // deep sub field rules validation
                    if (ruleValuesIsArray) {
                        const arrayElement = ruleValues[0];
                        this._recursive(arrayElement, [...parentPath, fieldName, '*']);
                    } else if (hasSubFields) {
                        this._recursive(ruleValues, [...parentPath, fieldName]);
                    }
                }
            } else {
                // field validation
                const fieldAllRules = Object.keys(ruleConfig.rules);
                const lastBasePathIndex = parentPath.length - 1;
                const fieldName = parentPath[lastBasePathIndex];
                const originParentPath = [...parentPath];
                originParentPath.pop();
                this._fieldValidation(fieldAllRules, originParentPath, fieldName, 'object', ruleConfig.rules);
            }
        });
    };

    /**
     *
     * @param fieldAllRules
     * @param parentPath
     * @param fieldName
     * @param ruleType
     * @param fieldRulesConfig
     * @private
     */
    _fieldValidation(fieldAllRules, parentPath, fieldName, ruleType, fieldRulesConfig) {
        const bail = fieldAllRules.includes('bail');
        const isNullable = fieldAllRules.includes('nullable');
        const presentOnly = fieldAllRules.includes('present');
        const ruleMeta = new RuleMeta(this.data);
        ruleMeta
            .setFieldPath(parentPath, fieldName)
            .setParentValues();

        fieldAllRules.every((ruleWithOptions) => {
            ruleMeta
                .resetRule()
                .setRule(ruleWithOptions, isNullable, presentOnly);
            const ruleIsExist = this.wrappers.hasOwnProperty(ruleMeta.ruleName);
            if (ruleIsExist) {
                // validation
                const ruleValidation = this.wrappers[ruleMeta.ruleName](ruleMeta);

                // set error message
                if (!ruleValidation.result) {
                    ruleValidation.fail.forEach((failDataPath) => {
                        let errorMessage = undefined;
                        switch(ruleType) {
                            case 'string':
                                errorMessage = this._getDefaultErrorMessage(
                                    ruleMeta.ruleName,
                                    ruleMeta.ruleOptions
                                );
                                break;
                            case 'object':
                                const errorMessageInRuleConfig = fieldRulesConfig[ruleWithOptions];
                                errorMessage = this._getDefaultErrorMessage(
                                    ruleMeta.ruleName,
                                    ruleMeta.ruleOptions,
                                    errorMessageInRuleConfig
                                );
                                break;
                        }
                        this._setErrorMessage(failDataPath, errorMessage);
                    });
                    if (bail) {
                        return false;
                    }
                }
            }
            return true;
        });
    }

    /**
     *
     * @param ruleName
     * @param ruleOptionValues
     * @param errorMessageInRuleConfig
     * @return {string}
     * @private
     */
    _getDefaultErrorMessage(ruleName, ruleOptionValues, errorMessageInRuleConfig) {
        if (LaraValidator.rules[ruleName]) {
            // rule
            const ruleOptionValuesIsString = typeof ruleOptionValues === 'string';
            const ruleOptionValuesIsArray = Array.isArray(ruleOptionValues);
            // static rule information
            const staticRule = LaraValidator.rules[ruleName];
            const staticOptionsIsString = typeof staticRule.options === 'string';
            const staticOptionsIsArrayWithElement = (Array.isArray(staticRule.options) && staticRule.options.length > 0);
            const hasStaticErrorMessage = staticRule.errorMessage;
            // error message
            let errorMessage = (errorMessageInRuleConfig)
                ? errorMessageInRuleConfig
                : (hasStaticErrorMessage)
                    ? staticRule.errorMessage
                    : `${ruleName} fail`;
            // post-processing for replacing rule options
            let staticRuleOptions = (staticOptionsIsString)
                ? [staticRule.options]
                : (staticOptionsIsArrayWithElement)
                    ? staticRule.options
                    : [];
            ruleOptionValues = (ruleOptionValuesIsString)
                ? [ruleOptionValues]
                : (ruleOptionValuesIsArray)
                    ? ruleOptionValues
                    : [] ;
            ruleOptionValues.forEach((ruleOptionValue, index) => {
                let staticRuleOption = staticRuleOptions[index] || staticRuleOptions[0] || 'NO_MAPPING_RULE_OPTIONS';
                staticRuleOption = staticRuleOption.replace(/\./gi, '\\.');
                errorMessage = errorMessage.replace(
                    new RegExp(`:${staticRuleOption}`, 'i'),
                    ruleOptionValue
                );
            });
            // return
            return errorMessage;
        } else {
            throw Error(`No such a rule named "${ruleName}"`);
        }
    }

    /**
     *
     * @param fieldPath
     * @param errorMessage
     * @private
     */
    _setErrorMessage(fieldPath, errorMessage) {
        const pathString = fieldPath.join('.');
        if (!this.errorMessage[pathString]) {
            this.errorMessage[pathString] = [];
        }
        this.errorMessage[pathString].push(errorMessage);
    }
};

LaraValidator.rules = {
    accepted: { options: undefined, errorMessage: undefined },
    alpha: { options: undefined, errorMessage: undefined },
    alpha_dash: { options: undefined, errorMessage: undefined },
    alpha_num: { options: undefined, errorMessage: undefined },
    array: { options: undefined, errorMessage: undefined },
    in_array: { options: 'anotherfield', errorMessage: undefined },
    in: { options: 'foo', errorMessage: undefined },
    not_in: { options: 'foo', errorMessage: undefined },
    distinct: { options: undefined, errorMessage: undefined },
    same: { options: 'field', errorMessage: undefined },
    different: { options: 'field', errorMessage: undefined },
    date: { options: undefined, errorMessage: undefined },
    after: { options: 'date', errorMessage: undefined },
    after_or_equal: { options: 'date', errorMessage: undefined },
    date_equals: { options: 'date', errorMessage: undefined },
    before_or_equal: { options: 'date', errorMessage: undefined },
    before: { options: 'date', errorMessage: undefined },
    regex: { options: 'pattern', errorMessage: undefined },
    not_regex: { options: 'pattern', errorMessage: undefined },
    starts_with: { options: 'foo', errorMessage: undefined },
    present: { options: undefined, errorMessage: undefined },
    required: { options: undefined, errorMessage: undefined },
    required_if: { options: 'anotherfield', errorMessage: undefined },
    required_unless: { options: 'anotherfield', errorMessage: undefined },
    required_with: { options: 'foo', errorMessage: undefined },
    required_with_all: { options: 'foo', errorMessage: undefined },
    required_without: { options: 'foo', errorMessage: undefined },
    required_without_all: { options: 'foo', errorMessage: undefined },
    gt: { options: 'field', errorMessage: undefined },
    gte: { options: 'field', errorMessage: undefined },
    lt: { options: 'field', errorMessage: undefined },
    lte: { options: 'field', errorMessage: undefined },
    max: { options: 'max', errorMessage: undefined },
    between: { options: 'min,max', errorMessage: undefined },
    min: { options: 'min', errorMessage: undefined },
    boolean: { options: undefined, errorMessage: undefined },
    numeric: { options: undefined, errorMessage: undefined },
    digits: { options: 'digits', errorMessage: undefined },
    digits_between: { options: 'min,max', errorMessage: undefined },
    integer: { options: undefined, errorMessage: undefined },
    string: { options: undefined, errorMessage: undefined },
    url: { options: undefined, errorMessage: undefined },
    uuid: { options: undefined, errorMessage: undefined },
    email: { options: undefined, errorMessage: undefined },
    size: { options: 'size', errorMessage: undefined },
    ip: { options: undefined, errorMessage: undefined },
    confirmed: { options: undefined, errorMessage: undefined },
    filled: { options: undefined, errorMessage: undefined },
    file: { options: undefined, errorMessage: undefined },
    image: { options: undefined, errorMessage: undefined },
    mimetypes: { options: 'text/plain', errorMessage: undefined },
    mimes: { options: 'foo', errorMessage: undefined },
};
