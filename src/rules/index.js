import laravelMimeMapper from "../laravelMimeMapper";

class Rules {
    constructor() {};
    accepted(val) {
        return (
            (typeof val === 'string' &&
                (val.toLowerCase() === 'yes' ||
                    val.toLowerCase() === 'on' ||
                    val.toLowerCase() === 'true' ||
                    val === '1')) ||
            val === 1 ||
            val === true
        );
    };
    /**
     * @param {Date|Number} val
     * @param {string} type should be one of '>', '>=', '=', '<=' or '<'
     * @param {Date|Number} compareVal
     */
    dateCompared(val, type, compareVal) {
        let testDate = undefined;
        let compareDate = undefined;
        try {
            testDate = (val instanceof Date) ? (new Date(val)).getTime() : val;
            compareDate = (compareVal instanceof Date) ? (new Date(compareVal)).getTime() : compareVal;
        } catch (error) {
            return false;
        }
        switch (type) {
            case '>':
                return testDate > compareDate;
            case '>=':
                return testDate >= compareDate;
            case '=':
                return testDate === compareDate;
            case '<=':
                return testDate <= compareDate;
            case '<':
                return testDate < compareDate;
            default:
                return false;
        }
    };
    afterDate(val, compareVal) {
        return this.dateCompared(val, '>', compareVal);
    };
    afterOrEqualDate(val, compareVal) {
        return this.dateCompared(val, '>=', compareVal);
    };
    alpha(val) {
        const alphaRegex = /^[a-zA-Z]*$/gi;
        return this.regex(val, alphaRegex);
    };
    alphaDash(val) {
        const alphaDashRegex = /^[a-zA-Z0-9\-_]*$/gi;
        return this.regex(val, alphaDashRegex);
    };
    alphaNum(val) {
        const alphaNumRegex = /^[a-zA-Z0-9]*$/gi;
        return this.regex(val, alphaNumRegex);
    };
    isArray(val) {
        return Array.isArray(val);
    };
    beforeDate(val, compareVal) {
        return this.dateCompared(val, '<', compareVal);
    };
    beforeOrEqualDate(val, compareVal) {
        return this.dateCompared(val, '<=', compareVal);
    };
    between(min, val, max) {
        return this.sizeCompare(val, '>=', min) &&
            this.sizeCompare(val, '<=', max);
    };
    isBoolean(val) {
        return (
            typeof val === 'boolean' ||
            val === '1' ||
            val === '0' ||
            val === 1 ||
            val === 0
        );
    };
    // todo: should be wrapper
    confirmed(val, fieldName, objectData = {}) {
        const confirmedFieldVal = objectData[`${fieldName}_confirmation`];
        return val === confirmedFieldVal;
    };
    isDate(val) {
        const isValidDateInstance = val instanceof Date && val.toString() !== 'Invalid Date';
        if (isValidDateInstance) {
            return true;
        } else if (val) {
            try {
                const dateInstance = new Date(val);
                const invalidDate = dateInstance.toString() === 'Invalid Date';
                return !invalidDate;
            } catch (error) {
                return false;
            }
        }
        return false;
    };
    equalDate(val, compareVal) {
        return this.dateCompared(val, '=', compareVal);
    };
    different(val, compareVal) {
        return !(this.same(val, compareVal));
    };
    digits(val, length) {
        const lengthOfNumeric = this.numericLength(val);
        return lengthOfNumeric !== 0 &&
            lengthOfNumeric === length;
    };
    digitsBetween(val, min, max) {
        const length = this.numericLength(val);
        return length !== 0 &&
            min <= length && length <= max;
    };
    isDistinct(array) {
        let exitColumns = [];
        if (Array.isArray(array)) {
            let result = true;
            array.every((ele) => {
                ele = JSON.stringify(ele);
                if (exitColumns.includes(ele)) {
                    result = false;
                    return false;
                } else {
                    exitColumns.push(ele);
                    return true;
                }
            });
            return result;
        }
        return false;
    };
    isEmail(val) {
        const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/gi;
        return this.regex(val, emailRegex);
    };
    isFile(val) {
        return val instanceof File;
    };
    filled(val) {
        if (val) {
            if (Array.isArray(val)) {
                return val.length > 0;
            } else if (typeof val === 'object') {
                return Object.keys(val).length > 0;
            }
            return true;
        }
        return false;
    };
    greaterThan(val, compareVal) {
        return this.sizeCompare(val, '>', compareVal);
    };
    greaterThanOrEqual(val, compareVal) {
        return this.sizeCompare(val, '>=', compareVal);
    };
    isImage(val) {
        if (this.isFile(val)) {
            const imageMIMEs = [
                'image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                // 'image/webp',
                // 'image/x-icon',
                // 'image/vnd.microsoft.icon',
                'image/svg+xml',
            ];
            return imageMIMEs.includes(val.type);
        }
        return false;
    };
    includeIn(val, opts = []) {
        try {
            return opts.includes(val);
        } catch (error) {
            return false
        }
    };
    inArray(val, arrayVal) {
        try {
            /*
            *                       toString()                JSON.stringify()            .constructor.toString()
            * {}                    '[object Object]'         '{}'                        'function Object() { [native code] }'
            * {data: 123}           '[object Object]'         '{data: 123}'               'function Object() { [native code] }'
            * /^Mc/i                '/^Mc/i'                  '{}'                        'function RegExp() { [native code] }'
            * WithPropClass('A')    '[object Object]'         '{prop: "A"}'               'class WithPropClass { ... }'
            * NoPropClass()         '[object Object]'         '{}'                        'class NoPropClass { ... }'
            * [1, 2, 3]             '1, 2, 3'                 '[1, 2, 3]'                 'function Array() { [native code] }'
            *
            * **/

            const valObjectType = this.getObjectType(val);
            if (valObjectType) {
                // compare for object
                const valStr = this.getObjectStringByType(val, valObjectType);
                let validResult = false;
                arrayVal.every((ele) => {
                    const eleObjectType = this.getObjectType(ele);
                    if (eleObjectType) {
                        const eleStr = this.getObjectStringByType(ele, eleObjectType);
                        if (valStr === eleStr) {
                            validResult = true;
                            return false;
                        } else {
                            return true;
                        }
                    }
                    return true;
                });
                return validResult;

            } else {
                return this.includeIn(val, arrayVal);
            }
        } catch (error) {
            return false
        }
    };
    isInteger(val) {
        return Number.isInteger(val);
    };
    isIP(val, type = 'ip') {
        switch (type.toLowerCase()) {
            case 'ipv4':
                return this.isIPv4(val);
            case 'ipv6':
                return this.isIPv6(val);
            default:
                return this.isIPv4(val) || this.isIPv6(val);
        }
    };
    isIPv4(val) {
        const ipv4Regex = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/gi;
        return this.regex(val, ipv4Regex);
    };
    isIPv6(val) {
        const ipv6Regex = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/gi;
        return this.regex(val, ipv6Regex);
    };
    lessThan(val, compareVal) {
        return this.sizeCompare(val, '<', compareVal);
    };
    lessThanOrEqual(val, compareVal) {
        return this.sizeCompare(val, '<=', compareVal);
    };
    isMimeTypes(val, mimeTypeArray = []) {
        if (this.isFile(val)) {
            return mimeTypeArray.includes(val.type);
        }
        return false;
    };
    isMime(val, mimeArray = []) {
        if (this.isFile(val)) {
            const guessMime = laravelMimeMapper[val.type];
            return mimeArray.includes(guessMime);
        }
        return false;
    };
    notIncludeIn(val, checkValArray = []) {
        return !this.includeIn(val, checkValArray);
    };
    notRegex(val, regEx) {
        return !this.regex(val, regEx);
    };
    nullable(val) {
        return (!!val || val === null);
    };
    numericLength(val) {
        // reference:
        // https://stackoverflow.com/questions/14879691/get-number-of-digits-with-javascript/28203456#28203456
        if (this.isNumeric(val)) {
            return (Math.log10((val ^ (val >> 31)) - (val >> 31)) | 0) + 1;
        }
        return 0;
    };
    isNumeric(val) {
        return typeof val === 'number' && !Number.isNaN(val);
    };
    present(fieldName, objectVal = {}) {
        try {
            return objectVal.hasOwnProperty(fieldName);
        } catch (error) {
            return false;
        }
    };
    regex(val, regEx) {
        try {
            return !!val.match(new RegExp(regEx));
        } catch (error) {
            return false;
        }
    };
    required(val) {
        const isUndefined = val === undefined;
        const isNull = val === null;
        const isEmptyString = typeof val === 'string' && val === '';
        const isEmptyArray = Array.isArray(val) && val.length === 0;
        const curlyObjectConstructor = 'function Object() { [native code] }';
        const isEmptyCurlyObject = val && typeof val === 'object' &&
            val.constructor.toString() === curlyObjectConstructor &&
            Object.keys(val).length === 0;
        return !(isUndefined || isNull || isEmptyString || isEmptyArray || isEmptyCurlyObject);
    };
    requiredIf(val, conditionData, conditionVal) {
        if (this.same(conditionData, conditionVal)) {
            return this.required(val);
        }
        return true;
    };
    requiredUnless(val, conditionData, conditionVal) {
        if (!this.same(conditionData, conditionVal)) {
            return this.required(val);
        }
        return true;
    };
    same(val, compareVal) {
        let valObjectType = undefined;
        let compareValObjectType = undefined;
        let getObjectTypeError = false;
        try {
            valObjectType = this.getObjectType(val);
            compareValObjectType = this.getObjectType(compareVal);
        } catch (error) {
            getObjectTypeError = true;
        }
        const sameObjectType = valObjectType === compareValObjectType;
        if (!getObjectTypeError && valObjectType && compareValObjectType && sameObjectType) {
            const valStr = this.getObjectStringByType(val, valObjectType);
            const compareValStr = this.getObjectStringByType(compareVal, compareValObjectType);
            return valStr === compareValStr;
        } else {
            return val === compareVal;
        }
    };
    getSize(arg) {
        try {
            if (typeof arg === 'string') {
                return arg.length;
            } else if (typeof arg === 'number') {
                return arg;
            } else if (Array.isArray(arg)) {
                return arg.length;
            } else if (this.isFile(arg)) {
                return arg.size;
            } else {
                return null;
            }
        } catch (error) {
            return null;
        }
    };
    sizeCompare(val, type, compareSize) {
        let checkValue = this.getSize(val);
        if (val !== null) {
            switch(type) {
                case '<':
                    return checkValue < compareSize;
                case '<=':
                    return checkValue <= compareSize;
                case '=':
                    return checkValue === compareSize;
                case '>':
                    return checkValue > compareSize;
                case '>=':
                    return checkValue >= compareSize;
            }
        }
        return false;
    };
    equalSize(val, compareSize) {
        return this.sizeCompare(val, '=', compareSize);
    };
    startWith(val, startRegexes = []) {
        let isStartWithRegex = false;
        startRegexes.every((ele) => {
            if (this.regex(val, new RegExp(`^${ele}`))) {
                isStartWithRegex = true;
                return false;
            }
            return true;
        });
        return isStartWithRegex;
    };
    isString(val) {
        return typeof val === 'string';
    };
    isURL(val, type = undefined) {
        // https://mathiasbynens.be/demo/url-regex
        let urlRegex = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/gi;
        if (type) {
            switch (type.toLowerCase()) {
                case 'http':
                    urlRegex = /http:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi;
                    break;
                case 'https':
                    urlRegex = /https:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/gi;
                    break;
            }
        }
        return this.regex(val, urlRegex);
    };
    isUUID(val, version = 0) {
        let uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
        switch (version) {
            case 1:
                uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-1[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
                break;
            case 2:
                uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-2[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
                break;
            case 3:
                uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-3[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
                break;
            case 4:
                uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
                break;
            case 5:
                uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-5[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i;
                break;
        }
        try {
            return this.regex(val, uuidRegex);
        } catch (error) {
            return false;
        }
    };
    getObjectType(val) {
        if (typeof val === 'object') {
            if (Array.isArray(val)) {
                return 'array';
            }
            if (val.constructor.toString() === 'function Object() { [native code] }') {
                return 'curlyObject';
            }
            if (val.constructor.toString().includes('class')) {
                return (JSON.stringify(val) === '{}') ? 'withoutPropClass' : 'withPropClass'
            } else {
                return 'prototypeObject'
            }
        }
        return false;
    };
    getObjectStringByType(val, type) {
        switch (type) {
            case 'array':
            case 'curlyObject':
            case 'withPropClass':
            case 'withoutPropClass':
                return JSON.stringify(val);
            case 'prototypeObject':
                return val.toString();
            default:
                return undefined;
        }
    };
}

const rules = new Rules();

export default rules;