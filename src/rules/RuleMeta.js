import rules from './index';

export default class RuleMeta {
    constructor(data) {
        // origin data
        this.data = data;

        // field & path:
        this.fieldParentPath = [];
        this.fieldName = null;
        this.fieldPath = [];
        this.fieldPathLength = null;
        this.fieldIsInArray = false;

        // rule:
        this.rule = null;
        this.ruleName = null;
        this.ruleOptions = null;
        this.isNullable = false;
        this.presentOnly = false;

        // values:
        this.parentValues = [];
    }

    /**
     *
     * @param {Boolean} throwError
     * @returns {Boolean}
     */
    isInit(throwError = true) {
        const hasPath = rules.required(this.fieldPath);
        const hasRule = rules.required(this.ruleName);
        if (throwError && !(hasPath && hasRule)) {
            throw Error('RuleMeta has not been initialized, please invoke "setFieldPath" and "setRule"');
        }
        return hasPath && hasRule;
    }

    /**
     *
     * @param {Array} parentPath
     * @param {String} fieldName
     * @returns {RuleMeta}
     */
    setFieldPath(parentPath, fieldName) {
        this.fieldParentPath = parentPath;
        this.fieldName = fieldName;
        this.fieldPath = [...this.fieldParentPath, this.fieldName];
        this.fieldPathLength = this.fieldPath.length;
        this.fieldIsInArray = this.fieldPath.includes('*');
        return this;
    }

    /**
     *
     * @return {RuleMeta}
     */
    resetRule() {
        this.rule = null;
        this.ruleName = null;
        this.ruleOptions = null;
        this.isNullable = false;
        this.presentOnly = false;
        return this;
    }

    /**
     *
     * @param {String} rule
     * @param {Boolean} isNullable
     * @param {Boolean} presentOnly
     * @param {Function} ruleOptionProcessCallback
     * @returns {RuleMeta}
     */
    setRule(rule, isNullable = false, presentOnly = false, ruleOptionProcessCallback = undefined) {
        const ruleInfo = rule.split(':');
        this.rule = rule;
        this.ruleName = ruleInfo[0];
        this.ruleOptions = (ruleOptionProcessCallback)
            ? ruleOptionProcessCallback(ruleInfo[1])
            : ruleInfo[1];
        this.isNullable = isNullable;
        this.presentOnly = presentOnly;
        return this;
    }

    /**
     *
     * @param {Array|undefined} values
     * @returns {RuleMeta}
     */
    setParentValues(values = undefined) {
        this.parentValues = values || RuleMeta.getValuesByPaths(this.fieldParentPath, this.data);
        return this;
    }

    /**
     *
     * @param {*} value
     * @return {boolean}
     */
    needToValidate(value) {
        if (this.presentOnly && (value === null || value === undefined)) {
            return false;
        } else if (this.isNullable && value === null) {
            return false;
        }
        return true;
    }
}

/**
 *
 * @param {Array} path
 * @param {Object} data
 * @param {Boolean} isRequired
 * @return {undefined[]}
 */
RuleMeta.getValuesByPaths = (path, data, isRequired = true) => {
    try {
        let values = [ {path: [], value: data} ];
        path.forEach((nodeName) => {
            let nextIteratorValues = [];
            const isArrayNode = nodeName === '*';
            values.forEach((value) => {
                if (!value.value && !isRequired) {
                    nextIteratorValues.push({
                        path: [...value.path, nodeName],
                        value: undefined,
                    });
                } else {
                    if (isArrayNode) {
                        value.value.forEach((element, index) => {
                            nextIteratorValues.push({
                                path: [...value.path, index.toString()],
                                value: element,
                            });
                        });
                    } else {
                        nextIteratorValues.push({
                            path: [...value.path, nodeName],
                            value: value.value[nodeName],
                        });
                    }
                }
            });
            values = nextIteratorValues;
        });
        return values;
    } catch (error) {
        if (!isRequired) {
            return [undefined];
        }
        throw error;
    }
};

/**
 *
 * @param {string} stringPath0
 * @param {string} stringPath1
 * @return {string}
 */
RuleMeta.pathType = (stringPath0, stringPath1) => {
    const path0IsInArray = stringPath0.includes('*');
    const path1IsInArray = stringPath1.includes('*');
    if (!path0IsInArray && !path1IsInArray) {
        return 'ONE_COMPARE_WITH_ONE';
    } else if (!path0IsInArray && path1IsInArray) {
        return 'ONE_COMPARE_WITH_MULTI';
    } else if (path0IsInArray && !path1IsInArray) {
        return 'MULTI_COMPARE_WITH_ONE';
    } else if (path0IsInArray && path1IsInArray) {
        return 'MULTI_COMPARE_WITH_MULTI';
    }
};
