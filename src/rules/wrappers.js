import rules from './index';
import RuleMeta from './RuleMeta';

export default {
    dateValidPass: false,
    optsNotExistMessage: 'opts should include paths(array), rule(string) and data(object) three properties.',
    validationWrapper(ruleMeta, ruleFunction) {
        let result = { result: true, fail: [] };
        if (ruleMeta.isInit()) {
            ruleMeta.parentValues.every((parent) => {
                const fieldName = ruleMeta.fieldName;
                const parentValue = parent.value;
                if (rules.present(fieldName, parentValue)) {
                    const validatedVal = parentValue[fieldName];
                    if (ruleMeta.needToValidate(validatedVal)) {
                        if (!ruleFunction(ruleMeta, validatedVal)) {
                            result.result = false;
                            result.fail.push([...parent.path, ruleMeta.fieldName]);
                            return false;
                        }
                    }
                } else {
                    result.result = false;
                    result.fail.push([...parent.path, ruleMeta.fieldName]);
                    return false;
                }
                return true;
            });
        }
        return result;
    },
    validationWrapperForRuleField(ruleMeta, ruleFunction) {
        if (ruleMeta.isInit()) {
            let result = { result: true, fail: [] };

            const fieldPath = ruleMeta.fieldPath;
            let ruleOptionPath = undefined;
            try {
                ruleOptionPath = ruleMeta.ruleOptions.split('.');
            } catch (error) {
                result.result = false;
                result.fail.push([...ruleMeta.fieldPath]);
                return result;
            }
            const fieldStringPath = ruleMeta.fieldPath.join('.');
            const ruleOptionStringPath = ruleMeta.ruleOptions;

            // path type
            const pathType = RuleMeta.pathType(fieldStringPath, ruleOptionStringPath);
            const oneValueCompareWithOneValue = pathType === 'ONE_COMPARE_WITH_ONE';            // 1:1      // a.b.c    x.y.z
            const oneValueCompareWithMultiValues = pathType === 'ONE_COMPARE_WITH_MULTI';       // 1:[]     // a.b.c    x.*.z
            const multiValuesCompareWithOneValue = pathType === 'MULTI_COMPARE_WITH_ONE';       // []:1     // a.*.c    x.y.z
            const multiValuesCompareWithMultiValues = pathType === 'MULTI_COMPARE_WITH_MULTI';  // []:[]    // a.*.b    x.*.z

            // compared:
            if (multiValuesCompareWithMultiValues) {
                // get the information (last '*' index) of field path:
                const lastStarRegex = new RegExp(/\.\*\.+(?!.*\.\*\.)/, 'g');
                const lastStarIndexOfFieldPath = fieldStringPath
                    .replace(lastStarRegex, '.LAST_STAR.')
                    .split('.')
                    .indexOf('LAST_STAR');

                const fieldValues = RuleMeta.getValuesByPaths(fieldPath, ruleMeta.data, false);
                fieldValues.every((fieldValue) => {
                    // set the compared file path with correct index
                    /**
                     * field_0.*.field_1.*.field_2
                     * replace to:
                     * field_0.*.field_1.{CORRECT_INDEX}.field_2
                     */
                    const trueIndex = fieldValue['path'][lastStarIndexOfFieldPath];
                    const ruleOptionIndexPath = ruleOptionStringPath
                        .replace(lastStarRegex, `.${trueIndex}.`)
                        .split('.');
                    const ruleOptionValues = RuleMeta.getValuesByPaths(ruleOptionIndexPath, ruleMeta.data, false);
                    if (ruleOptionValues.length === 0) {
                        if (!ruleFunction(ruleMeta, fieldValue['value'], undefined)) {
                            result.result = false;
                            result.fail.push([...fieldValue['path']]);
                        }
                    } else {
                        ruleOptionValues.every((ruleOptionValue) => {
                            if (!ruleFunction(ruleMeta, fieldValue['value'], ruleOptionValue['value'])) {
                                result.result = false;
                                result.fail.push([...fieldValue['path']]);
                                return false;
                            }
                            return true;
                        });
                        return result.result;
                    }
                });
            } else {
                const fieldValues = RuleMeta.getValuesByPaths(fieldPath, ruleMeta.data, false);
                const ruleOptionValues = RuleMeta.getValuesByPaths(ruleOptionPath, ruleMeta.data, false);
                fieldValues.every((fieldValue) => {
                    if (ruleOptionValues.length === 0) {
                        if (!ruleFunction(ruleMeta, fieldValue['value'], undefined)) {
                            result.result = false;
                            result.fail.push([...fieldValue['path']]);
                        }
                    } else {
                        ruleOptionValues.every((ruleOptionValue) => {
                            if (!ruleFunction(ruleMeta, fieldValue['value'], ruleOptionValue['value'])) {
                                result.result = false;
                                result.fail.push([...fieldValue['path']]);
                                return false;
                            }
                            return true;
                        });
                    }
                    return result.result;
                });
            }
            return result;
        }
    },
    accepted(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.accepted(validatedVal);
        });
    },
    alpha(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.alpha(validatedVal);
        });
    },
    alpha_dash(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.alphaDash(validatedVal);
        });
    },
    alpha_num(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.alphaNum(validatedVal);
        });
    },
    array(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isArray(validatedVal);
        });
    },
    in_array(ruleMeta) {
        // pre-process get array data
        const arrayFieldPath = ruleMeta.ruleOptions.split('.');
        let arrayData = [];
        const originArrayData = [];
        RuleMeta.getValuesByPaths(arrayFieldPath, ruleMeta.data).forEach((valueElement) => {
            originArrayData.push(valueElement['value']);
        });
        originArrayData.forEach((element) => {
            if (Array.isArray(element)) {
                // element: []
                element.forEach((value) => {
                    arrayData.push(value);
                });
            } else if (element.constructor.toString() === 'function Object() { [native code] }') {
                // element: {}
                Object.keys(element).forEach((key) => {
                    arrayData.push(element[key]);
                });
            } else {
                // element: a value
                arrayData.push(element);
            }
        });

        // validation
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.inArray(validatedVal, arrayData);
        });
    },
    in(ruleMeta) {
        const arrayData = ruleMeta.ruleOptions.split(',');
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            const stringValidatedVal = (typeof validatedVal === 'number') ? validatedVal.toString() : validatedVal;
            return rules.includeIn(stringValidatedVal, arrayData);
        });
    },
    not_in(ruleMeta) {
        const arrayData = ruleMeta.ruleOptions.split(',');
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.notIncludeIn(validatedVal, arrayData);
        });
    },
    distinct(ruleMeta) {
        const allData = [];
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            allData.push(validatedVal);
            return rules.isDistinct(allData);
        });
    },
    same(ruleMeta) {
        return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
            if (ruleMeta.needToValidate(validatedVal)) {
                const validatedValIsExist = validatedVal !== null && validatedVal !== undefined;
                const ruleOptionValIsExist = ruleOptionVal !== null && ruleOptionVal !== undefined;
                const isSame = rules.same(validatedVal, ruleOptionVal);
                return validatedValIsExist && ruleOptionValIsExist && isSame;
            }
            return true;
        });
    },
    different(ruleMeta) {
        return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
            if (ruleMeta.needToValidate(validatedVal)) {
                const validatedValIsExist = validatedVal !== null && validatedVal !== undefined;
                const ruleOptionValIsExist = ruleOptionVal !== null && ruleOptionVal !== undefined;
                const isDifferent = rules.different(validatedVal, ruleOptionVal);
                return validatedValIsExist && ruleOptionValIsExist && isDifferent;
            }
            return true;
        });
    },
    date(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isDate(validatedVal);
        });
    },
    after(ruleMeta) {
        const compareDate = new Date(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.afterDate(new Date(validatedVal), compareDate);
        });
    },
    after_or_equal(ruleMeta) {
        const compareDate = new Date(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.afterOrEqualDate(new Date(validatedVal), compareDate);
        });
    },
    date_equals(ruleMeta) {
        const compareDate = new Date(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.equalDate(new Date(validatedVal), compareDate);
        });
    },
    before_or_equal(ruleMeta) {
        const compareDate = new Date(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.beforeOrEqualDate(new Date(validatedVal), compareDate);
        });
    },
    before(ruleMeta) {
        const compareDate = new Date(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.beforeDate(new Date(validatedVal), compareDate);
        });
    },
    regex(ruleMeta) {
        const regexInfo = ruleMeta.ruleOptions.split('/');
        const regex = new RegExp(regexInfo[1], regexInfo[2]);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.regex(validatedVal, regex);
        });
    },
    not_regex(ruleMeta) {
        const regexInfo = ruleMeta.ruleOptions.split('/');
        const regex = new RegExp(regexInfo[1], regexInfo[2]);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.notRegex(validatedVal, regex);
        });
    },
    starts_with(ruleMeta) {
        const startOpts = ruleMeta.ruleOptions.split(',');
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.startWith(validatedVal, startOpts);
        });
    },
    present(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return true;
        });
    },
    required(ruleMeta) {
        let result = { result: true, fail: [] };
        if (ruleMeta.isInit()) {
            try {
                ruleMeta.parentValues.every((parent) => {
                    const fieldName = ruleMeta.fieldName;
                    const parentValue = parent.value;
                    const validatedVal = parentValue[fieldName];
                    if (!rules.required(validatedVal)) {
                        result.result = false;
                        result.fail.push([...parent['path'], ruleMeta.fieldName]);
                        return false;
                    }
                    return true;
                });
            } catch (error) {
                result.result = false;
                result.fail.push([...ruleMeta.fieldParentPath, ruleMeta.fieldName]);
            }
        }
        return result;
    },
    required_if(ruleMeta) {
        // pre-process (check if the anotherfield field is equal to any value)
        const optionsInfo = ruleMeta.ruleOptions.split(',');
        const optionPath = optionsInfo.splice(0, 1)[0];
        const optionMatchingValues = optionsInfo;
        ruleMeta.ruleOptions = optionPath;

        // main process:
        try {
            return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
                const needToValidate = optionMatchingValues.includes(ruleOptionVal);
                if (needToValidate) {
                    try {
                        return rules.required(validatedVal);
                    } catch (error) {
                        return false;
                    }
                }
                return true;
            });
        } catch (error) {
            return { result: false, fail: [[...ruleMeta.fieldPath]] };
        }
    },
    required_unless(ruleMeta) {
        // pre-process (check if unless the anotherfield field is equal to any value)
        const optionsInfo = ruleMeta.ruleOptions.split(',');
        const optionPath = optionsInfo.splice(0, 1)[0];
        const optionMatchingValues = optionsInfo;
        ruleMeta.ruleOptions = optionPath;

        // main process:
        try {
            return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
                const needToValidate = !(optionMatchingValues.includes(ruleOptionVal));
                if (needToValidate) {
                    try {
                        return rules.required(validatedVal);
                    } catch (error) {
                        return false;
                    }
                }
                return true;
            });
        } catch (error) {
            return { result: false, fail: [[...ruleMeta.fieldPath]] };
        }
    },
    required_with(ruleMeta) {
        // pre-process (check if any of the other specified fields are present)
        let needToValidate = false;
        let allOptionPaths = undefined;
        try {
            allOptionPaths = ruleMeta.ruleOptions.split(',');
        } catch (error) {
            allOptionPaths = [];
        }
        allOptionPaths.every((optionStringPath) => {
            const optionParentPath = optionStringPath.split('.');
            const optionField = optionParentPath.splice((optionParentPath.length - 1), 1)[0];
            const optionParentValues = RuleMeta.getValuesByPaths(optionParentPath, ruleMeta.data);
            optionParentValues.every((optionParentValue) => {
                const isPresent = rules.present(optionField, optionParentValue['value']);
                if (isPresent) {
                    needToValidate = true;
                    return false;
                }
                return true;
            });
            return !needToValidate;
        });

        // main process
        if (needToValidate) {
            return this.required(ruleMeta);
        }
        return { result: true, fail: [] };
    },
    required_with_all(ruleMeta) {
        // pre-process (check if all of the other specified fields are present)
        let needToValidate = true;
        let allOptionPaths = undefined;
        try {
            allOptionPaths = ruleMeta.ruleOptions.split(',');
        } catch (error) {
            allOptionPaths = [];
            needToValidate = false;
        }
        allOptionPaths.every((optionStringPath) => {
            const optionParentPath = optionStringPath.split('.');
            const optionField = optionParentPath.splice((optionParentPath.length - 1), 1)[0];
            const optionParentValues = RuleMeta.getValuesByPaths(optionParentPath, ruleMeta.data);
            optionParentValues.every((optionParentValue) => {
                const isPresent = rules.present(optionField, optionParentValue['value']);
                if (!isPresent) {
                    needToValidate = false;
                    return false;
                }
                return true;
            });
            return needToValidate;
        });

        // main process
        if (needToValidate) {
            return this.required(ruleMeta);
        }
        return { result: true, fail: [] };
    },
    required_without(ruleMeta) {
        // pre-process (check if any of the other specified fields are not present)
        let needToValidate = false;
        let allOptionPaths = undefined;
        try {
            allOptionPaths = ruleMeta.ruleOptions.split(',');
        } catch (error) {
            allOptionPaths = [];
            needToValidate = true;
        }

        allOptionPaths.every((optionStringPath) => {
            const optionParentPath = optionStringPath.split('.');
            const optionField = optionParentPath.splice((optionParentPath.length - 1), 1)[0];
            let optionParentValues = undefined;
            try {
                optionParentValues = RuleMeta.getValuesByPaths(optionParentPath, ruleMeta.data);
            } catch (error) {
                optionParentValues = [];
                needToValidate = true;
            }
            optionParentValues.every((optionParentValue) => {
                const isPresent = rules.present(optionField, optionParentValue['value']);
                if (!isPresent) {
                    needToValidate = true;
                    return false;
                }
                return true;
            });
            return !needToValidate;
        });

        // main process
        if (needToValidate) {
            return this.required(ruleMeta);
        }
        return { result: true, fail: [] };
    },
    required_without_all(ruleMeta) {
        // pre-process (check if all of the other specified fields are not present)
        let needToValidate = true;
        let allOptionPaths = undefined;
        try {
            allOptionPaths = ruleMeta.ruleOptions.split(',');
        } catch (error) {
            allOptionPaths = [];
        }

        allOptionPaths.every((optionStringPath) => {
            const optionParentPath = optionStringPath.split('.');
            const optionField = optionParentPath.splice((optionParentPath.length - 1), 1)[0];
            let optionParentValues = undefined;
            try {
                optionParentValues = RuleMeta.getValuesByPaths(optionParentPath, ruleMeta.data);
            } catch (error) {
                optionParentValues = [];
                needToValidate = true;
            }
            optionParentValues.every((optionParentValue) => {
                const isPresent = rules.present(optionField, optionParentValue['value']);
                if (isPresent) {
                    needToValidate = false;
                    return false;
                }
                return true;
            });
            return needToValidate;
        });

        // main process
        if (needToValidate) {
            return this.required(ruleMeta);
        }
        return { result: true, fail: [] };
    },
    gt(ruleMeta) {
        return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
            if (ruleMeta.needToValidate(validatedVal)) {
                const validatedValIsExist = validatedVal !== null && validatedVal !== undefined;
                const ruleOptionValIsExist = ruleOptionVal !== null && ruleOptionVal !== undefined;
                const isSameType = typeof validatedVal === typeof ruleOptionVal;
                if (validatedValIsExist && ruleOptionValIsExist && isSameType) {
                    const validatedValSize = rules.getSize(validatedVal);
                    const ruleOptionValSize = rules.getSize(ruleOptionVal);
                    return rules.greaterThan(validatedValSize, ruleOptionValSize);
                } else {
                    return false;
                }
            }
            return true;
        });
    },
    gte(ruleMeta) {
        return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
            if (ruleMeta.needToValidate(validatedVal)) {
                const validatedValIsExist = validatedVal !== null && validatedVal !== undefined;
                const ruleOptionValIsExist = ruleOptionVal !== null && ruleOptionVal !== undefined;
                const isSameType = typeof validatedVal === typeof ruleOptionVal;
                if (validatedValIsExist && ruleOptionValIsExist && isSameType) {
                    const validatedValSize = rules.getSize(validatedVal);
                    const ruleOptionValSize = rules.getSize(ruleOptionVal);
                    return rules.greaterThanOrEqual(validatedValSize, ruleOptionValSize);
                } else {
                    return false;
                }
            }
            return true;
        });
    },
    lt(ruleMeta) {
        return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
            if (ruleMeta.needToValidate(validatedVal)) {
                const validatedValIsExist = validatedVal !== null && validatedVal !== undefined;
                const ruleOptionValIsExist = ruleOptionVal !== null && ruleOptionVal !== undefined;
                const isSameType = typeof validatedVal === typeof ruleOptionVal;
                if (validatedValIsExist && ruleOptionValIsExist && isSameType) {
                    const validatedValSize = rules.getSize(validatedVal);
                    const ruleOptionValSize = rules.getSize(ruleOptionVal);
                    return rules.lessThan(validatedValSize, ruleOptionValSize);
                } else {
                    return false;
                }
            }
            return true;
        });
    },
    lte(ruleMeta) {
        return this.validationWrapperForRuleField(ruleMeta, (ruleMeta, validatedVal, ruleOptionVal) => {
            if (ruleMeta.needToValidate(validatedVal)) {
                const validatedValIsExist = validatedVal !== null && validatedVal !== undefined;
                const ruleOptionValIsExist = ruleOptionVal !== null && ruleOptionVal !== undefined;
                const isSameType = typeof validatedVal === typeof ruleOptionVal;
                if (validatedValIsExist && ruleOptionValIsExist && isSameType) {
                    const validatedValSize = rules.getSize(validatedVal);
                    const ruleOptionValSize = rules.getSize(ruleOptionVal);
                    return rules.lessThanOrEqual(validatedValSize, ruleOptionValSize);
                } else {
                    return false;
                }
            }
            return true;
        });
    },
    max(ruleMeta) {
        const maxValue = Number(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            const sizeOfValidatedVal = rules.getSize(validatedVal);
            return rules.lessThanOrEqual(sizeOfValidatedVal, maxValue);
        });
    },
    between(ruleMeta) {
        const compareValues = ruleMeta.ruleOptions.split(',');
        const minValue = Number(compareValues[0]);
        const maxValue = Number(compareValues[1]);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            const sizeOfValidatedVal = rules.getSize(validatedVal);
            return rules.between(minValue, sizeOfValidatedVal, maxValue);
        });
    },
    min(ruleMeta) {
        const minValue = Number(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            const sizeOfValidatedVal = rules.getSize(validatedVal);
            return rules.greaterThanOrEqual(sizeOfValidatedVal, minValue);
        });
    },
    boolean(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isBoolean(validatedVal);
        });
    },
    numeric(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isNumeric(validatedVal);
        });
    },
    digits(ruleMeta) {
        const lengthOfNum = Number(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.digits(validatedVal, lengthOfNum);
        });
    },
    digits_between(ruleMeta) {
        const compareValues = ruleMeta.ruleOptions.split(',');
        const minValue = Number(compareValues[0]);
        const maxValue = Number(compareValues[1]);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.digitsBetween(validatedVal, minValue, maxValue);
        });
    },
    integer(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isInteger(validatedVal);
        });
    },
    string(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isString(validatedVal);
        });
    },
    url(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isURL(validatedVal);
        });
    },
    uuid(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isUUID(validatedVal);
        });
    },
    email(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isEmail(validatedVal);
        });
    },
    size(ruleMeta) {
        const expectSize = Number(ruleMeta.ruleOptions);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            const sizeOfValidatedVal = rules.getSize(validatedVal);
            return sizeOfValidatedVal === expectSize;
        });
    },
    ip(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isIP(validatedVal);
        });
    },
    confirmed(ruleMeta) {
        const confirmedFieldPaths = [...ruleMeta.fieldParentPath, `${ruleMeta.fieldName}_confirmation`];
        const confirmedFieldValues = RuleMeta.getValuesByPaths(confirmedFieldPaths, ruleMeta.data);
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            let isConfirmed = true;
            confirmedFieldValues.every((confirmedValue) => {
                if (!rules.same(validatedVal, confirmedValue['value'])) {
                    isConfirmed = false;
                    return false;
                }
                return true;
            });
            return isConfirmed;
        });
    },
    filled(ruleMeta) {
        let result = { result: true, fail: [] };
        if (ruleMeta.isInit()) {
            ruleMeta.parentValues.every((parent) => {
                const fieldName = ruleMeta.fieldName;
                const parentValue = parent.value;
                if (rules.present(fieldName, parentValue)) {
                    const validatedVal = parentValue[fieldName];
                    if (!rules.required(validatedVal)) {
                        result.result = false;
                        result.fail.push([...ruleMeta.fieldPath]);
                        return false;
                    }
                }
                return true;
            });
        }
        return result;
    },
    // file wrappers have no test case
    file(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isFile(validatedVal);
        });
    },
    image(ruleMeta) {
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isImage(validatedVal);
        });
    },
    mimetypes(ruleMeta) {
        const candidateTypes = ruleMeta.ruleOptions.split(',');
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isMimeTypes(validatedVal, candidateTypes);
        });
    },
    mimes(ruleMeta) {
        const candidateExtensions = ruleMeta.ruleOptions.split(',');
        return this.validationWrapper(ruleMeta, (ruleMeta, validatedVal) => {
            return rules.isMime(validatedVal, candidateExtensions);
        });
    },
};
